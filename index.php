<?php
require_once("lib/config.php");

try {
	MvcHtml::RenderControllerResult(
		MvcEcontrolRouter::ExecuteRouteRequest(
			MvcEcontrolRouter::FindRouteRequest()));
}
catch(ErrorException $ex) {
	$data = array("message"=>$ex->getMessage(),
		          "code"=>$ex->getCode(),
			      "file"=>$ex->getFile(),
		          "line"=>$ex->getLine());
	MvcHtml::RenderAction("error","",$data);
	die();
}
/*
// TESTING PREG MATCH
$str = "[{tabs id='29,30,31'}]";

preg_match("/\[{[ ]*tabs[ ]*id[ ]*=[ ]*'[0-9\,]+'[ ]*}\]/", $str, $matches);

print_r($matches);*/
?>
<?php
session_start();
foreach (glob("lib/mvcCore/*.php") as $filename) {require_once($filename);}
foreach (glob("lib/mvcEcontrol/*.php") as $filename) {require_once($filename);}
require_once("lib/reRoutes.php");
require_once(Config::$site["modelsRoot"] . "security/userContext.php"); // for MVC user context
require_once("lib/handlers.php");

//******************************************************************************************

abstract class Config {
	public static $site 
		= array("name"=>"Econtrol",
				"lang"=>"en",
				"http"=>"http",
				"domain"=>"econtrolcms.com",
				"root"=>"/", // default = "/"
				"modelsRoot"=>"lib/models/",
				"mailFrom"=>"webmaster@econtrolcms.com",
				"analyticsId"=>array(),
				"madmimi"=>array(
					'email'=>'jabshire@resolutecreative.com', 
					'apikey'=>'c0f1f9b1de73c65f4b2b396f426eede7',
					'audience_list'=>'eControl',
					'promotion'=>'eControlTemplate'
					)
				);
	
	public static $themes 
		= array("default"=>array("root"=>"views/__themes/default/", "masterPageFile"=>"themeMaster.php"),
		        "admin"=>array("root"=>"views/__themes/admin/", "masterPageFile"=>"themeMaster.php"));

	public static $dynamicContent
		= array("theme"=>"default",
		        "layouts"=>array(
		        	//"layout-1"=>"layout-1.php", 
		        	"layout-2"=>"layout-2.php", 
		        	"layout-3"=>"layout-3.php", 
		        	"layout-4"=>"layout-4.php", 
		        	"layout-5"=>"layout-5.php", 
		        	"layout-6"=>"layout-6.php"
		        	));

	public static $security
		= array("hmacKey"=>"trustno1butMe!",	// url authenticating and expiring (i.e. forgot password link in email)
				"emailIsUsername"=>true,		// make username email as defualt?
				"useCaptcha"=>true,				// turn Captcha on/off (true/false)
				"userCookieLifespan"=>604800,	// one week, in seconds (60*60*24*7)
				"userGroups"=>array("guest"=>1, "member"=>2, "admin"=>99, "super"=>999),
				"enableSso"=>true,
				"googleOAuth2"=>array("clientId"=>"280626581494.apps.googleusercontent.com", "secretKey"=>"jbV1melJqZRB30M7hCLUZ7De"),
				"facebookOAuth2"=>array("clientId"=>"297887453625216", "secretKey"=>"87b761329a615855af6a18907b81652b"),
				"twitterOAuth1"=>array("clientId"=>"2bCo3PvUO0mXZdOARDPITw", "secretKey"=>"xoWE6CRolBxpidzbrZL6gqe0gHRxqLGeAXjMBW3Eg")
				);

	public static $connectionStrings 
		//= array("security"=>"localhost;root;root;MvcSiteTemplate");
		= array("security"=>"localhost;ec_user;SK3fPxuA9BPmrh7K;ec_econtrolcms");
}

//******************************************************************************************

MvcRouter::$viewsRoot = "views/";
MvcRouter::$controllersRoot = "lib/controllers/";

?>
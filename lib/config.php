<?php
session_start();
foreach (glob("lib/mvcCore/*.php") as $filename) {require_once($filename);}
foreach (glob("lib/mvcEcontrol/*.php") as $filename) {require_once($filename);}
require_once("lib/reRoutes.php");
require_once(Config::$site["modelsRoot"] . "security/userContext.php"); // for MVC user context
require_once("lib/handlers.php");

//******************************************************************************************

abstract class Config {
	
	public static $site 
		= array(
			"name"=>"Econtrol",
			"lang"=>"en",
			"http"=>"http",
			"domain"=>"econtrolcms:8888",
			"root"=>"/", // default = "/"
			"modelsRoot"=>"lib/models/",
			"homePage"=>"", // dynamic/index/48/
			"mailFrom"=>"webmaster@econtrolcms.com",
			"analyticsId"=>array(),
			"madmimi"=>array(
				'email'=>'jabshire@resolutecreative.com', 
				'apikey'=>'c0f1f9b1de73c65f4b2b396f426eede7',
				'audience_list'=>'eControl',
				'promotion'=>'eControlTemplate'
				)
			);
			
	// end $site
	
	public static $themes 
		= array(
			"default"=>array(
				"root"=>"views/__themes/default/", 
				"masterPageFile"=>"themeMaster.php"
				),
		    "admin"=>array(
		    	"root"=>"views/__themes/admin/", 
		    	"masterPageFile"=>"themeMaster.php"
		    	)
		    );
		    	
	// end $themes
	
	public static $dynamicContent
		= array(
			"theme"=>"default",
		    "layouts"=>array(
		    	"page"=>array(
    				1=>"layouts/layout-page-1.php",
					2=>"layouts/layout-page-2.php", 
					3=>"layouts/layout-page-3.php", 
					4=>"layouts/layout-page-4.php", 
					5=>"layouts/layout-page-5.php"
					),
				"menu"=>array(
			 		1=>"layouts/layout-menu-1.php",
			 		2=>"layouts/layout-menu-2.php",
			 		3=>"layouts/layout-menu-3.php",
			 		4=>"layouts/layout-menu-4.php",
			 		5=>"layouts/layout-menu-5.php"
					),
				"widget"=>array(
             		1=>"layouts/layout-widget-1.php",
             		2=>"layouts/layout-widget-2.php",
             		3=>"layouts/layout-widget-3.php",
             		4=>"layouts/layout-widget-4.php",
             		5=>"layouts/layout-widget-5.php"
		            )
		        ),
			"uploads" => array(
				"root"=>"uploads/",
				"image-versions"=>array(
					"small|120|90", 
					"medium|800|600", 
					"large|1024|768"
					),
				),
			"htmlEditor" => array(
				"tags" => array( 
					'"a"' => array(
						'check_attributes' => array( '"href"'=>'"url"', '"ref"'=>'"index"' ),
						'set_attributes' => array( '"target"'=>'"_self"' )
						),
					'"img"' => array(
						'check_attributes' => array( '"src"'=>'"url"', '"alt"'=>'"alt"', '"width"'=>'"numbers"', '"height"'=>'"numbers"' ),
						),
					'"hr"' => '',
					'"u"' => 1,
					"iframe" => array(
						'check_attributes' => array( '"src"'=>'"url"', '"width"'=>'"numbers"', '"height"'=>'"numbers"', 'allowFullScreen'=>'""', 'mozallowfullscreen'=>'""', 'webkitAllowFullScreen'=>'""', 'webkitAllowFullScreen'=>'""', 'frameborder'=>'"numbers"' ),
						)
					), // tags
				"classes" => array(
					'"left"' => 1,
					'"right"' => 1,
					'"center"' => 1,
					'"boxfit"' => 1
					)
			
			) // htmlEditor

		);

	// end $dynamicContent
	
	public static $security
		= array(
			"hmacKey"=>"trustno1butMe!",	// url authenticating and expiring (i.e. forgot password link in email)
			"emailIsUsername"=>true,		// make username email as defualt?
			"useCaptcha"=>true,				// turn Captcha on/off (true/false)
			"userCookieLifespan"=>604800,	// one week, in seconds (60*60*24*7)
			"userGroups"=>array("guest"=>1, "member"=>2, "admin"=>99, "super"=>999),
			"enableSso"=>true,
			"googleOAuth2"=>array("clientId"=>"280626581494.apps.googleusercontent.com", "secretKey"=>"jbV1melJqZRB30M7hCLUZ7De"),
			"facebookOAuth2"=>array("clientId"=>"297887453625216", "secretKey"=>"87b761329a615855af6a18907b81652b"),
			"twitterOAuth1"=>array("clientId"=>"2bCo3PvUO0mXZdOARDPITw", "secretKey"=>"xoWE6CRolBxpidzbrZL6gqe0gHRxqLGeAXjMBW3Eg")
			);

	// end $security
	
	public static $connectionStrings 
		= array("security"=>"localhost;ec_user;SK3fPxuA9BPmrh7K;ec_econtrolcms");
		//= array("security"=>"localhost;ec_user;SK3fPxuA9BPmrh7K;ec_econtrolcms");
	
	// end $connectionStrings
	
}

//******************************************************************************************

MvcRouter::$viewsRoot = "views/";
MvcRouter::$controllersRoot = "lib/controllers/";

?>
<html>
	<head>
		<title>Econtrol Help</title>
		<style type="text/css">
			body { font-size:16px; width:960px; margin:auto; }

			.topic {
				background-color:#cccccc;
				padding: 0.25em 1em 0.5em 1em;
				margin:1em;
				border-radius: 5px;
			}

			.code {
				font-family: "Courier New", Courier, monospace;
				font-size:0.75em;
				padding:1.0em 3.0em;
				color: #000055;
			}
			.code .indent { padding-left: 2.0em; }
			.code .comment { color:#004400; }
		</style>
	</head>
	<body>
		<h1>Econtrol Help</h1>

		<div class="contents">
			<ul>
				<li>
					Extended Mvc
					<ul>
						<li><a href="#includeMvcFiles">Including the MvcEcontrol files</a></li>
						<li><a href="#slugs">Slugs</a></li>
					</ul>
				</li>
			</ul>
		</div>
		
		<div class="topic">
			<a name="includeMvcFiles"/><h3>Including the MvcEcontrol files</h3>

			If you want to use extended Mvc features, such as slugs, you will need to include the MvcEntrol files. You can do this by adding the following line to your config.php, <strong><u>after including the MvcCore files</u></strong>:
			
			<div class="code">
				<div>foreach (glob("lib/mvcEcontrol/*.php") as $filename) {require_once($filename);}</div>
			</div>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="slugs"><h3>Slugs</h3>

			A slug must appear in the URL between the domain name and the controller name (a valid URL may contain only the domain name and Slug):

			<div class="code">http://mywebsite.com/<strong>Slug</strong>/ControllerName/Action/Data</div>

			To support slugs, you will need to modify the Mvc startup code in your site's startup file (index.php). You will need to use the extended MvcEcontrolRouter to properly find the route request:
			<div class="code">
				<div>MvcHtml::RenderControllerResult(</div>
					<div class="indent">MvcRouter::ExecuteRouteRequest(</div>
						<div class="indent"><span class="indent"><strong>MvcEcontrolRouter</strong>::FindRouteRequest()));</span><span class="comment">// use extended class to properly parse slugs from the URL</span></div>
			</div>

			It's important for the Econtrol Router to be able to distinguish between slugs and controllers when parsing the Url. This is done with the following function, found inside lib/mvcEcontrol/MvcEcontrolRouter.php:

			<div class="code">
				<div>private static function IsSlug($slug) {</div>
					<div class="indent">$securityService = new SecurityService();</div>
					<div class="indent">return $securityService->DoesUserNameExists($slug);</div>
				<div>}</div>
			</div>

			<div>The above code searches the user table to determine if the "slug" matches any of the user's names. If a match is found, the Router assumes that portion of the Url is a slug. If no match is found, the Router assumes there is no slug present in the Url. This is the default implementation. If you want the slug's value to be something other than the user's name, simply change the body of the above function, making sure it returns true when a slug is found (valid).</div>

			<br/>

			<div>Here's how the Member controller's "Info" action uses the slug:</div>

			<div class="code">
				<div>public function Info() {</div>
					<div class="indent">if ($slug = $this->GetRouteRequestSlug()) { <span class="comment">// optional function in MvcEcontrolController to simplify code</span></div>
						<div class="indent"><span class="indent">$userId = $this->securityService->DoesUserNameExists($slug);</span></div>
						<div class="indent"><span class="indent">$user = $this->securityService->GetUser($userId);</span></div>
					<div class="indent">}</div>
					<div class="indent">else <span class="comment">// no slug. default to logged in user</span></div>
						<div class="indent"><span class="indent">$user = SecurityController::RequireLogin();</span></div>
					<br/>
					<div class="indent">return $this->View("info", $user);</div>
				<div>}</div>
			</div>

			<div>Every controller has a member variable called $routeRequest. The $routeRequest contains an array called $custom. If a slug exists you will find it here. So you can optionally determine if a slug is present using the following code:</div>

			<div class="code">if (isset($this->routeRequest->custom["slug"])) ...</div>

			<div>The MvcEcontrolController class extends the MvcController class, and contains a function called "GetRouteRequestSlug." This function is optional, and simplifies the code needed to find the value of a slug. To use this function, the controller must extend MvcEcontrolContoller, like so:</div>

			<div class="code">class SecurityController extends MvcEcontrolController { <span class="comment">// extend MvcEcontrolController instead of MvcController</span></div>

			<div><i>Note: The MemberController extends the SecurityController, so the SecurityController needs to extend MvcEcontrolController in order for the MemberController to use GetRouteRequestSlug(). Previously, the SecurityController extended the core MvcController class.</i></div>

			<br/>

			<div>You can force an action to require a slug by using the following helper function (also part of MvcEcontrolController). If a slug is present, it will be returned by the function. If no slug is present, an error is thrown and the user is redirected to an error page.</div>

			<div class="code">$slug = $this->RequireSlug($this);</div>

			<div>Here's what the Member's Info action would look if you required a slug, and did <u>not</u> want to default to the logged in user:</div>

			<div class="code">
				<div>public function Info() {</div>
					<div class="indent">$slug = $this->RequireSlug($this);<span class="comment">// require a slug</span></div>
					<div class="indent">$userId = $this->securityService->DoesUserNameExists($slug);</div>
					<div class="indent">$user = $this->securityService->GetUser($userId);</div>
					<br/>
					<div class="indent">return $this->View("info", $user);</div>
				<div>}</div>
			</div>

			<div>One last thing: You can use the MvcEcontrolHtml class to create links that contain slugs in the Url. For example:</div>

			<div class="code">
				echo MvcEcontrolHtml::SlugActionLink("My Info", "drogers415", 'Member','Info', array(), array("id"=>"foo", "class"=>"button"));
				<div class="comment">
						// output:<br/>
						// &lt;a href="/drogers415/member/info/" id="foo" class="button" &gt;My Info&lt;/a&gt;
					</div>
			</div>

			<a href="#top">top</a>
		</div>
	</body>
</html>
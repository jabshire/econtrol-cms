<html>
	<head>
		<title>MvcCore Help</title>
		<style type="text/css">
			body { font-size:16px; width:960px; margin:auto; }

			.topic {
				background-color:#cccccc;
				padding: 0.25em 1em 0.5em 1em;
				margin:1em;
				border-radius: 5px;
			}

			.code {
				font-family: "Courier New", Courier, monospace;
				font-size:0.75em;
				padding:1.0em 3.0em;
				color: #000055;
			}
			.code .indent { padding-left: 2.0em; }
			.code .comment { color:#004400; }
		</style>
	</head>
	<body>
		<h1>MvcCore Help</h1>

		<div class="contents">
			<ul>
				<li>
					Getting Started
					<ul>
						<li><a href="#includeFiles">Including the MvcCore files</a></li>
						<li><a href="#friendlyUrls">Friendly URLs</a></li>
					</ul>
				</li>
				<li>
					Controllers
					<ul>
						<li><a href="#controllers">Controllers</a></li>
						<li><a href="#actions">Actions</a></li>
					</ul>
				</li>
				<li>
					Views
					<ul>
						<li><a href="#views">Views</a></li>
						<li><a href="#viewContext">View Context</a></li>
					</ul>
				</li>
				<li>
					Models
					<ul>
						<li><a href="#models">Models</a></li>
						<li><a href="#modelBinding">Model Binding</a></li>
						<li><a href="#modelValidation">Model Validation</a></li>
					</ul>
				</li>
				<li>
					Html Helper
					<ul>
						<li><a href="#actionLinks">Action Links</a></li>
						<li><a href="#formInputControls">Form Input Controls</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="topic">
			<a name="includeFiles"/><h3>Including the MvcCore files</h3>

			If you keep all the files in a folder named "mvcCore," then you can include the files with the following code:
			<div class="code">
				<div>foreach (glob("mvcCore/*.php") as $filename) {</div>
				<div class="indent"/>require_once($filename);</div>
				<div>}</div>
			</div>

			After you have included the files, you will need to include startup code in your site's startup file. You can do this with the following code:
			<div class="code">
				<div>MvcHtml::RenderControllerResult(</div>
					<div class="indent">MvcRouter::ExecuteRouteRequest(</div>
						<div class="indent"><span class="indent">MvcRouter::FindRouteRequest()));</span></div>
			</div>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="friendlyUrls"/><h3>Friendly URLs</h3>

			MvcCore will not function properly unless your site uses friendly URLs, and they must be in the following format:
			<div class="code">http://mywebsite.com/ControllerName/Action/Data</div>

			If you're web server is running Apache, then you can enable friendly URLs with an .htacces file. Your .htaccess file might look like the following:
			<div class="code">
				<div>&lt;IfModule mod_rewrite.c&gt;</div>
				<div>Options +FollowSymLinks  </div>
				<div>RewriteEngine On  </div>
				<div>RewriteBase /</div>
				<br/>
				<div>RewriteCond %{SCRIPT_FILENAME} !-d  </div>
				<div>RewriteCond %{SCRIPT_FILENAME} !-f  </div>
				<div>RewriteCond %{REQUEST_URI} !\.(css|gif|ico|jpg|js|png|swf|txt)$</div>
				<br/>
				<div>RewriteRule ^.*$ ./index.php</div>
				<div>&lt;/IfModule&gt;</div>
			</div>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="controllers"/><h3>Controllers</h3>

			The ControllerName portion of the URL specifies the controller that will be performing the requested action. The ControllerName is not required to be in the URL. When no ControllerName is specified, MvcCore will try to locate a Home controller. Here is an example of a Home controller:
			
			<div class="code">
				<div>class HomeController extends MvcController {</div>
					<div class="indent">public __construct() {</div>
						<div class="indent"><span class="indent">parent::__construct();</span></div>
					<div class="indent">}</div>
					<br/>
					<div class="indent">public function Index() {</div>
						<div class="indent"><span class="indent">return $this->View("index");</span></div>
					<div class="indent">}</div>
				<div>}</div>
			</div>

			All controllers should extend the "MvcController" base class. Also, each controller class name should end with "Controller." So in our example the ControllerName is "Home" and the ControllerType is "HomeController." Note that a URL is not case sensitive when containing a ControllerName.
			
			<br/><br/>

			Your default controller does not have to be Home. You can change the default controller with the following code:

			<div class="code">
				MvcRouter::AddReRoute(new MvcRouteRequest(), new MvcRouteRequest("myDefaultControllerName")); <span class="comment">// reroute unspecified controller to "MyDefaultControllerName"</span>
			</div>

			Controller class files need to be named in a certain way, and be located in a specific location. The default location for all controllers is in the "controllers" folder in the root of the web site. Each file name must match the ControllerName. For example, the location of a Home controller would be:

			<div class="code">&lt;web site root&gt;/controllers/home.php</div>

			You can change the default folder with the following code:

			<div class="code">MvcController::$controllersRoot = "myControllersFolder/"; <span class="comment">// changes location to &lt;web site root&gt;/myControllersFolder/home.php</span></div>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="actions"/><h3>Actions</h3>

			The Action portion of the URL specifies the action that the controller will perform. The Action is not required to be in the URL; however, the Action cannot be in the URL unless the ControllerName precedes it. The default Action for any controller is the Index action. This action will be called when no Action is specified in the URL.
			
			<div class="code">
				<div>class HomeController extends MvcController {</div>
					<div class="indent">public __construct() {</div>
						<div class="indent"><span class="indent">parent::__construct();</span></div>
					<div class="indent">}</div>
					<br/>
					<strong>
					<div class="indent"><span class="comment">// default action</span></div>
					<div class="indent">public function Index() {</div>
						<br/>
						<div class="indent"><span class="indent"><span class="comment">// do something</span></span></div>
						<br/>
						<div class="indent"><span class="indent">return $this->View("index");</span></div>
					<div class="indent">}</div>
					</strong>
				<div>}</div>
			</div>

			The list of available Actions that a controller can perform is determined by the functions contained in that controller class. For example, if you want the Home Controller to be able to perform an action called Hello, then it needs to have a function named "Hello." Here is what the Home controller might look like:

			<div class="code">
				<div>class HomeController extends MvcController {</div>
					<div class="indent">public __construct() {</div>
						<div class="indent"><span class="indent">parent::__construct();</span></div>
					<div class="indent">}</div>
					<br/>
					<div class="indent">public function Index() {</div>
						<div class="indent"><span class="indent">return $this->View("index");</span></div>
					<div class="indent">}</div>
					<br/>
					<strong>
					<div class="indent"><span class="comment">// called when the ControllerName is "home" and the Action is "hello"</span></div>
					<div class="indent"><span class="comment">// ex: http://www.mywebsite.com/home/hello</span></div>
					<div class="indent">public function Hello() {</div>
						<div class="indent"><span class="indent">return $this->View("hello");</span></div>
					<div class="indent">}</div>
					</strong>
				<div>}</div>
			</div>

			<div>All controllers must have an Index action/function.</div>
			<br/>
			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="views"/><h3>Views</h3>

			All controller actions must return a result. Most of the time you will want your Action to return a result that is sent to a particular View. You do this with the following code inside your Action function:
			
			<div class="code">return $this->View("hello"); <span class="comment">// return a result to the hello view</span></div>

			You are not required to give your View a name that matches the function/Action that calls it (although it often makes it easier to manage your files).

			<br/><br/>

			View files need to be stored in a specific location. The default location is in the "views" folder in the root of the web site, in a sub folder that matches the ControllerName. The name of the view file needs to match the View name. For example, the location of the Hello view would be:

				<div class="code">&lt;web site root&gt;/views/home/hello.php</div>

			You can change the views folder with the following code:

				<div class="code">MvcRouter::$viewsRoot = "myViewsFolder/"; <span class="comment">// change location to &lt;web site root&gt;/myViewsFolder/home/hello.php</span></div>

			Note that you are always required to place your view files in a sub folder that matches the ControllerName. This cannot be changed.
			
			<br/><br/>

			Think of Views as the final destination pages where html code is generated and rendered to the user. A Hello view might look as simple as this:

			<div class="code">
				<div>&lt;html&gt;</div>
					<div class="indent">&lt;head&gt;</div>
						<div class="indent"><span class="indent">&lt;title&gt;Hello&lt;/title&gt;</span></div>
					<div class="indent">&lt;/head&gt;</div>
					<div class="indent">&lt;body&gt;</div>
						<div class="indent"><span class="indent">&lt;div&gt;Hello World!&lt;/div&gt;</span></div>
					<div class="indent">&lt;/body&gt;</div>
				<div>&lt;/html&gt;</div>
			</div>

			So, suppose you have the following URL:

				<div class="code">http://mywebsite.com/home/hello</div>

			Visiting this URL will cause MvcCore to create an instance of the HomeController class. Once initialized, the HomeController will invoke it's Hello function. The Hello function will return a result to the Hello view (located at &lt;web site root&gt;/views/home/hello.php). The view will render, and will display:

			<br/><br/>
			<img src="images/view.jpg"/>
			<br/><br/>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="viewContext"/><h3>View Context</h3>

			There are several variables available for use within your views. One of those is the view context. Here is some of the information that a view can use from the view context:

			<div class="code">
				$__viewContext->controllerType; <span class="comment">// the controller type used in the request</span>
				<br/>
				$__viewContext->action; <span class="comment">// the action (controller function called)</span>
				<br/>
				$__viewContext->view; <span class="comment">// the name of the view returned by the controller action</span>
				<br/>
				$__viewContext->viewData; <span class="comment">// an array containing optional key=>value data, set by the controller action</span>
				<br/>
				$__viewContext->modelState; <span class="comment">// model error information</span>
			</div>

			$__viewContext->modelState is discussed in the <a href="#modelValidation">Model Validation</a> section.<br/>
			$__viewContext->controllerType tells the view the controller that was initialized by the request.<br/>
			$__viewContext->action tells the view the controller action that executed.<br/>
			$__viewContext->view is the name of the current view, returned by the controller action.<br/>
			By default, $__viewContext->viewData is an empty array; but it can be used to send any information to the view that you don't want contained in your view model (see the <a href="#models">Models</a> section for more info about models).<br/>
			
			<br/>
			
			All of the information in the view context is set automatically, except for viewData. Here's how you add an item to viewData. You'll want to add it from within a controller action:

			<div class="code">
				<div>public function Hello() {</div>
					<br/>
					<strong>
					<div class="indent"><span class="comment">// ex: send a message to the view</span></div>
					<div class="indent">$this->viewData["message"] = "I love php!";</div>
					</strong>
					<br/>
					<div class="indent">return $this->View("hello");</div>
				<div>}</div>
			</div>

			Here is an example of a view that accesses all of the view context information discussed:

			 <div class="code">
				<div>&lt;html&gt;</div>
					<div class="indent">&lt;head&gt;</div>
						<div class="indent"><span class="indent">&lt;title&gt;Hello&lt;/title&gt;</span></div>
					<div class="indent">&lt;/head&gt;</div>
					<div class="indent">&lt;body&gt;</div>
						<div class="indent"><span class="indent">&lt;div&gt;Hello World!&lt;/div&gt;</span></div>
						<div class="indent"><span class="indent">&lt;hr/&gt;</span></div>
						<div class="indent"><span class="indent">&lt;div&gt;</span></div>
						<div class="indent"><span class="indent"><span class="indent"><strong>Controller Type = &lt;?php echo $__viewContext->controllerType; ?&gt;</strong>&lt;br/&gt;</span></span></div>
						<div class="indent"><span class="indent"><span class="indent"><strong>Controller Name = &lt;?php echo MvcRouter::GetControllerName($__viewContext->controllerType); ?&gt;</strong>&lt;br/&gt;</span></span></div>
						<div class="indent"><span class="indent"><span class="indent"><strong>Action = &lt;?php echo $__viewContext->action; ?&gt;</strong>&lt;br/&gt;</span></span></div>
						<div class="indent"><span class="indent"><span class="indent"><strong>View = &lt;?php echo $__viewContext->view; ?&gt;</strong>&lt;br/&gt;</span></span></div>
						<div class="indent"><span class="indent"><span class="indent"><strong>Message = &lt;?php echo $__viewContext->viewData["message"]; ?&gt;</strong>&lt;br/&gt;</span></span></div>
						<div class="indent"><span class="indent">&lt;/div&gt;</span></div>
					<div class="indent">&lt;/body&gt;</div>
				<div>&lt;/html&gt;</div>
			</div>

			Remember that the controller name can be found in the URL. The controller type is the name of the corresponding controller class. The controller type is simply the controller name with "Controller" appended to the end of it (see the <a href="#controllers">Controllers</a> section). You can use the MvcRouter::GetControllerName function to get the name from the type. Here is the output:
			
			<br/><br/>
			
			<img src="images/viewContext.jpg"/>
			<br/><br/>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="models"/><h3>Models</h3>

			Since most views will not be static, you will often want your controller to send data to your view that will affect its output. You send data to your view in the form of a Model. Here is an example of a Person model:

			<div class="code">
				<div>class Person {</div>
				<div class="indent">var $firstName;</div>
				<div class="indent">var $lastName;</div>
				<div>}</div>
			</div>

			Sending a Person model to our Hello view is easy. We simply create an instance of Person inside our Hello action. Then we send the instance to the Hello view as a second parameter in the return:

			<div class="code">
				<div>class HomeController extends MvcController {</div>
					<div class="indent">public __construct() {</div>
						<div class="indent"><span class="indent">parent::__construct();</span></div>
					<div class="indent">}</div>
					<br/>
					<div class="indent">public function Index() {</div>
						<div class="indent"><span class="indent">return $this->View("index");</span></div>
					<div class="indent">}</div>
					<br/>
					<strong>
					<div class="indent">public function Hello() {</div>
						<br/>
						<strong>
						<div class="indent"><span class="indent comment">// create a Person model</span></div>
						<div class="indent"><span class="indent">$person = new Person();</span></div>
						<div class="indent"><span class="indent">$person->firstName = "John";</span></div>
						<div class="indent"><span class="indent">$person->lastName = "Doe";</span></div>
						<br/>
						<div class="indent"><span class="indent comment">// send our model to the hello view</span></div>
						<div class="indent"><span class="indent">return $this->View("hello", $person);</span></div>
						</strong>
					<div class="indent">}</div>
					</strong>
				<div>}</div>
			</div>

			Here's how we use the model in our Hello view:

			<div class="code">
				<div>&lt;html&gt;</div>
					<div class="indent">&lt;head&gt;</div>
						<div class="indent"><span class="indent">&lt;title&gt;Hello&lt;/title&gt;</span></div>
					<div class="indent">&lt;/head&gt;</div>
					<div class="indent">&lt;body&gt;</div>
						<div class="indent"><span class="indent">&lt;div&gt;Hello <strong>&lt;?php echo $__model->firstName . " " . $__model->lastName; ?&gt;</strong>!&lt;/div&gt;</span></div>
					<div class="indent">&lt;/body&gt;</div>
				<div>&lt;/html&gt;</div>
			</div>

			Notice that, when we created our model in our Hello action, we named our model "$person." But in our view it is called "$__model." MvcCore will rename all models to "$__model" before exposing it to a view. You can only send one model to a view. Here is the output:

			<br/><br/>

			<img src="images/model.jpg"/>
			<br/><br/>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="modelBinding"/><h3>Model Binding</h3>
			
			In the <a href="#models">Models</a> section we created an instance of a Person model, populated it with data, and sent it to a view. Sometimes you will want to populate your model with data from a URL, or with form data that was posted back to the server. You can do this automatically with model binding.
			<br/><br/>

			The first step in binding data to a model is to add the model to your action as a function parameter. This is how you would set up a Person model in the Hello action:

			<div class="code">
				<div>class HomeController extends MvcController {</div>
					<div class="indent">public __construct() {</div>
						<div class="indent"><span class="indent">parent::__construct();</span></div>
					<div class="indent">}</div>
					<br/>
					<div class="indent">public function Index() {</div>
						<div class="indent"><span class="indent">return $this->View("index");</span></div>
					<div class="indent">}</div>
					<br/>
					<div class="comment indent">// bind data to a Person model when the Hello action is called</div>
					<div class="indent">public function Hello(<strong>Person $person = null</strong>) {</div>
						<div class="indent"><span class="indent">return $this->View("hello", $person);</span></div>
					<div class="indent">}</div>
				<div>}</div>
			</div>

			There are at least three ways data can be binded to our person model:
			<ol>
				<li>
					Data in a Friendly URL:
					<div class="code">
						http://www.mywebsite.com/home/hello/john/doe
					</div>
					In this example, "home" is the controller name, "hello" is the action, and everything that follows is data to be binded to a model. When binding data from a friendly URL, the data must be in the same order as the properties in the model. Since "john" is the first piece of data that will be binded, and "john" is our first name, we will want to make sure that $firstName is the first property in our Person class declaration, like so:

					<div class="code">
						<div>class Person {</div>
						<div class="comment indent">// firstName is the first property declared in the model</div>
						<strong><div class="indent">var $firstName;</div></strong>
						<div class="indent">var $lastName;</div>
						<div>}</div>
					</div>

					<img src="images/bindingFriendly.jpg"/>
					<br/><br/>
				</li>
				<li>
					"GET" data - the query string portion of a URL:
					<div class="code">
						http://www.mywebsite.com/home/hello/?firstName=john&lastName=doe<br/>
						<div class="comment">// or</div>
						http://www.mywebsite.com/home/hello/?lastName=doe&firstName=john <span class="comment">// order not important</span>
					</div>
					In this example, data is passed in the query string in name=value pairs. Since each piece of data is named, the order of the data is not important; however, each name must match the name of a property in the model, else it will be ignored. So, "firstName=john" will bind to the "$firstName" property of our Person model, regardless of the order it appears in the query string; but "first=john" will not, since the model does not have property named "$first."
					<br/><br/>
					<img src="images/bindingGET.jpg"/>
					<br/><br/>
				</li>
				<li>
					"POST" data - form or ajax post:
					<br/><br/>
					The rules are the same as with query string data. The name of each piece of data must match the name of a model property in order for it to be bound.
				</li>
			</ol>

			Note that you can switch between the different binding methods without needing to make changes to your code. The only exception is that your model properties must be in a specific order when binding data from a friendly URL.
			<br/><br/>

			It's recommended that you initialize your action's parameters to null if you plan on data being bound to them. In this way you can tell if data was bound or not. If the value of your model variable remains null, then no data was bound. If the value is not null, then at least one of it's properties was bound successfully:

			<div class="code">
				<div>public function Hello(Person $person = null) {</div>
					<strong>
					<div class="indent">if ($person == null) {</div>
					<div class="indent"><span class="comment indent">// no data was bound</span></div>
					<div class="indent"><span class="comment indent">// here you can initialize your $person model with default values, or throw an error</span></div>
					<div class="indent">} </div>
					<div class="indent">else { </div>
					<div class="indent"><span class="comment indent">// data was bound successfully</span></div>
					<div class="indent">} </div>
					</strong>
					<br/>
					<div class="indent">return $this->View("hello", $person);</div>
				<div>}</div>
			</div>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="modelValidation"/><h3>Model Validation</h3>
			
			Coming soon...
			<br/><br/>

			<a href="#top">top</a>
		</div>

		<div class="topic">
			<a name="actionLinks"/><h3>Action Links</h3>

			The ActionLink helper function makes it easier to generate links to MVC enabled pages. Here is an example of a simple use of the function:

			<div class="code">
				echo MvcHtml::ActionLink("click me", "home", "hello");
				<br/>
				<div class="comment">
					// output:<br/>
					// &lt;a href="/home/hello/" &gt;click me&lt;/a&gt;
				</div>
			</div>

			The first parameter is the text portion of the anchor tag, the second parameter is the controller type or name (either one will work), and the third parameter is the action. Notice that the URL generated is a relative path from the root of the web site.

			<br/><br/>

			There is a fourth, optional parameter that accepts the "data" portion of the URL to be generated. The format of the URL will vary depending on the value of this parameter. It may be helpful to refer to the <a href="#modelBinding">Model Binding</a> section for more information about how the data portion of a URL is used. Here are several examples that show the different ways the data parameter can be supplied to the ActionLink function. Each example is followed by the output that would be generated. Notice how some URLs are friendly, while others contain query string data:

			<ol>
				<li class="code">
					echo MvcHtml::ActionLink("click me", "home", "hello", "john");
					<br/>
					<div class="comment">
						// output:<br/>
						// &lt;a href="/home/hello/john/" &gt;click me&lt;/a&gt;
					</div>
				</li>
				<li class="code">
					echo MvcHtml::ActionLink("click me", "home", "hello", array("john","doe"));
					<br/>
					<div class="comment">
						// output:<br/>
						// &lt;a href="/home/hello/john/doe/" &gt;click me&lt;/a&gt;
					</div>
				</li>
				<li class="code">
					echo MvcHtml::ActionLink("click me", "home", "hello", array("firstName"=>"john","lastName"=>"doe"));
					<br/>
					<div class="comment">
						// output:<br/>
						// &lt;a href="/home/hello/?firstName=john&lastName=doe" &gt;click me&lt;/a&gt;
					</div>
				</li>
				<li class="code">
					class Person {<br/>
						<span class="indent">var $firstName = "";</span><br/>
						<span class="indent">var $lastName = "";</span><br/>
					}<br/>
					$person = new Person();<br/>
					$person->firstName="john";<br/>
					$person->lastName="doe";<br/>
					<br/>
					echo MvcHtml::ActionLink("click me", "home", "hello", $person);
					<br/>
					<div class="comment">
						// output:<br/>
						// &lt;a href="/home/hello/?firstName=john&lastName=doe" &gt;click me&lt;/a&gt;
					</div>
				</li>
			</ol>

			There is a fifth, optional parameter that can be used to set the anchor tag's attributes (id, class, style, etc). This parameter must be an array of key=>value pairs:

			<div class="code">
				echo MvcHtml::ActionLink("click me", "home", "hello", array(), array("id"=>"foo", "class"=>"button"));
				<div class="comment">
						// output:<br/>
						// &lt;a href="/home/hello/" id="foo" class="button" &gt;click me&lt;/a&gt;
					</div>
			</div>

			When using the attributes parameter without the data parameter, the following values are valid for the data parameter:
			<ul class="code">
				<li>null</li>
				<li>""</li>
				<li>array()</li>
			</ul>

			<a href="#top">top</a>
		</div>
		
		<div class="topic">
			<a name="formInputControls"><h3>Form Input Controls</h3>

			There are several html helper functions you can use to render form input controls (text, password, hidden, etc). These are helpful for creating controls against model properties, because they will automatically set up the proper id/name needed to bind back to the model after the form is submitted (see <a href="#modelBinding">Model Binding</a>), and they will automaically set the value of the control with the corresponding model property's value.
	
			<br/><br/>

			Here is an example using the TextBoxFor helper function:

			<div class="code">
				echo MvcHtml::TextBoxFor($__model, "firstName", array("size"=>15));
				<div class="comment">
					// output:<br/>
					// First Name: &lt;input type='text' id='firstName' name='firstName' value="john"  size="15"/&gt;
				</div>
			</div>

			The first parameter is the model ($__model) passed to the view (see the <a href="#models">Models</a> section for more info). The second parameter is the name of the model property that is to be bound to the control. The third property is an optional array of key=>value pairs that define any attributes you would like the control to have. In the above example, the attributes property was used to give the textbox a size of 15.

			<br/><br/>
			<img src="images/textBoxFor.jpg"/>
			<br/><br/>

			Here is a list of the control functions that are currently available. All of the functions have the same signature, except for the HiddenFor function, which does not accept an attributes parameter:
			<ul>
				<li>TextBoxFor</li>
				<li>PasswordFor</li>
				<li>CheckBoxFor</li>
				<li>HiddenFor <i>(no attributes parameter available for hidden field)</i></li>
			</ul>

			<a href="#top">top</a>
		</div>
	</body>
</html>
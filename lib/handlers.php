<?php
error_reporting(0); // necessary to catch fatal errors
//error_reporting(E_ALL);
@ini_set('display_errors', '1'); // needed for IE(???)

//**************************************************************************************

function CustomErrorHandler($errno, $errstr, $errfile, $errline) {
	switch($errno) {
		case E_ERROR:
		case E_USER_ERROR:
		case E_RECOVERABLE_ERROR:
			ob_end_clean(); // clear the screen
			$data = array("message"=>$errstr, "code"=>500, "file"=>$errfile, "line"=>$errline);
			MvcHtml::RenderAction("error","",$data);
			die();
			break;
		case E_NOTICE:
		case E_USER_NOTICE:
			echo "<div class='php-error'>********************************************************<br/>\n";
			echo "Notice: $errstr<br/>\nLine $errline of $errfile<br/>\n";
			echo "********************************************************</div>\n";
			break;
		case E_WARNING:
		case E_USER_WARNING:
			echo "<div class='php-error'>********************************************************<br/>\n";
			echo "Warning: $errstr<br/>\nLine $errline of $errfile<br/>\n";
			echo "********************************************************</div>\n";
			break;
		default:
			// let default handling take place
			return false;
			break;
	};
}

set_error_handler('CustomErrorHandler');

//**************************************************************************************

function FatalErrorShutdownHandler()
{
  $last_error = error_get_last();
  if ($last_error != null) switch($last_error["type"]) {
	  case E_ERROR:
	  case E_USER_ERROR:
		  // recover from fatal error
		  if (isset($_SESSION["__workingDirectory"]))
			  chdir($_SESSION["__workingDirectory"]);
		  CustomErrorHandler($last_error["type"], $last_error['message'], $last_error['file'], $last_error['line']);
		  break;
  };
}

register_shutdown_function('FatalErrorShutdownHandler');

// remember the working directory
// so that we can go back to it if FatalErrorShutdownHandler called
if (!isset($_SESSION["__workingDirectory"]))
	$_SESSION["__workingDirectory"] = getcwd();
?>
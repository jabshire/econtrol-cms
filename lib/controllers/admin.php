<?php
require_once("security.php");
require_once(Config::$site["modelsRoot"] . "services/adminService.php");
require_once(Config::$site["modelsRoot"] . "services/uploadService.php");

class AdminController extends MvcEcontrolController {
	var $adminService = null;
	var $uploadService = null;
	var $requiredGroups = array();

	function __construct() { 
        parent:: __construct();
		$this->adminService = new AdminService();
		$this->uploadService = new UploadService();
		$this->requiredGroups = array(Config::$security["userGroups"]["admin"],
			                          Config::$security["userGroups"]["super"]);
    } 

	public function Index() {
		SecurityController::RequireLogin($this->requiredGroups);
		return $this->View("index");
	}

	public function Users(UserSearchCriteria $criteria = null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($criteria == null) $criteria = new UserSearchCriteria();

                if($criteria->orderBy == ""){
                    $criteria->orderBy="UserId";
                    $criteria->orderByDirection="DESC";                
                }
                
		$results = $this->adminService->SearchUsers($criteria);

		return $this->View("users", $results);
	}

	public function User($id = null, EditUserAdminRequest $request = null) {
		SecurityController::RequireLogin($this->requiredGroups);

		if ($request == null)
			return $this->View("user", $this->adminService->GetUser($id));
		else if ($this->modelState->IsValid()) {
			$this->adminService->UpdateUser($request);
			$this->Redirect("admin","users");
		}
		else
			return $this->View("user", $request);
	}

	public function DeleteUser(DeleteUserRequest $request) {
		SecurityController::RequireLogin($this->requiredGroups);

		if ($this->modelState->IsValid()) {
			$this->adminService->DeleteUser($request);
			SecurityController::Redirect("admin","users");
		}

		return $this->Users();
	}

	public function Pages(DynamicContentSearchCriteria $criteria=null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($criteria == null) {
			$criteria = new DynamicContentSearchCriteria();
			$criteria->type="PAGE";            
		}
                
                if($criteria->orderBy == ""){
                    $criteria->orderBy="c.dynamic_content_id";
                    $criteria->orderByDirection="DESC";                
                }

		$result = $this->adminService->SearchDynamicContent($criteria);
		return $this->View("pages", $result);
	}

	public function AddPage(CreateDynamicContentRequest $request = null) {
		SecurityController::RequireLogin($this->requiredGroups);

		// get list of widgets for page form
		$criteria = new DynamicContentSearchCriteria();
		$criteria->type="WIDGET";
		$criteria->status = "PUBLISHED";
		$this->viewData["widgets"] = $this->adminService->SearchDynamicContent($criteria)->results;

		if ($request == null)
			return $this->View("addPage", new CreateDynamicContentRequest());

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$id = $this->adminService->CreateDynamicContent($request, $userContext->userId);
			SecurityController::Redirect("admin", "page/edit", $id);
		}

		return $this->View("addPage", $request);
	}

	public function EditPage($getId=null, EditDynamicContentRequest $request=null) {
		SecurityController::RequireLogin($this->requiredGroups);

		// get list of widgets for page form
		$criteria = new DynamicContentSearchCriteria();
		$criteria->type="WIDGET";
		$criteria->status = "PUBLISHED";
		$this->viewData["widgets"] = $this->adminService->SearchDynamicContent($criteria)->results;

		if ($getId)
			return $this->View("editPage", $this->adminService->GetDynamicContent($getId));

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$this->adminService->EditDynamicContent($request, $userContext->userId);
			$this->viewData["updated"] = true;
			$request = $this->adminService->GetDynamicContent($request->id);
		}

		return $this->View("editPage", $request);
	}

	public function DeletePage($id) {
		SecurityController::RequireLogin($this->requiredGroups);

		$this->adminService->DeleteDynamicContent($id);
		SecurityController::Redirect("admin","pages");
	}

	public function Menus(DynamicContentSearchCriteria $criteria=null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($criteria == null) {
			$criteria = new DynamicContentSearchCriteria();
			$criteria->type="MENU";
		}
                
                if($criteria->orderBy == ""){
                    $criteria->orderBy="c.dynamic_content_id";
                    $criteria->orderByDirection="DESC";                
                }

		$result = $this->adminService->SearchDynamicContent($criteria);
		return $this->View("menus", $result);
	}

	public function AddMenu(CreateDynamicContentRequest $request = null) {
		SecurityController::RequireLogin($this->requiredGroups);

		if ($request == null)
			return $this->View("addMenu", new CreateDynamicContentRequest());

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$id = $this->adminService->CreateDynamicContent($request, $userContext->userId);
			SecurityController::Redirect("admin", "menu/edit", $id);
		}

		return $this->View("addMenu", $request);
	}

	public function EditMenu($getId=null, EditDynamicContentRequest $request=null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($getId)
			return $this->View("editMenu", $this->adminService->GetDynamicContent($getId));

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$this->adminService->EditDynamicContent($request, $userContext->userId);
			$this->viewData["updated"] = true;
			
			$request = $this->adminService->GetDynamicContent($request->id);
		}

		return $this->View("editMenu", $request);
	}

	public function PreviewMenu($id) {
		SecurityController::RequireLogin($this->requiredGroups);
		return $this->View("previewMenu", $this->adminService->GetDynamicContent($id));
	}

	public function DeleteMenu($id) {
		SecurityController::RequireLogin($this->requiredGroups);

		$this->adminService->DeleteDynamicContent($id);
		SecurityController::Redirect("admin","menus");
	}

	public function Widgets(DynamicContentSearchCriteria $criteria=null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($criteria == null) {//by default
			$criteria = new DynamicContentSearchCriteria();
			$criteria->type="WIDGET";
		}
                
                if($criteria->orderBy == ""){
                    $criteria->orderBy="c.dynamic_content_id";
                    $criteria->orderByDirection="DESC";                
                }

		$result = $this->adminService->SearchDynamicContent($criteria);
		return $this->View("widgets", $result);
	}

	public function AddWidget(CreateDynamicContentRequest $request = null) {
		SecurityController::RequireLogin($this->requiredGroups);

		if ($request == null)
			return $this->View("addWidget", new CreateDynamicContentRequest());

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$id = $this->adminService->CreateDynamicContent($request, $userContext->userId);
			SecurityController::Redirect("admin", "widget/edit", $id);
		}

		return $this->View("addWidget", $request);
	}

	public function EditWidget($getId=null, EditDynamicContentRequest $request=null) {
		SecurityController::RequireLogin($this->requiredGroups);
		if ($getId)
			return $this->View("editWidget", $this->adminService->GetDynamicContent($getId));

		if ($this->modelState->IsValid()) {
			$userContext = MvcSecurity::GetUserContext();
			$this->adminService->EditDynamicContent($request, $userContext->userId);
			$this->viewData["updated"] = true;
			$request = $this->adminService->GetDynamicContent($request->id);
		}

		return $this->View("editWidget", $request);
	}

	public function DeleteWidget($id) {
		SecurityController::RequireLogin($this->requiredGroups);

		$this->adminService->DeleteDynamicContent($id);
		SecurityController::Redirect("admin","widgets");
	}

	public function FileUploader() {
		$this->viewData["imageVersions"] = $this->uploadService->GetImageVersions();
		return $this->View("fileUploader");
	}

	public function FileUploaderHandler() {
		$this->uploadService->ProcessRequest();
	}

	public function DeleteImage($id) {
		SecurityController::RequireLogin($this->requiredGroups);
		
		$images = $this->uploadService->GetImage($id);
		$imageVersions = $this->uploadService->GetImageVersions();
		$rootDir = $_SERVER['DOCUMENT_ROOT'];
		$strReplace = Config::$site['http'] . '://' . Config::$site["domain"] . '/';
		
		if (!count($images)){ return false; }
		
		foreach($images[0] as $imageKey => $imageValue){
			if (strstr($imageKey, 'url')){
				unlink( $rootDir . '/' . str_replace($strReplace,'', urldecode($imageValue)) ); // -> will delete file
			}
		}
		$this->adminService->DeleteDynamicContent($id); // -> will delete record
	}
}

?>
<?php

require_once(Config::$site["modelsRoot"] . "services/dynamicContentService.php");

class DynamicController extends MvcEcontrolController {

	var $dynamicContentService = null;

	function __construct() { 
        parent:: __construct();

		$this->dynamicContentService = new DynamicContentService();
    } 

	public function Index(DynamicContentKey $contentKey=null, $view=0) {
		$content = $this->dynamicContentService->GetContentData($contentKey->id);
		
		if (!$view && $content->status == 'DRAFT'){
			throw new ErrorException("Content has not been published", 401);
		}
		
		//if (!$view && $content->status != "PUBLISHED")
		//	throw new ErrorException("Content has not been published", 401);

		switch($content->type) {
			case "PAGE":
				return $this->Page($content);
			case "WIDGET":
				return $this->Widget($content);
			case "MENU":
				return $this->Menu($content);
			case "IMAGE":
				return $this->Image($content, $view);
			default:
				throw new ErrorException("Unsupported dynamic content type", 404);
		}
	}

	// ======================== PAGE
	
	public function Page(DynamicContentData $content=null) {
		return $this->View("page", $content);
	}

	// ======================== WIDGET
	
	public function Widget(DynamicContentData $widget=null) {
		return $this->View("widget", $widget);
	}
	
	// ======================== MENU

	public function Menu(DynamicContentData $menu=null) {
		$menuItems = json_decode($menu->body);

		if(count($menuItems)) foreach($menuItems as $item) {
			if (is_numeric($item->url)) {
				$content = $this->dynamicContentService->GetContentData($item->url);
				$item->url = $content->primaryRoute;
			}
		}
		$menu->body = $menuItems;

		$this->AddRoutePossibilitiesToViewData();
		return $this->View("menu", $menu);
	}
	
	// ======================== IMAGE

	public function Image(DynamicContentData $image=null, $url="url") {
		$body = (array)json_decode($image->body);
		$image->body = isset($body[$url]) ? $body[$url] : $body["url"];

		return $this->View("image", $image);
	}
	
	// ======================== ADD POSSIBLE ROUTES

	private function AddRoutePossibilitiesToViewData() {
		$originalRouteRequest = MvcEcontrolRouter::FindRouteRequest();
		$originalRoute = trim($originalRouteRequest->GetRoute(), "/") . "/";

		$reRouteRequest = MvcEcontrolRouter::ReRouteRequest($originalRouteRequest);
		$reRoute = $reRouteRequest->GetRoute();

		$dynamicContentPrimaryRoute = "";
		$dynamicContentKey = $this->dynamicContentService->GetContentKeyByRoute($originalRoute);
		if ($dynamicContentKey != null) {
			$dynamicContent = $this->dynamicContentService->GetContentData($dynamicContentKey->id);
			$dynamicContentPrimaryRoute = $dynamicContent->primaryRoute;
		}

		$this->viewData["originalRoute"] = $originalRoute;
		$this->viewData["reRoute"] = $reRoute;
		$this->viewData["dynamicContentPrimaryRoute"] = $dynamicContentPrimaryRoute;
	}
}

?>
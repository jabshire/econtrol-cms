<?php

class ErrorController extends MvcController {

	function __construct() { 
        parent:: __construct();
    } 

	public function Index(ErrorException $error=null) {
		if ($error != null) {
			switch($error->getCode()) {
				case 401:
				case 404:
				case 500:
					return $this->View($error->getCode(), $error);
					break;
				default:
					return $this->View("generic", $error);
					break;
			};
		}
		else {
			return $this->View("generic");
		}
	}

	public function _401() {
		return $this->View("401");
	}

	public function _404() {
		return $this->View("404");
	}

	public function _500() {
		return $this->View("500");
	}
}

?>
<?php

require_once(Config::$site["modelsRoot"] . "services/securityService.php");
require_once(Config::$site["modelsRoot"] . "services/mailService.php");

class SecurityController extends MvcEcontrolController {

	var $securityService = null;
	var $mailService = null;

	function __construct() { 
        parent:: __construct();

		$this->securityService = new SecurityService();
		$this->mailService = new MailService();

		$originatingRouteRequest = MvcRouter::FindRouteRequest();
		$this->viewData["__OriginatingControllerType"] = $originatingRouteRequest->controllerType;
    } 

	public function Index() {
		$this->Redirect();
	}

	public function Login(LoginRequest $credentials=null) {
		if (MvcSecurity::GetUserContext())
			$this->Redirect();

		if ($credentials == null)
			$credentials = new LoginRequest();
		elseif($this->modelState->IsValid()) {
			$user = $this->securityService->GetUserContextByLogin($credentials);
			if ($user != null) {
				MvcSecurity::SetUserContext($user, ($credentials->remember)? Config::$security["userCookieLifespan"] : 0);
				if ($credentials->reply) {
					header("Location: " . $credentials->reply);
					exit;
				}
				else
					$this->Redirect();
			}
			else
				$this->modelState->AddError("", "Invalid email and/or password");
		}
		elseif(count($_POST) == 0)
			$this->modelState->Clear();

		// didn't want to add providers to model
		$this->viewData["ssoProviders"] = $this->securityService->GetSsoProvider();
		return $this->View("login", $credentials);
	}

	public function Sso(SsoRequest $request=null) {
		if ($request != null && $this->modelState->IsValid()) {
			try {
				$identity = $this->securityService->RequestSsoIdentity($request->providerId, $request->reply);
				$user = $this->securityService->GetUserContextBySsoIdentity($identity);
				
				if (!$request->reply && isset($_REQUEST["state"]))
					$request->reply = $_REQUEST["state"];

				// if the identity is new, offer to auto-create as member account
				if ($user == null || $user->userId < 1) {
					$createRequest = new CreateUserFromSsoRequest($identity);
					$createRequest->reply = $request->reply;
					$this->Redirect("security", "sso-create-member", $createRequest);
				}

				// update user identity info
				$this->securityService->UpdateUserSsoIdentity($user, $identity);

				MvcSecurity::SetUserContext($user, 0);
				if ($request->reply) {
					header("Location: " . $request->reply);
					exit;
				}
				else
					$this->Redirect();
			}
			catch(Exception $e) {
				$this->modelState->AddError("", $e->getMessage());
			}
		}

		// on validation error: go back to login screen, but retain reply url
		$credentials = new LoginRequest();
		$credentials->reply = $request->reply;
		// didn't want to add providers to model
		$this->viewData["ssoProviders"] = $this->securityService->GetSsoProvider();
		return $this->View("login", $credentials);
	}

	public function Logout() {
		MvcSecurity::DeleteUserContext();
		$this->Redirect();
	}

	public function ChangePassword(ChangePasswordRequest $request=null) {
		SecurityController::RequireLogin();
		
		$userContext = MvcSecurity::GetUserContext();

		if ($request == null) {
			$request = new ChangePasswordRequest();
			$request->userId = $userContext->userId;
			return $this->View("changePassword", $request);
		}
		elseif($this->modelState->IsValid()) {
			$this->securityService->ChangeUserPassword($request->userId, $request->newPassword);
			setcookie(MvcSecurity::$userContextKey . "PasswordChanged", "1", time()+60, "/");
			$this->Redirect($this->viewData["__OriginatingControllerType"], "Account");
		}

		return $this->View("changePassword", $request);
	}

	public function ForgotPassword($email) {
		if (MvcSecurity::GetUserContext())
			$this->Redirect();

		if (isset($_POST["email"])) {
			if (trim($email) == "")
				$this->modelState->AddError("", "Please enter the email associated with your account.");
			else {
				$userId = $this->securityService->DoesUserEmailExists($email);
				if (!$userId)
					$this->modelState->AddError("", "The email you entered is not associated with any account. Please check the email and try again.");
				else {
					$token = $this->securityService->CreateSecurityToken($userId, 60*60*1);
					$url = MvcRouter::GetActionUrl($this->viewData["__OriginatingControllerType"], "ResetPassword", $token);
					$vars = array("[{siteName}]"=>Config::$site["name"],
			          "[{url}]"=>Config::$site["http"] . "://" . Config::$site["domain"] . $url);
					$this->mailService->SendForgotPassword($email, $vars);
				}
			}
		}

		return $this->View("forgotPassword", $email);
	}

	public function ResetPassword(ResetPasswordRequest $request=null, SecurityToken $token) {
		if (MvcSecurity::GetUserContext())
			$this->Redirect();

		SecurityController::RequireValidSecurityToken($token);

		if ($request == null) {
			$request = new ResetPasswordRequest();
			$request->userId = $token->data;
			return $this->View("resetPassword", $request);
		}
		elseif ($this->modelState->IsValid()) {
			$this->securityService->ChangeUserPassword($request->userId, $request->password);
			$this->viewData["passwordReset"] = true;
		}

		return $this->View("resetPassword", $request);
	}

	public function SsoCreateMember(CreateUserFromSsoRequest $request) {
		if ($this->modelState->IsValid() && $request->userGroupId) {
			$password = "";
			$user = $this->securityService->CreateUserFromSsoIdentity($request, $password);
			$vars = array("[{siteName}]"=>Config::$site["name"],
			          "[{provider}]"=>ucfirst($request->providerId),
			          "[{username}]"=>$user->email,
			          "[{password}]"=>$password,
			          "[{myAccountUrl}]"=>Config::$site["http"] . "://" . Config::$site["domain"] . MvcRouter::GetActionUrl("Member", "Account"));
			$this->mailService->SendSsoMemberCreated($user->email, $vars);
			$this->mailService->AddToList($user->email, array("firstName"=>$user->firstName, "lastName"=>$user->lastName));

			MvcSecurity::SetUserContext($user, 0);
			if ($request->reply) {
				header("Location: " . $request->reply);
				exit;
			}
			else
				$this->Redirect();
		}

		return $this->View("ssoCreateMember", $request);
	}

	public function SsoEdit() {
		self::RequireLogin();
		$userContext = MvcSecurity::GetUserContext();

		$this->viewData["ssoProviders"] = $this->securityService->GetSsoProvider();
		return $this->View("ssoEdit", $this->securityService->GetUserSsoIdentities($userContext->userId));
	}

	public function SsoAdd($providerId) {
		self::RequireLogin();

		if (!trim($providerId))
			return $this->EditSso();

		try {
			$userContext = MvcSecurity::GetUserContext();
			$identity = $this->securityService->RequestSsoIdentity($providerId);
			$userId = $this->securityService->DoesSsoIdentityExist($identity->identityId);

			if ($userId) {
				if ($userId != $userContext->userId)
					$this->modelState->AddError("", "The following identity is already associated with a different " . Config::$site["name"] . " account: " . $identity->identityId);
				else
					$this->modelState->AddError("", "The following identity is already associated with your " . Config::$site["name"] . " account: " . $identity->identityId);
			}
			else {
				$this->securityService->CreateUserSsoIdentity($userContext, $identity);
			}
		}
		catch(Exception $e) {
			$this->modelState->AddError("", $e->getMessage());
		}

		return $this->SsoEdit();
	}

	public function SsoDelete(UserSsoIdentity $identity=null) {
		self::RequireLogin();

		if ($identity != null) {
			$this->securityService->DeleteUserSsoIdentity($identity);
		}
		return $this->SsoEdit();
	}

	public static function RequireLogin($userGroupIds = array()) {
		$userContext = MvcSecurity::GetUserContext();

		if ($userContext == null) {
			$routeRequest = MvcRouter::FindRouteRequest();
			$replyUrl = MvcRouter::GetActionUrl($routeRequest->controllerType, $routeRequest->action, $routeRequest->data);
			MvcController::Redirect("Security", "Login", array("reply"=>$replyUrl));
		}
		else {
			if (count($userGroupIds) == 0)
				$userGroupIds = array_slice(Config::$security["userGroups"], 1);

			if (array_search($userContext->userGroupId, $userGroupIds) === false) {
				throw new ErrorException("Insufficient permission", 401);
			}
		}

		return $userContext;
	}

	public static function RequireValidSecurityToken($token) {
		if (!SecurityService::IsValidSecurityToken($token)) {
			header('HTTP/1.1 403 Forbidden');
			exit;
		}
	}
}

?>
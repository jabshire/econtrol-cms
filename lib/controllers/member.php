<?php

require_once("security.php");

class MemberController extends SecurityController {

	function __construct() { 
        parent:: __construct();
    } 

	public function Index() {
		if (MvcSecurity::GetUserContext() == null)
			$this->Redirect("Member","Login");
		else
			$this->Redirect("Member","Account");
	}

	public function Create(CreateUserRequest $request=null) {
		if (MvcSecurity::GetUserContext())
			$this->Redirect("Member","Account");

		if ($request == null)
			return $this->View("create", new CreateUserRequest());
		elseif ($this->modelState->IsValid()) {
			$verificationCode = $this->securityService->CreateUser($request);
			$token = $this->securityService->CreateSecurityToken($verificationCode, 60*60*24);
			$vars = array("[{siteName}]"=>Config::$site["name"],
			          "[{url}]"=>Config::$site["http"] . "://" . Config::$site["domain"] . MvcRouter::GetActionUrl("Member", "Verify", $token));
			$this->mailService->SendAccountVerify($request->email, $vars);
			$this->mailService->AddToList($request->email, array("firstName"=>$request->firstName, "lastName"=>$request->lastName));
			$this->Redirect("Member", "Created", $request->email);
		}

		return $this->View("create", $request);
	}

	// TODO: Once email is implemented, verificationCode will not be needed in this action
	public function Created($email) {
		return $this->View("created", $email);
	}

	public function Verify(SecurityToken $token) {
		$this->RequireValidSecurityToken($token);

		if ($this->securityService->VerifyUser($token->data))
			return $this->View("verified");
		else
			return $this->Redirect();
	}

	public function Account() {
		$userContext = SecurityController::RequireLogin();

		if (isset($_COOKIE[MvcSecurity::$userContextKey . "PasswordChanged"])) {
			$this->viewData["HasPasswordChanged"] = true;
			setcookie(MvcSecurity::$userContextKey . "PasswordChanged", "0", time()-60, "/");
		}

		$account = $this->securityService->GetUserAccount($userContext->userId);
		return $this->View("account", $account);
	}

	public function Edit(EditUserRequest $request=null) {
		$userContext = SecurityController::RequireLogin();

		if ($request == null) {
			$request = new EditUserRequest();
			$request->userId = $userContext->userId;
			$request->userGroupId = $userContext->userGroupId;
			$request->userName = $userContext->userName;
			$request->email = $userContext->email;
			$request->oldEmail = $userContext->email;
			$request->firstName = $userContext->firstName;
			$request->lastName = $userContext->lastName;
		}
		elseif ($this->modelState->IsValid()) {
			$this->securityService->UpdateUser($request);
			$userContext->email = $request->email;
			$userContext->firstName = $request->firstName;
			$userContext->lastName = $request->lastName;
			MvcSecurity::UpdateUserContext($userContext);
			$this->Redirect("Member","Account");
		}

		return $this->View("edit", $request);
	}

	public function Info() {
		// to require slug
		//$slug = SecurityController::RequireSlug($this);

		// optional slug
		if ($slug = $this->GetRouteRequestSlug()) {
			$userId = $this->securityService->DoesUserNameExists($slug);
			$user = $this->securityService->GetUser($userId);
		}
		else
			$user = SecurityController::RequireLogin();

		return $this->View("info", $user);
	}
}

?>
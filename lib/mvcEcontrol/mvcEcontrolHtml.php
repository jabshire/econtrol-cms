<?php

require_once(Config::$site["modelsRoot"] . "services/dynamicContentService.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentKey.php");

class MvcEcontrolHtml extends MvcHtml {
	public static $dynamicContentService = null;
	
	public static function ValidationMessageFor(MvcViewContext $viewContext, $property, $customMsg=null, $attributes=array()) {
		return parent::ValidationMessageFor($viewContext, $property, $customMsg, $attributes);
	}

	public static function ValidationSummary($viewContext, $bShowPropertyErrors=true, $customMsg=null, $attributes=array()) {
		return parent::ValidationSummary($viewContext, $bShowPropertyErrors, $customMsg, $attributes);
	}

	public static function YesNoFor($obj, $property, $isOptional=false, $optionalMsg = "", $attributes=array()) {
		$html = "";

		$html .= "<select name=\"$property\"" . self::ParseAttributes($attributes) . ">";
			if ($isOptional) $html .= "<option value=\"\">$optionalMsg</option>";
			$html .= "<option value=\"1\"" .  ($obj->$property == 1 ? " selected" : "") . ">Yes</option>";
			$html .= "<option value=\"0\"" .  ($obj->$property !== null && $obj->$property == 0 ? " selected" : "") . ">No</option>";
		$html .= "</select>";

		return $html;
	}

	public static function SlugActionLink($text, $slug, $controllerType, $action, $data=array(), $attributes=array()) {
		return self::ActionLink($text, $slug . "/" . $controllerType, $action, $data, $attributes);
	}

	public static function Image($id, $size="") {
		$html = "";

		$view = ($size) ? ($size . "_url") : "url";

		ob_start();
		self::RenderAction("dynamic", "index", array("id"=>$id, "view"=>$view));
		$output = ob_get_contents();
		ob_end_clean();

		$html .= $output;

		return $html;
	}

	public static function Menu($id, $layout=1) {
		$menu = self::$dynamicContentService->GetContentData($id);
		$menu->layout = $layout;
		$html = "";

		ob_start();
		self::RenderAction("dynamic", "menu", (array)$menu);
		$output = ob_get_contents();
		ob_end_clean();

		$html .= $output;

		return $html;
	}

	public static function Sidebar($ids) {
		$widgetIds = json_decode($ids);
		
		$html = '';
		if (count($widgetIds)) foreach($widgetIds as $widgetId) {
			$html .= self::Widget($widgetId);
		}

		return $html;
	}

	public static function Widget($id) {
		$widget = self::$dynamicContentService->GetContentData($id);
		$html = "";

		ob_start();
		self::RenderAction("dynamic", "widget", (array)$widget);
		$output = ob_get_contents();
		ob_end_clean();

		$html .= $output;

		return $html;
	}

	public static function StaticWidget($action) {
		$html = "";

		ob_start();
		self::RenderAction("widget", $action);
		$output = ob_get_contents();
		ob_end_clean();

		$html .= $output;

		return $html;
	}

	public static function Tabs($id, $layout=1) {
		if (stristr($id, ',')) $ids = explode(',', $id.',');
		
		$ids = array_filter($ids);
		$tabs = array();
		$html = "";
		
		ob_start();
		echo '<section class="ec-tabs">'."\n";
		echo '<nav class="ec-tabsNav">'."\n";
		echo '<ul>'."\n";
		foreach($ids as $id){
			$tabContent = self::$dynamicContentService->GetContentData($id); //var_dump($tabContent);
			echo '<li class="ec-tabNav-'.$tabContent->id.'"><a href="'.$tabContent->id.'">'.str_replace('Tab - ', '', $tabContent->title).'</a></li>';
		}
		echo '</ul>'."\n";
		echo '</nav>'."\n";
		echo '<section class="ec-tabsBody">'."\n";
		foreach($ids as $id){
			$tabContent = self::$dynamicContentService->GetContentData($id);
			echo '<article class="ec-tabsBodyPage ec-tabsBodyPage-'.$tabContent->id.'">'.$tabContent->body.'</article>';
		}
		echo '</section>'."\n";
		echo '</section>'."\n";
		$output = ob_get_contents();
		ob_end_clean();

		$html .= $output;

		return $html;
	}

	function ReplaceShortCodes($text) {
		$text = str_replace("&#039;","'",$text);
		$text = str_replace("&nbsp;", " ", $text);

		return preg_replace_callback(
            array("/\[{[ ]*menu[ ]*id[ ]*=[ ]*'[\d]+'[ ]*layout[ ]*=[ ]*'[\d]'[ ]*}\]/",
			      "/\[{[ ]*static[ ]*action[ ]*=[ ]*'[\w|-]+'[ ]*}\]/",
			      "/\[{[ ]*widget[ ]*id[ ]*=[ ]*'[\w|-]+'[ ]*}\]/",
			      "/\[{[ ]*tabs[ ]*id[ ]*=[ ]*'[0-9\,]+'[ ]*}\]/"
			      ),
            "MvcEcontrolHtml::ReplaceShortCodesWithDynamicContent",
            $text);
	}

	static function ReplaceShortCodesWithDynamicContent($shortCodes) {
		if (preg_match("(\[{[ ]*menu)", $shortCodes[0])) {
			$menuAttributes = preg_replace(array("(\[{[ ]*menu[ ]*id[ ]*=[ ]*')", "('[ ]*layout[ ]*=[ ]*')", "('[ ]*}\])"), ";", $shortCodes[0]);
			$menuAttributes = explode(";", trim($menuAttributes, ";"));
			if (count($menuAttributes)<2){ // in case no layout is set
				$menuAttributes[1] = 1;
			}
			return self::Menu($menuAttributes[0], $menuAttributes[1]);
		}

		if (preg_match("(\[{[ ]*static)", $shortCodes[0])) {
			$staticAttributes = preg_replace(array("(\[{[ ]*static[ ]*action[ ]*=[ ]*')", "('[ ]*}\])"), ";", $shortCodes[0]);
			$staticAttributes = explode(";", trim($staticAttributes, ";"));
			return self::StaticWidget($staticAttributes[0]);
		}

		if (preg_match("(\[{[ ]*widget)", $shortCodes[0])) {
			$widgetAttributes = preg_replace(array("(\[{[ ]*widget[ ]*id[ ]*=[ ]*')", "('[ ]*}\])"), ";", $shortCodes[0]);
			$widgetAttributes = explode(";", trim($widgetAttributes, ";"));
			return self::Widget($widgetAttributes[0]);
		}

		if (preg_match("(\[{[ ]*tabs)", $shortCodes[0])) {
			$tabsAttributes = preg_replace(array("(\[{[ ]*tabs[ ]*id[ ]*=[ ]*')", "('[ ]*layout[ ]*=[ ]*')", "('[ ]*}\])"), ";", $shortCodes[0]);
			$tabsAttributes = explode(";", trim($tabsAttributes, ";"));
			if (count($tabsAttributes)<2){ // in case no layout is set
				$tabsAttributes[1] = 1;
			}
			return self::Tabs($tabsAttributes[0], $tabsAttributes[1]);
		}

		return $shortCodes[0];
	}
}

MvcEcontrolHtml::$dynamicContentService = new DynamicContentService();

?>
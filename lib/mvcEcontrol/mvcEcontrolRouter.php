<?php

require_once(Config::$site["modelsRoot"] . "services/securityService.php");
require_once(Config::$site["modelsRoot"] . "services/dynamicContentService.php");

class MvcEcontrolRouter extends MvcRouter {

	public static function FindRouteRequest() {
		$request = new MvcRouteRequest();

		$uri = parse_url($_SERVER["REQUEST_URI"]);
		$path = self::$appRoot == "/"
		      ? @split("/", substr($uri["path"], 1))
			  : @split("/", str_ireplace(self::$appRoot, "", $uri["path"]));

		// slug
		if (count($path) && strlen($path[0]) > 0 && self::IsSlug($path[0]))
			$request->custom["slug"] = urldecode(array_shift($path));

		// (dynamic content)
		$request->custom["route"] = trim(join("/", $path), "/") . "/";

		// controller
		if (count($path) && strlen($path[0]) > 0)
			$request->controllerType = urldecode(array_shift($path));
		$request->controllerType = self::GetControllerType($request->controllerType);

		// action
		if (count($path) && strlen($path[0]) > 0)
			$request->action = urldecode(array_shift($path));

		// data
		while(count($path)) {
			$d = urldecode(array_shift($path));
			if (trim($d)) $request->data[] = $d;
		}

		return $request;
	}

	public static function ExecuteRouteRequest($request) {
		try {
			return parent::ExecuteRouteRequest($request);
		}
		catch(ErrorException $ex) {
			if ($ex->getCode() != 404) throw $ex;

			$dynamicContentService = new DynamicContentService();
			$dynamicContentKey = $dynamicContentService->GetContentKeyByRoute($request->custom["route"]);

			if ($dynamicContentKey != null) {
				$request->controllerType = parent::GetControllerType("dynamic");
				$request->action = "";
				$request->data += (array)$dynamicContentKey;

				return parent::ExecuteRouteRequest($request);
			}
			else
				throw $ex;
		}
	}

	private static function IsSlug($slug) {
		$securityService = new SecurityService();
		return $securityService->DoesUserNameExists($slug);
	}
}

?>
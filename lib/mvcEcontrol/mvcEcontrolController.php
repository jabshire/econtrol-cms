<?php

abstract class MvcEControlController extends MvcController {

	function GetRouteRequestSlug() {
		return (isset($this->routeRequest->custom["slug"]))
			? $this->routeRequest->custom["slug"]
			: "";
	}

	public function RequireSlug() {
		if (!$slug = $this->GetRouteRequestSlug())
			throw new ErrorException("This controller/action requires a slug.", 401);
		return $slug;
	}
	
	protected function SecureView($view="", $model=null) {
		$userContext = MvcSecurity::GetUserContext();
		
		if ($userContext == null) {
			$routeRequest = MvcRouter::FindRouteRequest();
			$replyUrl = MvcRouter::GetActionUrl($routeRequest->controllerType, $routeRequest->action, $routeRequest->data);
			MvcController::Redirect("member", "login", array("reply"=>$replyUrl));
		}
		
		return parent::View($view, $model);
	}
}

?>
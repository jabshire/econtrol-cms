<?php

require_once(Config::$site["modelsRoot"] . "searchCriteria.php");

class UserSearchCriteria extends SearchCriteria implements IMvcModelValidator
{

    var $userGroupId = -1;
    var $userGroupText = "";
    var $userName = "";
    var $email = "";
    var $firstName = "";
    var $lastName = "";
    var $isVerified = null;
    var $isVerifiedText = "";

    function ValidateProperty($name)
    {
        return "";
    }

    function ValidateModel()
    {
        // select options cannot send null, only empty string
        // set to null so empty string is not mistaken for 0 ('No')
        if ($this->isVerified === "")
            $this->isVerified = null;
        return "";
    }

}

?>
<?php

require_once(Config::$site["modelsRoot"] . "searchCriteria.php");

class DynamicContentSearchCriteria extends SearchCriteria {
	var $type = "";
	var $description = "";
	var $title = "";
	var $seoTitle = "";
	var $seoDescription = "";
	var $createByUserName = "";
	var $modifyByUserName = "";
	var $status = "";
        var $statusText = "";
}

?>
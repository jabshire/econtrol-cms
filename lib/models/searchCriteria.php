<?php

class SearchCriteria {
	var $currentPage = 1;
	var $resultsPerPage = 100;
        var $orderBy = "";
        var $orderByDirection = "";

	function Skip() {
		$page = $this->currentPage >= 1 ? $this->currentPage : 1;
		return ($page-1) * $this->resultsPerPage;
	}
}

?>
<?php

class DynamicContentData extends DynamicContentKey {
	var $createByUserName = "";
	var $createDate = "";
	var $publishDate = NULL;
	var $modifyByUserName = "";
	var $modifyDate = "";

	var $seoTitle = "";
	var $seoDescription = "";
	var $teaser = "";
	var $body = "";
	var $class = "";
	var $image = 0;
	var $sidebar = "";
	var $layout = 0;

	var $primaryRoute = "";
}

?>
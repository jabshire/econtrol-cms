<?php

class EditDynamicContentRequest extends CreateDynamicContentRequest implements IMvcModelValidator {
	public function ValidateProperty($name) {
		return parent::ValidateProperty($name);
	}

	public function ValidateModel() {
		return parent::ValidateModel();
	}
	
	protected function DoesRouteExist() {
		$service = new DynamicContentService();
		$dynamicContentId = $service->DoesRouteExist($this->primaryRoute);

		// ignore if route belongs to this content
		return ($dynamicContentId && $this->id != $dynamicContentId);
	}
}

?>
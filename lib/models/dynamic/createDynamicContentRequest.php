<?php

class CreateDynamicContentRequest extends DynamicContentData implements IMvcModelValidator {
	public function ValidateProperty($name) {
		switch($this->type) {
			case "PAGE":
				return $this->ValidatePageProperty($name);
			case "MENU":
				return $this->ValidateMenuProperty($name);
			case "WIDGET":
				return $this->ValidateWidgetProperty($name);
			default:
				return "";
		}
	}

	public function ValidatePageProperty($name) {
		switch($name) {
			case "title":
				if (!trim($this->title)) return "Please enter a title.";
				break;
			case "status":
				if (!trim($this->status)) return "Please select a status.";
				break;
			case "seoTitle":
				if (!trim($this->seoTitle)) return "Please enter an SEO title.";
				break;
			case "seoDescription":
				// no validation
				break;
			case "teaser":
				// no validation
				break;
			case "body":
				if (!trim($this->body)) return "All posts must have a body.";
				break;
			case "image":
				// no validation
				break;
			case "sidebar":
				// no validation
				break;
			case "layout":
				if (!trim($this->layout)) return "Please select a layout.";
				break;
			case "primaryRoute":
				if (!trim($this->primaryRoute)) return "Please enter a route.";
				// fix format
				$this->primaryRoute = trim($this->primaryRoute,"/") . "/";
				if ($this->DoesRouteExist($this->primaryRoute)) return "Route already exists.";

				break;
		};

		return "";
	}

	public function ValidateMenuProperty($name) {
		switch($name) {
			case "title":
				if (!trim($this->title)) return "Please enter a name for this menu.";
				break;
			case "body":
				if (!trim($this->body)) return "Please add at least one menu item.";
				break;
		};

		return "";
	}

	public function ValidateWidgetProperty($name) {
		switch($name) {
			case "layout":
				if (!trim($this->layout)) return "All widgets must have a layout.";
				break;
			case "title":
				if (!trim($this->title)) return "Please enter a title.";
				break;
			case "status":
				if (!trim($this->status)) return "Please select a status.";
				break;
			case "body":
				if (!trim($this->body)) return "All posts must have a body.";
				break;
		};

		return "";
	}

	public function ValidateModel() {
		return "";
	}
	
	protected function DoesRouteExist() {
		$service = new DynamicContentService();
		return $service->DoesRouteExist($this->primaryRoute);
	}
}

?>
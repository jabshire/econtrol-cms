<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/dynamicContentRepository.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentKey.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentData.php");
require_once(Config::$site["modelsRoot"] . "dynamic/createDynamicContentRequest.php");
require_once(Config::$site["modelsRoot"] . "dynamic/editDynamicContentRequest.php");

class DynamicContentService {

	var $dynamicContentRepository = null;

	function __construct() {
		$this->dynamicContentRepository = new DynamicContentRepository();
	}

	function CreateContent(CreateDynamicContentRequest $content, $userId) {
		$dynamicContentId = $this->dynamicContentRepository->InsertContent($content->type,
			                                                                      $content->title,
																				  $content->status,
																				  $content->seoTitle,
																				  $content->seoDescription,
																				  $content->teaser,
																				  $content->body,
																				  $content->image,
																				  $content->sidebar,
																				  $userId,
																				  $content->layout,
																				  $content->primaryRoute);

		return $dynamicContentId;
	}

	function EditContent(EditDynamicContentRequest $content, $userId) {
		$dynamicContentId = $this->dynamicContentRepository->UpdateContent($content->id,
																				  $content->type,
			                                                                      $content->title,
																				  $content->status,
																				  $content->seoTitle,
																				  $content->seoDescription,
																				  $content->teaser,
																				  $content->body,
																				  $content->image,
																				  $content->sidebar,
																				  $content->publishDate,
																				  $userId,
																				  $content->layout,
																				  $content->primaryRoute);
	}

	function GetContentKeyByRoute($route) {
		$data = $this->dynamicContentRepository->SelectContentByRoute($route);
		if ($data == null || count($data) == 0)
			return null;

		$key = new DynamicContentKey();
		$key->id = $data["dynamic_content_id"];
		$key->type = $data["type"];
		$key->description = $data["title"];
		$key->status = $data["status"];

		return $key;
	}

	function GetContentData($dynamicContentId) {
		$data = $this->dynamicContentRepository->SelectContentDataById($dynamicContentId);

		$content = new DynamicContentData();
		$content->id = $data["dynamic_content_id"];
		$content->type = $data["type"];
		$content->title = stripslashes($data["title"]);
		$content->status = $data["status"];
		$content->createByUserName = $data["create_by_user_name"];
		$content->createDate = $data["create_date"];
		$content->publishDate = $data["publish_date"];
		$content->modifyByUserName = $data["modify_by_user_name"];
		$content->modifyDate = $data["modify_date"];
		$content->seoTitle = stripslashes($data["seo_title"]);
		$content->seoDescription = stripslashes($data["seo_description"]);
		$content->teaser = stripslashes($data["teaser"]);
		$content->body = stripslashes($data["body"]);
		$content->image = $data["image"];
		$content->sidebar = $data["sidebar"];
		$content->layout = $data["layout"];
		$content->primaryRoute = $data["primary_route"];

		return $content;
	}

	function DoesRouteExist($route) {
		$data = $this->dynamicContentRepository->SelectContentByRoute($route);

		return ($data && isset($data["dynamic_content_id"])) ? $data["dynamic_content_id"] : 0;
	}
}

?>
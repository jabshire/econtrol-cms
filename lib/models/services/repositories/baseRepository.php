<?php

class BaseRepository {
	var $link = "";

	function __construct($connectionString) {
		$host = strtok($connectionString, ";");
		$user = strtok(";");
		$pw = strtok(";");
		$db = strtok("");

		$this->link = mysqli_connect($host, $user, $pw, $db);
	}

	protected function QueryFirst($query) {
		$retval = "";

		if (mysqli_multi_query($this->link, $query)) {
			if ($result = mysqli_store_result($this->link)) {
				$retval = @mysqli_fetch_assoc($result);
				mysqli_free_result($result);
			}
		}else {
			$this->Error($query);
		}

		return $retval;
	}

	protected function Query($query) {
		$retval = array();

		if (mysqli_multi_query($this->link, $query)) {
			do {
				if ($result = mysqli_store_result($this->link)) {
					while ($row = @mysqli_fetch_assoc($result)) {
						$retval[] = $row;
					}
					mysqli_free_result($result);
				}

				//if (mysqli_more_results($link)) {
					// do something?
				//}
			} while (mysqli_next_result($this->link));
		}else {
			$this->Error($query);
		}

		return $retval;
	}

	private function Error($query) {
		$errorNo = @mysqli_errno($this->link);
		$errorMsg = mysqli_error($this->link);
		echo "<br/><br/><span style='color:red; font-size:-1;'>*** " . $errorMsg . " (" . $errorNo . "), query = </span> " . $query . "<span style='color:red;'> ***</span><br/><br/>"; exit;
	}
}

?>
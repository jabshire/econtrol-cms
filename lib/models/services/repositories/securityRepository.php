<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/baseRepository.php");

class SecurityRepository extends BaseRepository {

	function __construct() {
		parent:: __construct(Config::$connectionStrings["security"]);
	}

	function InsertUser($userGroupId, $userName, $password, $email, $firstName, $lastName, $isVerified, $verificationCode) {
		$query = "INSERT INTO User
		          SET UserGroupId=$userGroupId,
					  UserName='" . trim($userName) . "',
					  Password='" . trim($password) . "',
					  Email='" . trim($email) . "',
					  FirstName='" . trim(addslashes($firstName)) . "',
					  LastName='" . trim(addslashes($lastName)) . "',
					  IsVerified=$isVerified,
					  VerificationCode='$verificationCode',
					  CreateDate=now()";

		@$this->Query($query);
		return true;
	}

	function InsertUserSsoIdentity($providerId, $identityId, $userName, $email, $firstName, $lastName, $userId) {
		$query = "INSERT INTO sso_identity(sso_provider_id, identity_id, user_name, email, first_name, last_name, user_id) VALUES('$providerId', '$identityId', '$userName', '$email', '$firstName', '$lastName', $userId)";
		@$this->Query($query);
	}

	function SelectUserByUserId($userId) {
		$query = "SELECT * FROM User WHERE UserId='$userId'";
		return $this->QueryFirst($query);
	}

	function SelectUserByUserName($userName) {
		$query = "SELECT * FROM User WHERE UserName='" . trim($userName) . "'";
		return $this->QueryFirst($query);
	}

	function SelectUserByEmail($email) {
		$query = "SELECT * FROM User WHERE Email='" . trim($email) . "'";
		return $this->QueryFirst($query);
	}

	function SelectUserByLogin($email, $password) {
		$query = "SELECT UserId, UserGroupId, UserName, Email, FirstName, LastName
		          FROM User
				  WHERE IsVerified=1 AND Email='" . trim($email) . "' AND Password='" . trim($password) . "'";

	    return $this->QueryFirst($query);
	}

	function SelectUserBySsoIdentity($providerId, $identityId) {
		// first query doesn't work well because Twitter doesn't give an email
		// also not good for populating MyAccount edit page using UserContext
		/*$query = "SELECT u.UserId, u.UserGroupId, i.user_name, i.email, i.first_name, i.last_name, i.sso_provider_id
				FROM sso_identity i, User u
				WHERE sso_provider_id='$providerId' AND identity_id='$identityId'
				AND u.UserId=i.user_id";*/

		$query = "SELECT u.UserId, u.UserGroupId, u.UserName, u.Email, u.FirstName, u.LastName, i.sso_provider_id
				FROM sso_identity i, User u
				WHERE sso_provider_id='$providerId' AND identity_id='$identityId'
				AND u.UserId=i.user_id
				AND IsVerified=1";

		return $this->QueryFirst($query);
	}

	function SelectSsoProvider($providerId="", $enabledOnly=true) {
		$query = "SELECT sso_provider_id, sso_type, description, priority, enabled FROM sso_provider WHERE 2";
		if ($enabledOnly) $query .= " AND enabled=1";
		if ($providerId) $query .= " AND sso_provider_id='$providerId'";
		$query .= " ORDER BY priority";

		return $this->Query($query);
	}

	function SelectSsoEndpoint($providerId="") {
		$query = "SELECT authorization, scope, token, identity FROM sso_endpoint WHERE sso_provider_id='$providerId'";
		return $this->QueryFirst($query);
	}

	function SelectUserSsoIdentities($userId) {
		$query = "SELECT i.sso_provider_id, p.description, p.sso_type, i.identity_id, i.user_name, i.email, i.first_name, i.last_name
					FROM sso_identity i, sso_provider p
					WHERE i.user_id = $userId AND p.sso_provider_id=i.sso_provider_id
					ORDER BY p.priority";

		return $this->Query($query);
	}

	function SelectSsoIdentityById($identityId) {
		$query = "SELECT user_id FROM sso_identity WHERE identity_id='$identityId'";
		return $this->QueryFirst($query);
	}

	function UpdateUser($userId, $userGroupId, $userName, $email, $firstName, $lastName, $isVerified=null) {
		$query = "UPDATE User
		          SET UserGroupId=$userGroupId,
					  UserName='$userName',
					  Email='" . trim($email) . "',
					  FirstName='" . trim(addslashes($firstName)) . "',
					  LastName='" . trim(addslashes($lastName)) . "'";

		if ($isVerified !== null) $query .= ", IsVerified=$isVerified";

		$query .= " WHERE UserId=$userId";

		@$this->Query($query);
		return true;
	}

	function UpdateUserPassword($userId, $password) {
		$query = "UPDATE User
		          SET Password='" . trim($password) . "'
				  WHERE UserId=$userId";

		@$this->Query($query);
		return true;
	}

	function UpdateUserVerificationCode($verificationCode) {
		$query = "SELECT UserId FROM User WHERE IsVerified=0 AND VerificationCode='$verificationCode'";
		$row = $this->QueryFirst($query);

		if (isset($row["UserId"]) && $row["UserId"] > 0) {
			$query = "UPDATE User SET IsVerified=1 WHERE UserId=$row[UserId]";
			@$this->Query($query);
			return true;
		}

		return false;
	}

	function UpdateUserSsoIdentity($providerId, $identityId, $userName, $email, $firstName, $lastName, $userId) {
		$query = "UPDATE sso_identity SET user_name='$userName', email='$email', first_name='$firstName', last_name='$lastName' WHERE sso_provider_id='$providerId' AND identity_id='$identityId' AND user_id=$userId";
		@$this->Query($query);
	}

	function DeleteUser($userId) {
		$query = "DELETE FROM sso_identity WHERE user_id=$userId";
		$this->Query($query);
		$query = "DELETE FROM User WHERE UserId=$userId";
		@$this->Query($query);
	}

	function DeleteUserSsoIdentity($providerId, $identityId, $userId) {
		$query = "DELETE FROM sso_identity WHERE sso_provider_id='$providerId' AND identity_id='$identityId' AND user_id=$userId";
		@$this->Query($query);
	}

	function SearchUsersCount($userGroupId=-1, $userName="", $email="", $firstName="", $lastName="", $isVerified=null) {
		$query = "SELECT COUNT(*) as 'count' From User WHERE 1";
		if ($userGroupId >= 0) $query .= " AND UserGroupId=$userGroupId";
		if ($userName) $query .= " AND UserName LIKE '%$userName%'";
		if ($email) $query .= " AND Email LIKE '%$email%'";
		if ($firstName) $query .= " AND FirstName LIKE '%$firstName%'";
		if ($lastName) $query .= " AND LastName LIKE '%$lastName%'";
		if ($isVerified !== null) $query .= " AND IsVerified=$isVerified";
		$query .= " ORDER BY email";

		return $this->QueryFirst($query);
	}

	function SearchUsers($userGroupId=-1, $userName="", $email="", $firstName="", $lastName="", $isVerified=null, $skip, $limit, $orderBy="") {
		$query = "SELECT User.*,i.sso_provider_id, i.email as ssoEmail From User
                    LEFT JOIN sso_identity i ON User.UserId = i.user_id
                    WHERE 1";
		if ($userGroupId >= 0) $query .= " AND user.UserGroupId=$userGroupId";
		if ($userName) $query .= " AND user.UserName LIKE '%$userName%'";
		if ($email) $query .= " AND user.Email LIKE '%$email%'";
		if ($firstName) $query .= " AND user.FirstName LIKE '%$firstName%'";
		if ($lastName) $query .= " AND user.LastName LIKE '%$lastName%'";
		if ($isVerified !== null) $query .= " AND user.IsVerified=$isVerified";
                                
                if ($orderBy) 
                    $query .= " ORDER BY $orderBy";
                else
                    $query .= " ORDER BY user.Email";
                
                $query .= " LIMIT $skip, $limit";

		return $this->Query($query);
	}
}

?>
<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/baseRepository.php");

class MailRepository extends BaseRepository {

	var $messageDirectory = "";

	function __construct() {
		parent:: __construct(Config::$connectionStrings["security"]);
		$this->messageDirectory = Config::$site["modelsRoot"] . "services/repositories/mailMessages/";
	}

	function GetAccountVerify() {
		return $this->GetMessageData("accountVerify.txt");
	}

	function GetForgotPassword() {
		return $this->GetMessageData("forgotPassword.txt");
	}

	function SsoMemberCreated() {
		return $this->GetMessageData("ssoMemberCreated.txt");
	}

	function GetMessageData($file) {
		$file = file($this->messageDirectory . $file);
		$subject = "";
		$message = "";

		foreach($file as $line) {
			if (!$subject)
				$subject = $line;
			else {
				if ($message) $message .= "\r\n";
				$message .= $line;
			}
		}

		return array("subject"=>$subject, "message"=>$message);
	}
}

?>
<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/baseRepository.php");

class DynamicContentRepository extends BaseRepository {

	function __construct() {
		parent:: __construct(Config::$connectionStrings["security"]);
	}

	function InsertContent($type, $title, $status, $seoTitle, $seoDescription, $teaser, $body, $class, $image, $sidebar, $createByUserId, $createDate, $publishDate, $layout, $route) {
		$title = addslashes($title);
		$seoTitle = addslashes($seoTitle);
		$seoDescription = addslashes($seoDescription);
		$teaser = addslashes($teaser);
		$body = addslashes($body);

		$query = "INSERT INTO dynamic_content
		          SET type='$type',
					  title='$title',
					  status='$status',
					  create_by_user_id=$createByUserId,
					  create_date='$createDate',";

		if ($publishDate) $query .= "publish_date='$publishDate',";

		$query .="	  modify_by_user_id=$createByUserId,
					  modify_date=now()";

		@$this->Query($query);

		$result = $this->QueryFirst("SELECT LAST_INSERT_ID() as 'id'");
		$dynamic_content_id = $result["id"];

		$this->InsertContentData($dynamic_content_id, $seoTitle, $seoDescription, $teaser, $body, $class, $image, $sidebar, $layout);

		$this->InsertContentRoute($dynamic_content_id, $route);

		return $dynamic_content_id;
	}

	function InsertContentData($dynamic_content_id, $seoTitle, $seoDescription, $teaser, $body, $class, $image, $sidebar, $layout) {
		if (!$seoTitle && !$seoDescription && !$teaser && !$body && !$image && !$sidebar && !$layout) return;

		$query = "INSERT INTO dynamic_content_data
		          SET dynamic_content_id=$dynamic_content_id,
					  seo_title='$seoTitle',
					  seo_description='$seoDescription',
					  teaser='$teaser',
					  body='$body',
					  class='$class',
					  image=$image,
					  sidebar='$sidebar',
					  layout='$layout'";

		@$this->Query($query);
	}

	function InsertContentRoute($dynamic_content_id, $route) {
		if (!$route) return;
		
		$query = "INSERT INTO dynamic_content_routes
				  SET dynamic_content_id=$dynamic_content_id,
					  route='$route',
					  is_primary=1";

		@$this->Query($query);
	}

	function UpdateContent($id, $type, $title, $status, $seoTitle, $seoDescription, $teaser, $body, $class, $image, $sidebar, $publishDate, $modifyByUserId, $layout, $route) {
		$title = addslashes($title);
		$seoTitle = addslashes($seoTitle);
		$seoDescription = addslashes($seoDescription);
		$teaser = addslashes($teaser);
		$body = addslashes($body);

		$query = "UPDATE dynamic_content
		          SET type='$type',
					  title='$title',
					  status='$status',";

		if ($publishDate) $query .= " publish_date='" . date('Y-m-d H:i:s', strtotime($publishDate)) . "',";

		$query .= "	  modify_by_user_id=$modifyByUserId,
					  modify_date=now()
				  WHERE dynamic_content_id=$id";

		@$this->Query($query);

		$query = "UPDATE dynamic_content_data
		          SET seo_title='$seoTitle',
					  seo_description='$seoDescription',
					  teaser='$teaser',
					  body='$body',
					  class='$class',
					  image=$image,
					  sidebar='$sidebar',
					  layout='$layout'
				  WHERE dynamic_content_id=$id";

		@$this->Query($query);

		if (trim($route)) {
			$query = "UPDATE dynamic_content_routes
						  SET is_primary=0
						  WHERE dynamic_content_id=$id";
				@$this->Query($query);

			$query = "SELECT COUNT(*) AS 'cnt'
					  FROM dynamic_content_routes
					  WHERE dynamic_content_id=$id
					  AND route='$route'";

			$results = $this->QueryFirst($query);
			if ($results['cnt'] == 0) {
				$query = "INSERT INTO dynamic_content_routes
						  SET dynamic_content_id=$id,
						  route='$route',
						  is_primary=1";
				@$this->Query($query);
			}
			else {
				$query = "UPDATE dynamic_content_routes
						  SET is_primary=1
						  WHERE dynamic_content_id=$id
						  AND route='$route'";
				@$this->Query($query);
			}
		}
	}

	function DeleteContent($id) {
		@$this->QueryFirst("DELETE FROM dynamic_content_routes WHERE dynamic_content_id=$id;");
		@$this->QueryFirst("DELETE FROM dynamic_content_data WHERE dynamic_content_id=$id;");
		@$this->QueryFirst("DELETE FROM dynamic_content WHERE dynamic_content_id=$id;");
	}

	function SelectContentDataById($id) {
		$query = "SELECT d.*, c.*, r.route AS 'primary_route',
				u.UserName AS 'create_by_user_name', u2.UserName AS 'modify_by_user_name'
				FROM dynamic_content c
				LEFT JOIN dynamic_content_data d ON d.dynamic_content_id=c.dynamic_content_id
				LEFT JOIN dynamic_content_routes r ON r.dynamic_content_id=c.dynamic_content_id AND r.is_primary='1'
				LEFT JOIN User u ON u.UserId=c.create_by_user_id
				LEFT JOIN User u2 ON u2.UserId=c.modify_by_user_id
				WHERE c.dynamic_content_id=$id";

		return $this->QueryFirst($query);
	}

	function SelectContentByRoute($route) {
		$query = "SELECT c.*
				FROM dynamic_content c, dynamic_content_routes r
				WHERE r.dynamic_content_id=c.dynamic_content_id
				AND r.route = '$route'";

		return $this->QueryFirst($query);
	}

	function SelectContentRoutes($dynamicContentId) {
		$query = "SELECT * FROM dynamic_content_routes WHERE dynamic_content_id=$dynamicContentId";
		return $this->Query($query);
	}

	function SearchDynamicContentCount($type="", $title="", $status="", $seoTitle="", $seoDescription="", $createByUserName="", $modifyByUserName="") {
		$query = "SELECT COUNT(*) AS 'count'
		          FROM dynamic_content c, User u, User u2
		          WHERE u.UserId = c.create_by_user_id AND u2.UserId = c.modify_by_user_id";
		if ($type) $query .= " AND c.type = '$type'";
		if ($title) $query .= " AND c.title LIKE '%$title%'";
		if ($seoTitle) $query .= " AND d.seo_title LIKE '%$seoTitle%'";
		if ($seoDescription) $query .= " AND d.seo_description LIKE '%$seoDescription%'";
		if ($createByUserName) $query .= " AND u.UserName LIKE '%$createByUserName%'";
		if ($modifyByUserName) $query .= " AND u2.UserName LIKE '%$modifyByUserName%'";
		if ($status) $query .= " AND c.status= '$status'";

		return $this->QueryFirst($query);
	}

	function SearchDynamicContent($type="", $title="", $status="", $seoTitle="", $seoDescription="", $createByUserName="", $modifyByUserName="", $skip, $limit, $orderBy="") {
		$query = "SELECT d.*, c.*, r.route as 'primary_route', u.UserName as 'create_by_user_name', u2.UserName as 'modify_by_user_name'
		          FROM dynamic_content c
				  LEFT JOIN dynamic_content_data d ON d.dynamic_content_id=c.dynamic_content_id
				  LEFT JOIN dynamic_content_routes r ON r.dynamic_content_id=c.dynamic_content_id AND r.is_primary='1'
				  LEFT JOIN User u ON u.UserId = c.create_by_user_id
				  LEFT JOIN User u2 ON u2.UserId = c.modify_by_user_id
				  WHERE 1";
		if ($type) $query .= " AND c.type = '$type'";
		if ($title) $query .= " AND c.title LIKE '%$title%'";
		if ($seoTitle) $query .= " AND d.seo_title LIKE '%$seoTitle%'";
		if ($seoDescription) $query .= " AND d.seo_description LIKE '%$seoDescription%'";
		if ($createByUserName) $query .= " AND u.UserName LIKE '%$createByUserName%'";
		if ($modifyByUserName) $query .= " AND u2.UserName LIKE '%$modifyByUserName%'";
		if ($status) $query .= " AND c.status= '$status'";
                
                if ($orderBy) 
                    $query .= " ORDER BY $orderBy ";
                else
                    //$query .= " ORDER BY c.create_date DESC ";
                    $query .= " ORDER BY c.dynamic_content_id DESC ";
                
                $query .= "LIMIT $skip, $limit";

		return $this->Query($query);
	}
}

?>
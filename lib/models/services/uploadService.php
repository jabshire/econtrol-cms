<?php

require_once("lib/fileUploader/server/php/UploadHandler.php");
require_once(Config::$site["modelsRoot"] . "services/adminService.php");
require_once(Config::$site["modelsRoot"] . "services/dynamicContentService.php");

class UploadService extends UploadHandler {

	var $adminService = null;
	var $dynamicContentService = null;

	function __construct() {
		$this->adminService = new AdminService();
		$this->dynamicContentService = new DynamicContentService();

		$uploadDir = Config::$dynamicContent["uploads"]["root"] . date("Y") . "/" . date("m") . "/";
		
		$options = array('upload_dir' => $uploadDir,
			             'upload_url' => Config::$site["http"] . "://" . Config::$site["domain"] . Config::$site["root"] . $uploadDir,
			             'image_versions' => $this->GetImageVersions()
					);

		parent::__construct($options, false);
	}

	function ProcessRequest() {
		switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
                break;
            case 'GET':
                $this->GetList();
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->Upload();
                break;
            case 'DELETE':
                $this->delete();
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }
	}

	function GetImageVersions() {
		$imgSizes = Config::$dynamicContent["uploads"]["image-versions"];

		$imgVersions = array();
		foreach($imgSizes as $imgSize) {
			$imgParams = explode("|", $imgSize);

			$imgVersions[$imgParams[0]]["max_width"] = $imgParams[1];
			$imgVersions[$imgParams[0]]["max_height"] = $imgParams[2];
			$imgVersions[$imgParams[0]]["jpeg_quality"] = 80;
		}

		return $imgVersions;
	}

	private function GetList() {
		$criteria = new DynamicContentSearchCriteria();
		$criteria->type = "IMAGE";

		$images = $this->adminService->SearchDynamicContent($criteria);

		$result = array();
		foreach($images->results as $image) {
			$result = json_decode($image->body);
			$result = (array)$result;
			$result["dynamic_content_id"] = $image->id;
			$results[] = $result;
		}

		echo json_encode($results);
	}

	private function Upload() {
		$results = $this->post(false);

		if (count($results)) {
			$img = $results[0];
			$dynamicContentId = 0;
			
			$data = (array)$img;

			if (!isset($data["error"])) {
				unset($data["delete_url"]);
				unset($data["delete_type"]);

				$content = new CreateDynamicContentRequest();
				$content->type="IMAGE";
				$content->title=$data["name"];
				$content->status="PUBLISHED";
				$content->body = json_encode($data);

				$user = MvcSecurity::GetUserContext();

				$dynamicContentId = $this->dynamicContentService->CreateContent($content, $user->userId);

				$data["dynamic_content_id"] = $dynamicContentId;
				$results = array();
				$results[] = $data;
			}
		}

		return $this->generate_response($results, true);
	}

	public function GetImage($id) {
		$criteria = new DynamicContentSearchCriteria();
		$criteria->type = "IMAGE";
		$images = $this->adminService->SearchDynamicContent($criteria);

		$result = array();
		foreach($images->results as $image) {
			if ($image->id == $id){
				$result = json_decode($image->body);
				$result = (array)$result;
				$result["dynamic_content_id"] = $image->id;
				$results[] = $result;
			}
		}

		return $results;
	}
}

?>
<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/mailRepository.php");
require_once(Config::$site["modelsRoot"] . "services/madmimiService.php");

class MailService {
	var $mailRepository = null;
	var $madMimi = null;

	function __construct() {
		$this->mailRepository = new MailRepository();

		if (isset(Config::$site["madmimi"]["email"]) && isset(Config::$site["madmimi"]["apikey"])) {
			$this->madMimi = new MadMimi(Config::$site["madmimi"]["email"], Config::$site["madmimi"]["apikey"]);
		} 
	}

	public function SendAccountVerify($mailTo, $vars = array()) {
		$data = $this->mailRepository->GetAccountVerify();
		$this->InjectVariablesAndSend($mailTo, $data, $vars);
	}

	public function SendForgotPassword($mailTo, $vars = array()) {
		$data = $this->mailRepository->GetForgotPassword();
		$this->InjectVariablesAndSend($mailTo, $data, $vars);
	}

	public function SendSsoMemberCreated($mailTo, $vars = array()) {
		$data = $this->mailRepository->SsoMemberCreated();
		$this->InjectVariablesAndSend($mailTo, $data, $vars);
	}

	public function InjectVariablesAndSend($mailTo, $data, $vars) {
		$data["subject"] = strtr($data["subject"], $vars);
		$data["message"] = strtr($data["message"], $vars);

		$this->Send($mailTo, $data["subject"], $data["message"]);
	}

	public function Send($mailTo, $subject, $message) {
		$mailFrom = Config::$site["mailFrom"];

		if ($this->madMimi == null) {
			$headers = 'From: ' . $mailFrom . "\r\n" .
				   'Reply-To: ' . $mailFrom . "\r\n" .
			       'MIME-Version: 1.0' . "\r\n" .
			       'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
			       'X-Mailer: PHP/' . phpversion();

			mail($mailTo, $subject, $message, $headers);
		}
		else {
			$promotionName = Config::$site["madmimi"]["promotion"];

			$options = array('recipients' => $mailTo,
				             'promotion_name' => $promotionName,
				             'subject' => $subject, 
				             'from' => $mailFrom);

			$body = array('body' => $message);

			$this->madMimi->SendMessage($options, $body);
		}
	}

	public function AddToList($mailTo, $custom=array(), $list="") {
		if ($this->madMimi == null) {
			// no support
		}
		else {
			$user = $custom;
			$user["email"] = $mailTo;
			$user["add_list"] = $list ? $list : Config::$site["madmimi"]["audience_list"];

			$this->madMimi->AddUser($user);
		}
	}
}

?>
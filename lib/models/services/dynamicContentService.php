<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/dynamicContentRepository.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentKey.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentData.php");
require_once(Config::$site["modelsRoot"] . "dynamic/createDynamicContentRequest.php");
require_once(Config::$site["modelsRoot"] . "dynamic/editDynamicContentRequest.php");

class DynamicContentService {

	var $dynamicContentRepository = null;

	function __construct() {
		$this->dynamicContentRepository = new DynamicContentRepository();
	}

	function CreateContent(CreateDynamicContentRequest $content, $userId) {
		$content->createDate = @date("Y-m-d H:i:s");
		$content->publishDate = $content->status == "PUBLISHED" ? $content->createDate : "";

		$dynamicContentId = $this->dynamicContentRepository->InsertContent($content->type,
			                                                                      $content->title,
																				  $content->status,
																				  $content->seoTitle,
																				  $content->seoDescription,
																				  $content->teaser,
																				  $content->body,
																				  $content->class,
																				  $content->image,
																				  $content->sidebar,
																				  $userId,
																				  $content->createDate,
																				  $content->publishDate,
																				  $content->layout,
																				  $content->primaryRoute);

		return $dynamicContentId;
	}

	function EditContent(EditDynamicContentRequest $content, $userId) {
		if (!$content->publishDate && $content->status == "PUBLISHED")
			$content->publishDate = @date("Y-m-d H:i:s");

		$dynamicContentId = $this->dynamicContentRepository->UpdateContent($content->id,
																				  $content->type,
			                                                                      $content->title,
																				  $content->status,
																				  $content->seoTitle,
																				  $content->seoDescription,
																				  $content->teaser,
																				  $content->body,
			                                                                      $content->class,
																				  $content->image,
																				  $content->sidebar,
																				  $content->publishDate,
																				  $userId,
																				  $content->layout,
																				  $content->primaryRoute);
	}

	function DeleteContent($dynamicContentId) {
		$this->dynamicContentRepository->DeleteContent($dynamicContentId);
	}

	function GetContentKeyByRoute($route) {
		$data = $this->dynamicContentRepository->SelectContentByRoute($route);
		if ($data == null || count($data) == 0)
			return null;

		$key = new DynamicContentKey();
		$key->id = $data["dynamic_content_id"];
		$key->type = $data["type"];
		$key->description = $data["title"];
		$key->status = $data["status"];

		return $key;
	}

	function GetContentData($dynamicContentId) {
		$data = $this->dynamicContentRepository->SelectContentDataById($dynamicContentId);

		$content = new DynamicContentData();
		$content->id = $data["dynamic_content_id"];
		$content->type = $data["type"];
		$content->title = str_replace("'", "&#039;", stripslashes($data["title"]));
		$content->status = $data["status"];
		$content->createByUserName = $data["create_by_user_name"];
		$content->createDate = $data["create_date"];
		$content->publishDate = $data["publish_date"];
		$content->modifyByUserName = $data["modify_by_user_name"];
		$content->modifyDate = $data["modify_date"];
		$content->seoTitle = str_replace("'", "&#039;", stripslashes($data["seo_title"]));
		$content->seoDescription = str_replace("'", "&#039;", stripslashes($data["seo_description"]));
		$content->teaser = str_replace("'", "&#039;", stripslashes($data["teaser"]));
		$content->body = str_replace("'", "&#039;", stripslashes($data["body"]));
		$content->class = $data["class"];
		$content->image = $data["image"];
		$content->sidebar = $data["sidebar"];
		$content->layout = $data["layout"];
		$content->primaryRoute = $data["primary_route"];

		return $content;
	}

	function DoesRouteExist($route) {
		$data = $this->dynamicContentRepository->SelectContentByRoute($route);

		return ($data && isset($data["dynamic_content_id"])) ? $data["dynamic_content_id"] : 0;
	}
}

?>
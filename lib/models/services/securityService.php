<?php

require_once(Config::$site["modelsRoot"] . "services/sso/lightopenid/lightOpenId.php");
require_once(Config::$site["modelsRoot"] . "services/sso/oauth1/twitteroauth.php");
require_once(Config::$site["modelsRoot"] . "services/sso/oauth2/oAuth2Wrapper.php");
require_once(Config::$site["modelsRoot"] . "services/repositories/securityRepository.php");
require_once(Config::$site["modelsRoot"] . "security/userBase.php");
require_once(Config::$site["modelsRoot"] . "security/user.php");
require_once(Config::$site["modelsRoot"] . "security/userContext.php");
require_once(Config::$site["modelsRoot"] . "security/userAccount.php");
require_once(Config::$site["modelsRoot"] . "security/deleteUserRequest.php");
require_once(Config::$site["modelsRoot"] . "security/userSsoIdentity.php");
require_once(Config::$site["modelsRoot"] . "security/createUserRequest.php");
require_once(Config::$site["modelsRoot"] . "security/editUserRequest.php");
require_once(Config::$site["modelsRoot"] . "security/loginRequest.php");
require_once(Config::$site["modelsRoot"] . "security/ssoRequest.php");
require_once(Config::$site["modelsRoot"] . "security/createUserFromSsoRequest.php");
require_once(Config::$site["modelsRoot"] . "security/ssoProvider.php");
require_once(Config::$site["modelsRoot"] . "security/ssoIdentity.php");
require_once(Config::$site["modelsRoot"] . "security/changePasswordRequest.php");
require_once(Config::$site["modelsRoot"] . "security/resetPasswordRequest.php");
require_once(Config::$site["modelsRoot"] . "security/securityToken.php");

class SecurityService {

	var $securityRepository = null;

	function __construct() {
		$this->securityRepository = new SecurityRepository();
	}

	public static function GetHMAC($value) {
		return hash_hmac('sha1', $value, Config::$security["hmacKey"]);
	}

	public static function CreateSecurityToken($data, $lifespan) {
		$token = new SecurityToken();
		$token->data = $data;
		$token->timestamp = time() + $lifespan;
		$token->hmac = SecurityService::GetHMAC($token->data . $token->timestamp);

		return $token;
	}

	public static function IsValidSecurityToken(SecurityToken $token) {
		if ($token->timestamp && $token->hmac == SecurityService::GetHMAC($token->data . $token->timestamp)) {
			return (time() <= $token->timestamp);
		}

		return false;
	}

	public function CreateUser(CreateUserRequest $user) {
		$verificationCode = uniqid();

		$this->securityRepository->InsertUser($user->userGroupId, 
			                                  $user->userName, 
			                                  md5($user->password),
											  $user->email,
			                                  $user->firstName,
			                                  $user->lastName,
											  0,
											  $verificationCode);
		return $verificationCode;
	}

	public function CreateUserSsoIdentity(User $user, SsoIdentity $identity) {
		$this->securityRepository->InsertUserSsoIdentity($identity->providerId, $identity->identityId, $identity->userName, $identity->email, $identity->firstName, $identity->lastName, $user->userId);
	}

	public function CreateUserFromSsoIdentity(CreateUserFromSsoRequest $userIdentity, &$password) {
		$password = uniqid();

		// create user
		$this->securityRepository->InsertUser($userIdentity->userGroupId, 
			                                  $userIdentity->newUserName, 
			                                  md5($password),
											  $userIdentity->newEmail,
			                                  $userIdentity->firstName,
			                                  $userIdentity->lastName,
											  1,
											  $userIdentity->identityId);

		// simulate login to get user context
		$loginRequest = new LoginRequest();
		$loginRequest->email = $userIdentity->newEmail;
		$loginRequest->password = $password;

		$userContext = $this->GetUserContextByLogin($loginRequest);
		$userContext->ssoProviderId=$userIdentity->providerId;
		$userContext->ssoIdentityId=$userIdentity->identityId;

		// create sso identity
		$this->CreateUserSsoIdentity($userContext, $userIdentity);

		return $userContext;
	}

	public function VerifyUser($verificationCode) {
		return $this->securityRepository->UpdateUserVerificationCode($verificationCode);
	}

	public function ChangeUserPassword($userId, $password) {
		return $this->securityRepository->UpdateUserPassword($userId, md5($password));
	}

	public function GetUser($userId) {
		$user = new User();
		$data = $this->securityRepository->SelectUserByUserId($userId);

		$user = new User();
		$user->userId = $data["UserId"];
		$user->userGroupId = $data["UserGroupId"];
		$user->userName = $data["UserName"];
		$user->email = $data["Email"];
		$user->firstName = $data["FirstName"];
		$user->lastName = $data["LastName"];

		return $user;
	}

	public function GetUserSsoIdentities($userId) {
		$identities = array();

		$ids = $this->securityRepository->SelectUserSsoIdentities($userId);

		foreach($ids as $id) {
			$identity = new UserSsoIdentity();
			$identity->userId = $userId;
			$identity->providerId = $id["sso_provider_id"];
			$identity->providerName = $id["description"];
			$identity->ssoType = $id["sso_type"];
			$identity->identityId = $id["identity_id"];
			$identity->userName = $id["user_name"];
			$identity->email = $id["email"];
			$identity->firstName = $id["first_name"];
			$identity->lastName = $id["last_name"];

			$identities[] = $identity;
		}

		return $identities;
	}

	public function GetUserAccount($userId) {
		$account = new UserAccount();

		$user = $this->GetUser($userId);
		$account->userId = $user->userId;
		$account->userGroupId = $user->userGroupId;
		$account->userName = $user->userName;
		$account->email = $user->email;
		$account->firstName = $user->firstName;
		$account->lastName = $user->lastName;
		$account->ssoIdentities = $this->GetUserSsoIdentities($userId);

		return $account;
	}

	public function GetUserContextByLogin(LoginRequest $credentials) {
		$user = null;
		$data = $this->securityRepository->SelectUserByLogin($credentials->email, md5($credentials->password));

		if ($data) {
			$user = new UserContext();
			$user->userId = $data["UserId"];
			$user->userGroupId = $data["UserGroupId"];
			$user->userName = $data["UserName"];
			$user->email = $data["Email"];
			$user->firstName = $data["FirstName"];
			$user->lastName = $data["LastName"];
			$user->ssoProviderId = "";
		}

		return $user;
	}

	public function DoesUserNameExists($userName) {
		$rows = $this->securityRepository->SelectUserByUserName($userName);

		if (count($rows) > 0) {
			return $rows["UserId"];
		}

		return 0;
	}

	public function DoesUserEmailExists($email) {
		$rows = $this->securityRepository->SelectUserByEmail($email);
		if (count($rows) > 0) {
			return $rows["UserId"];
		}

		return 0;
	}

	public function DoesSsoIdentityExist($identityId) {
		$rows = $this->securityRepository->SelectSsoIdentityById($identityId);
		if (count($rows) > 0) {
			return $rows["user_id"];
		}

		return 0;
	}

	public function GetUserContextBySsoIdentity(SsoIdentity $identity, $filterUnverified=false) {
		$user = null;
		$data = $this->securityRepository->SelectUserBySsoIdentity($identity->providerId, $identity->identityId);

		if ($data) {
			$user = new UserContext();
			$user->userId = $data["UserId"];
			$user->userGroupId = $data["UserGroupId"];
			$user->userName = $data["UserName"];
			$user->email = $data["Email"];
			$user->firstName = $data["FirstName"];
			$user->lastName = $data["LastName"];
			$user->ssoProviderId=$identity->providerId;
			$user->ssoIdentityId=$identity->identityId;
		}

		return $user;
	}

	public function GetSsoProvider($providerId="") {
		$data = $this->securityRepository->SelectSsoProvider($providerId);

		$providers = array();
		foreach($data as $d) {
			$provider = new SsoProvider();
			$provider->id = $d["sso_provider_id"];
			$provider->type = $d["sso_type"];
			$provider->description = $d["description"];

			$providers[] = $provider;
		}

		return $providers;
	}

	public function RequestSsoIdentity($providerId, $state="") {
		$providers = $this->GetSsoProvider($providerId);

		switch($providers[0]->type) {
			case "oauth":
				return $this->RequestOAuth1Identity($providers[0], $state);
				break;
			case "oauth2":
				return $this->RequestOAuth2Identity($providers[0], $state);
				break;
			case "openid":
				return $this->RequestOpenIdIdentity($providers[0]);
				break;
			default:
				throw new Exception("SSO type " . $providers[0]->type . " not supported");
				break;
		};
	}

	public function UpdateUser(EditUserRequest $request) {
		// admin can set password
		if ($request->password){
			$this->ChangeUserPassword($request->userId, $request->password);
		}
		return $this->securityRepository->UpdateUser($request->userId, $request->userGroupId, $request->userName, $request->email, $request->firstName, $request->lastName, $request->isVerified);
	}

	public function UpdateUserSsoIdentity(User $user, SsoIdentity $identity) {
		$this->securityRepository->UpdateUserSsoIdentity($identity->providerId, $identity->identityId, $identity->userName, $identity->email, $identity->firstName, $identity->lastName, $user->userId);
	}

	public function DeleteUser(DeleteUserRequest $request) {
		$this->securityRepository->DeleteUser($request->userId);
	}

	public function DeleteUserSsoIdentity(UserSsoIdentity $identity) {
		$this->securityRepository->DeleteUserSsoIdentity($identity->providerId, $identity->identityId, $identity->userId);
	}

	private function RequestOAuth1Identity(SsoProvider $provider, $state="") {
		if ($provider->id != "twitter")
			throw new Exception("oAuth1 is only supported for Twitter!");

		$callback = Config::$site["http"] . "://" . Config::$site["domain"] . (isset($_SERVER["SCRIPT_URL"]) ? $_SERVER["SCRIPT_URL"] : $_SERVER["REDIRECT_URL"]);
		if ($state) $callback .= "?reply=" . $state;

		$clientId = Config::$security[$provider->id . "OAuth1"]["clientId"];
		$secretKey = Config::$security[$provider->id . "OAuth1"]["secretKey"];

		if (isset($_REQUEST["denied"]))
			throw new Exception("Access denied!");
		
		if (!isset($_REQUEST["oauth_token"])) {
			$connection = new TwitterOAuth($clientId, $secretKey);
	 
			/* Get temporary credentials. */
			$request_token = $connection->getRequestToken($callback);

			/* Save temporary credentials to session. */
			$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
			 
			/* If last connection failed don't display authorization link. */
			switch ($connection->http_code) {
			  case 200:
				/* Build authorize URL and redirect user to Twitter. */
				$url = $connection->getAuthorizeURL($token);
				header('Location: ' . $url); 
				break;
			  default:
				/* Show notification if something went wrong. */
				echo 'Could not connect to Twitter. Refresh the page or try again later.';
			}
		}
		else {
			$oauthToken = isset($_SESSION['oauth_token']) ? $_SESSION['oauth_token'] : "";
			$oauthTokenSecret = isset($_SESSION['oauth_token_secret']) ? $_SESSION['oauth_token_secret'] : "";

			unset($_SESSION['oauth_token']);
			unset($_SESSION['oauth_token_secret']);

			if ($oauthToken !== $_REQUEST['oauth_token']) {
				throw new Exception("Invalid request");
			}

			/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
			$connection = new TwitterOAuth($clientId, $secretKey, $oauthToken, $oauthTokenSecret);

			/* Request access tokens from twitter */
			$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

			/* Save the access tokens. Normally these would be saved in a database for future use. */
			//$_SESSION['access_token'] = $access_token;

			/* Remove no longer needed request tokens */
			//unset($_SESSION['oauth_token']);
			//unset($_SESSION['oauth_token_secret']);

			/* If HTTP response is 200 continue otherwise send to connect page to retry */
			if (200 == $connection->http_code) {
				$content = $connection->get('account/verify_credentials');
				
				$identity = new SsoIdentity();
				$identity->providerId = $provider->id;

				foreach($content as $key=>$value) {
					switch($key) {
						case "id":
							$identity->identityId = $value;
							break;
						case "screen_name":
							$identity->userName = $value;
							break;
						case "email":
							$identity->email = $value;
							break;
						case "name":
							$name = explode(" ", $value);
							$identity->firstName = $name[0];
							for($i = 1; $i < count($name); $i++)
								$identity->lastName .= $name[$i] . " ";
							$identity->lastName = trim($identity->lastName);
							break;
					};
				}

				return $identity;
			} else {
				throw new Exception("Invalid request, http code " . $connection->http_code);
			}
		}
	}

	private function RequestOAuth2Identity(SsoProvider $provider, $state="") {
		$domain = Config::$site["http"] . "://" . Config::$site["domain"];
		$clientId = Config::$security[$provider->id . "OAuth2"]["clientId"];
		$secretKey = Config::$security[$provider->id . "OAuth2"]["secretKey"];

		$service = new OAuth2Wrapper($domain, $clientId, $secretKey);
		$endpoint = $this->securityRepository->SelectSsoEndpoint($provider->id);

		if (isset($endpoint["authorization"])) {
			if (isset($_REQUEST["error"]))
				throw new Exception($_REQUEST["error"]);

			if (!$service->status) {
				$url = $service->RequestAuthorizationUrl($endpoint["authorization"], array("scope"=>$endpoint["scope"], "state"=>$state));
				header('Location: ' . $url);
			}
			else {
				$service->RequestAccessToken($endpoint["token"]);
				$service->RequestIdentity($endpoint["identity"]);

				if ($service->status == "identity") {
					$identity = new SsoIdentity();
					$identity->providerId = $provider->id;

					foreach($service->identity as $key=>$value) {
						switch($key) {
							case "id":
								$identity->identityId = $value;
								break;
							case "email":
								$identity->userName = $value;
								$identity->email = $value;
								break;
							case "name":
								$name = explode(" ", $value);
								$identity->firstName = $name[0];
								for($i = 1; $i < count($name); $i++)
									$identity->lastName .= $name[$i] . " ";
								$identity->lastName = trim($identity->lastName);
								break;
							case "given_name":
								$identity->firstName = $value;
								break;
							case "family_name":
								$identity->lastName = $value;
								break;
						};
					}

					return $identity;
				}
				else {
					throw new Exception("Authentication is invalid");
				}
			}
		}
		else {
			throw new Exception("Could not find oauth endpoint for " . $provider->id);
		}
	}

	private function RequestOpenIdIdentity(SsoProvider $provider) {
		$endpoint = $this->securityRepository->SelectSsoEndpoint($provider->id);

		// Google OpenId: endpoint = https://www.google.com/accounts/o8/id, required = contact/email;namePerson/first;namePerson/last
		if (isset($endpoint["identity"])) {
			try {
				$service = new LightOpenID(Config::$site["http"] . "://" . Config::$site["domain"]);
				if(!$service->mode) {
					$service->identity = $endpoint["identity"];
					$service->required = explode(";", $endpoint["scope"]);
					//$service->optional = explode(";", $endpoint["optional"]);
					header('Location: ' . $service->authUrl());
				} elseif($service->mode == 'cancel') {
					throw new Exception("User has canceled authentication!");
				} else {
					// prevent refresh or changing query string data
					if ($service->validate()) {
						$identity = new SsoIdentity();
						$identity->providerId = $provider->id;
						$identity->identityId = $service->identity;

						foreach($service->getAttributes() as $key=>$value) {
							switch($key) {
								case "contact/email":
									$identity->userName = $value;
									$identity->email = $value;
									break;
								case "namePerson":
									$name = explode(" ", $value);
									$identity->firstName = $name[0];
									for($i = 1; $i < count($name); $i++)
										$identity->lastName .= $name[$i] . " ";
									$identity->lastName = trim($identity->lastName);
									break;
								case "namePerson/first":
									$identity->firstName = $value;
									break;
								case "namePerson/last":
									$identity->lastName = $value;
									break;
							};
						}

						return $identity;
					}
					else {
						throw new Exception("Authentication is invalid");
					}
				}
			} catch(ErrorException $e) {
				throw new Exception("LightOpenID threw the following exception: " . $e->getMessage());
			}
		}
		else {
			throw new Exception("Could not find openId endpoint for " . $provider->id);
		}
	}
}

?>
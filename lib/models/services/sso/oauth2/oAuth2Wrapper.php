<?php
// https://code.google.com/apis/console/#project:280626581494:access
// http://code.google.com/p/google-api-php-client/wiki/OAuth2
// https://github.com/adoy/PHP-OAuth2
// https://developers.google.com/accounts/docs/OAuth2
// https://developers.google.com/accounts/docs/OAuth2InstalledApp
// https://developers.google.com/accounts/docs/OAuth2UserAgent

// https://developers.facebook.com/apps
// http://developers.facebook.com/docs/authentication/
// http://developers.facebook.com/docs/reference/dialogs/oauth/
// http://developers.facebook.com/docs/authentication/permissions/

require('Client.php');
require("GrantType/IGrantType.php");
require("GrantType/AuthorizationCode.php");

class OAuth2Wrapper {

	var $status = "";
	var $authorizationCode = null;
	var $client = null;
	var $redirectUri = "";

	var $identity = array();

	function __construct($domain, $clientId, $secretKey) {
		$this->client = new OAuth2\Client($clientId, $secretKey);
		$this->redirectUri = $domain . (isset($_SERVER["SCRIPT_URL"]) ? $_SERVER["SCRIPT_URL"] : $_SERVER["REDIRECT_URL"]);

		if (isset($_REQUEST["code"])) {
			$this->authorizationCode = $_REQUEST["code"];
			$this->status = "notoken";
		}
	}

	function RequestAuthorizationUrl($endpoint, $scope=array()) {
		// Google: scope space delimited
		// Facebook: scope comma delimitedz
		return $this->client->getAuthenticationUrl($endpoint, $this->redirectUri, $scope);
	}

	function RequestAccessToken($tokenEndpoint) {
		$params = array('code' => $this->authorizationCode, 'redirect_uri' => $this->redirectUri);
		$response = $this->client->getAccessToken($tokenEndpoint, 'authorization_code', $params);

		try {
			$token = null;

			if (!isset($response["result"]))
				throw new Exception("No 'result' key found in response.");

			// Google: $response["result"]["access_token"] = ...(array)
			// Facebook: $resonse["result"] = "access_tokent=...&expire=..." (query string format)
			if (is_array($response["result"])) {
				if (!isset($response["result"]["access_token"]))
					throw new Exception("No 'access_token' key found in response.");
				$token = $response["result"]["access_token"];
			}
			else {
				parse_str($response["result"]);
				if (!isset($access_token))
					throw new Exception("No 'access_token' parsed from response.");

				$token = $access_token;
			}

			if ($token == null)
				throw new Exception("unable to retrieve authorization token");

			$this->client->setAccessToken($token);
			$this->status = "token";
		}
		catch(Exception $e) {
			throw new Exception("Invalid Authorization while requesting access token: " . $e);
		}
	}

	function RequestIdentity($identityEndpoint) {
		$response = $this->client->fetch($identityEndpoint);

		$this->status = "identity";

		if (!isset($response["result"]["id"]))
			throw new Exception("Invalid Authorization while requesting identity");

		$this->identity = $response["result"];
	}
}

?>
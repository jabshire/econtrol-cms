<?php

require_once(Config::$site["modelsRoot"] . "services/securityService.php");
require_once(Config::$site["modelsRoot"] . "services/repositories/dynamicContentRepository.php");
require_once(Config::$site["modelsRoot"] . "searchCriteria.php");
require_once(Config::$site["modelsRoot"] . "searchResults.php");
require_once(Config::$site["modelsRoot"] . "admin/userSearchCriteria.php");
require_once(Config::$site["modelsRoot"] . "admin/editUserAdminRequest.php");
require_once(Config::$site["modelsRoot"] . "services/dynamicContentService.php");
require_once(Config::$site["modelsRoot"] . "admin/dynamicContentSearchCriteria.php");
require_once(Config::$site["modelsRoot"] . "dynamic/dynamicContentData.php");

class AdminService extends SecurityService
{

    var $dynamicContentService = null;
    var $dynamicContentRepository = null;

    function __construct()
    {
        parent::__construct();
        $this->dynamicContentService = new DynamicContentService();
        $this->dynamicContentRepository = new DynamicContentRepository();
    }

    public function SearchUsers(UserSearchCriteria $criteria)
    {
        $retval = new SearchResults();
        $retval->criteria = $criteria;
        $retval->results = array();

        $count = $this->securityRepository->SearchUsersCount($criteria->userGroupId, $criteria->userName, $criteria->email, $criteria->firstName, $criteria->lastName, $criteria->isVerified);
        $retval->totalResults = $count["count"];

        $orderBy = "";
        if($criteria->orderBy)
            $orderBy = $criteria->orderBy . " " . $criteria->orderByDirection;
        
        $data = $this->securityRepository->SearchUsers($criteria->userGroupId, $criteria->userName, $criteria->email, $criteria->firstName, $criteria->lastName, $criteria->isVerified, $criteria->Skip(), $criteria->resultsPerPage, $orderBy);

        foreach ($data as $d) {
            $user = new EditUserAdminRequest();
            $user->userId = $d["UserId"];
            $user->userGroupId = $d["UserGroupId"];
            $user->userName = $d["UserName"];
            $user->oldEmail = $d["Email"];
            $user->email = $d["Email"];
            $user->firstName = $d["FirstName"];
            $user->lastName = $d["LastName"];
            $user->isVerified = $d["IsVerified"];
            $user->createDate = $d["CreateDate"];
            $user->ssoProviderId = $d["sso_provider_id"];
            $user->ssoEmail = $d["ssoEmail"];
            $user->ssoIdentities = $this->GetUserSsoIdentities($user->userId);

            $retval->results[] = $user;
        }

        return $retval;
    }

    public function GetUser($userId)
    {
        $user = new EditUserAdminRequest();

        $data = $this->securityRepository->SelectUserByUserId($userId);
        $user->userId = $data["UserId"];
        $user->userGroupId = $data["UserGroupId"];
        $user->userName = $data["UserName"];
        $user->oldEmail = $data["Email"];
        $user->email = $data["Email"];
        $user->firstName = $data["FirstName"];
        $user->lastName = $data["LastName"];
        $user->isVerified = $data["IsVerified"];
        $user->createDate = $data["CreateDate"];
        $user->ssoIdentities = $this->GetUserSsoIdentities($userId);

        return $user;
    }

    public function SearchDynamicContent(DynamicContentSearchCriteria $criteria)
    {
        $retval = new SearchResults();
        $retval->criteria = $criteria;
        $retval->results = array();

        $count = $this->dynamicContentRepository->SearchDynamicContentCount($criteria->type, $criteria->title, $criteria->status, $criteria->seoTitle, $criteria->seoDescription, $criteria->createByUserName, $criteria->modifyByUserName);

        $retval->totalResults = $count["count"];

        $orderBy = "";
        if($criteria->orderBy)
            $orderBy = $criteria->orderBy . " " . $criteria->orderByDirection;
        
        $data = $this->dynamicContentRepository->SearchDynamicContent($criteria->type, $criteria->title, $criteria->status, $criteria->seoTitle, $criteria->seoDescription, $criteria->createByUserName, $criteria->modifyByUserName, $criteria->Skip(), $criteria->resultsPerPage, $orderBy);

        foreach ($data as $d) {
            $content = new DynamicContentData();

            $content->id = $d["dynamic_content_id"];
            $content->type = $d["type"];
            $content->title = stripslashes($d["title"]);
            $content->status = $d["status"];
            $content->createByUserName = $d["create_by_user_name"];
            $content->createDate = $d["create_date"];
            $content->publishDate = $d["publish_date"];
            $content->modifyByUserName = $d["modify_by_user_name"];
            $content->modifyDate = $d["modify_date"];
            $content->seoTitle = stripslashes($d["seo_title"]);
            $content->seoDescription = stripslashes($d["seo_description"]);
            $content->teaser = str_replace("'", "&#039;", stripslashes($d["teaser"]));
            $content->body = str_replace("'", "&#039;", stripslashes($d["body"]));
            $content->image = $d["image"];
            $content->sidebar = $d["sidebar"];
            $content->layout = $d["layout"];
            $content->primaryRoute = $d["primary_route"];


            $retval->results[] = $content;
        }

        return $retval;
    }

    function GetDynamicContent($id)
    {
        return $this->dynamicContentService->GetContentData($id);
    }

    function DeleteDynamicContent($id)
    {
        return $this->dynamicContentService->DeleteContent($id);
    }

    function CreateDynamicContent(CreateDynamicContentRequest $request, $userId)
    {
        return $this->dynamicContentService->CreateContent($request, $userId);
    }

    function EditDynamicContent(CreateDynamicContentRequest $request, $userId)
    {
        return $this->dynamicContentService->EditContent($request, $userId);
    }

}
<?php

class ChangePasswordRequest implements IMvcModelValidator {
	var $userId = 0;
	var $currentPassword = "";
	var $newPassword = "";
	var $confirmNewPassword = "";

	public function ValidateProperty($name) {
		switch($name) {
			case "currentPassword":
				if (!trim($this->currentPassword)) return "Please enter your current password";
				if (!$this->ValidateCurrentPassword()) return "Your current password is invalid";
				break;
			case "newPassword":
				if (!trim($this->newPassword)) return "Please enter a new password";
				if ($this->newPassword == $this->currentPassword) return "You did not change your password";
				break;
			case "confirmNewPassword":
				if (!trim($this->confirmNewPassword)) return "Please confirm your new password";
				if ($this->confirmNewPassword != $this->newPassword) return "Your new passwords do not match";
				break;
			default:
				return "";
		}
	}

	public function ValidateModel() {
		return "";
	}

	private function ValidateCurrentPassword() {
		$userContext = MvcSecurity::GetUserContext();
		$service = new SecurityService();

		$login = new LoginRequest();
		$login->email = $userContext->email;
		$login->password = $this->currentPassword;

		$user = $service->GetUserContextByLogin($login);

		return ($user != null);
	}
}

?>
<?php

class ResetPasswordRequest implements IMvcModelValidator {
	var $userId = 0;
	var $password = "";
	var $confirmPassword = "";

	public function ValidateProperty($name) {
		switch($name) {
			case "password":
				if (!trim($this->password)) return "Please enter a new password";
				break;
			case "confirmPassword":
				if (!trim($this->confirmPassword)) return "Please confirm your new password";
				if ($this->confirmPassword != $this->password) return "Your new passwords do not match.";
				break;
			default:
				return "";
		}
	}

	public function ValidateModel() {
		return "";
	}
}

?>
<?php

class EditUserRequest extends User implements IMvcModelValidator {
	var $oldEmail = "";
	var $isVerified = null;

	public function ValidateProperty($propertyName) {
		switch ($propertyName) {
			case "email":
				if ($this->email == "") return "Pleae enter a valid e-mail address.";
				if ($this->email != $this->oldEmail && $this->ValidateEmailDoesNotExist($this->email) == false) return "This e-mail address is already associated with an account.";
				break;
			case "userGroupId":
				$context = MvcSecurity::GetUserContext();
				if ($context == null) return "Unable to validate user group. User context not found.";
				if ($this->userGroupId > $context->userGroupId) return "Invalid user group (exceeds that of user context).";
				break;
		};

		return "";
	}

	public function ValidateModel() {
		if (Config::$security["emailIsUsername"] === true)
			$this->userName = $this->email;
			
		return "";
	}
}

?>
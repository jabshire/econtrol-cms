<?php

// Created so that User and SsoIdentity could share validation rule logic

class UserBase {
	var $userName = "";
	var $email = "";
	var $firstName = "";
	var $lastName = "";
	var $password = ""; // Added for admin / password setting

	// ***********************************************************************
	// For classes that inherit UserBase and implement IMvcModelValidator:

	protected function ValidateUserNameDoesNotExist($userName) {
		if (!$userName) return true;

		$service = new SecurityService();
		return ($service->DoesUserNameExists($userName) == false);
	}

	protected function ValidateEmailDoesNotExist($email) {
		if (!$email) return true;

		$service = new SecurityService();
		return ($service->DoesUserEmailExists($email) == false);
	}

	// ***********************************************************************
}

?>
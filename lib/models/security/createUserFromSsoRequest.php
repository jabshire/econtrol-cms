<?php

class CreateUserFromSsoRequest extends SsoIdentity implements IMvcModelValidator {
	var $userGroupId = 0;
	var $newUserName = "";
	var $newEmail = "";
	var $reply = "";

	function __construct(SsoIdentity $identity=null) {
		if ($identity != null) {
			$this->providerId=$identity->providerId;
			$this->identityId=$identity->identityId;
			$this->userName=$identity->userName;
			$this->newUserName=$identity->userName;
			$this->email=$identity->email;
			$this->newEmail = $identity->email;
			$this->firstName=$identity->firstName;
			$this->lastName=$identity->lastName;
		}
	}

	function ValidateProperty($name) {
		switch($name) {
			case "identityId":
				if ($this->ValidateIdentityIdDoesNotExist($this->identityId) == false) return "This " . ucfirst($this->providerId) . " identity is already associated with an existing " . Config::$site["name"] . " account.";
				break;
			case "newUserName":
				if (!trim($this->newUserName)) return "Please enter a valid user name.";
				if ($this->ValidateUserNameDoesNotExist($this->newUserName) == false) return "The user name you selected ($this->newUserName) matches the user name of an existing " . Config::$site["name"] . " account.";
				break;
			case "email":
				if ($this->ValidateEmailDoesNotExist($this->email) == false) return "The email address of this identity ($this->email) matches the email of an existing " . Config::$site["name"] . " account.";
				break;
			case "newEmail":
				if (!trim($this->email)) {
					if (!trim($this->newEmail)) return "Please enter a valid email address";
					if ($this->ValidateEmailDoesNotExist($this->newEmail) == false) return "The email you selected ($this->newEmail) matches the email of an existing " . Config::$site["name"] . " account.";
				}
				break;
			default:
				return "";
				break;
		};
	}

	function ValidateModel() {
		return "";
	}

	function ValidateIdentityIdDoesNotExist($identityId) {
		$securityService = new SecurityService();
		$user = $securityService->DoesSsoIdentityExist($identityId);
		return !$user;
	}
}

?>
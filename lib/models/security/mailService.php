<?php

require_once(Config::$site["modelsRoot"] . "services/repositories/mailRepository.php");

class MailService {
	var $mailRepository = null;

	function __construct() {
		$this->mailRepository = new MailRepository();
	}

	public function SendAccountVerifyMessage($mailTo, $url) {
		$data = $this->mailRepository->GetAccountVerifyMessage();

		$data["subject"] = str_replace("[siteName]", Config::$site["name"], $data["subject"]);
		$data["message"] = str_replace("[url]", Config::$site["http"] . "://" . Config::$site["domain"] . $url, $data["message"]);

		$this->Send($mailTo, $data["subject"], $data["message"]);
	}

	public function SendForgotPasswordMessage($mailTo, $url) {
		$data = $this->mailRepository->GetForgotPasswordMessage();

		$data["subject"] = str_replace("[siteName]", Config::$site["name"], $data["subject"]);
		$data["message"] = str_replace("[url]", Config::$site["http"] . "://" . Config::$site["domain"] . $url, $data["message"]);

		$this->Send($mailTo, $data["subject"], $data["message"]);
	}

	public function Send($mailTo, $subject, $message) {
		$headers = 'From: ' . Config::$site["mailFrom"] . "\r\n" .
				   'Reply-To: ' . Config::$site["mailFrom"] . "\r\n" .
			       'MIME-Version: 1.0' . "\r\n" .
			       'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
			       'X-Mailer: PHP/' . phpversion();

		mail($mailTo, $subject, $message, $headers);
	}
}

?>
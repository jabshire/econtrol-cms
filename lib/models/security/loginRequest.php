<?php

class LoginRequest implements IMvcModelValidator {
	var $email = "";
	var $password = "";
	var $remember = false;
	var $reply = "";

	public function ValidateProperty($name) {
		switch($name) {
			case "email":
				if (trim($this->email) == "") return "Enter your email";
				break;
			case "password":
				if (trim($this->password) == "") return "Enter your password";
				break;
		};

		return "";
	}

	public function ValidateModel() {
		return "";
	}
}

?>
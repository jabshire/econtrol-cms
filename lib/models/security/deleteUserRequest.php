<?php

class DeleteUserRequest implements IMvcModelValidator {
	var $userId;

	public function ValidateProperty($propertyName) {
		switch ($propertyName) {
			case "userId":
				$context = MvcSecurity::GetUserContext();
				if ($context == null) return "User context not found";
				// extra security
				if ($context->userGroupId < Config::$security["userGroups"]["admin"]) return "You're not allowed to delete users";
				if ($context->userId == $this->userId) return "You're not allowed to delete yourself";
				break;
		};

		return "";
	}

	public function ValidateModel() {
		return "";
	}
}

?>
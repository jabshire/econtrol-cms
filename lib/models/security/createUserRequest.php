<?php

class CreateUserRequest extends User implements IMvcModelValidator {
	var $password = "";
	var $confirmPassword = "";
	var $captcha = "";

	public function ValidateProperty($propertyName) {
		switch ($propertyName) {
			case "email":
				if ($this->email == "") return "Please enter a valid email address.";
				if ($this->ValidateEmailDoesNotExist($this->email) == false) return "This email address is already associated with an account.";
				break;
			case "password":
				if ($this->password == "") return "Please enter a password.";
				break;
			case "confirmPassword":
				if ($this->confirmPassword == "") return "Please confirm your password.";
				if ($this->confirmPassword != $this->password) return "Your passwords do not match.";
				break;
			case "userName":
				if (Config::$security["emailIsUsername"] === true) return "";
				if ($this->userName == "") return "Please enter a user name.";
				if ($this->ValidateUserNameDoesNotExist($this->userName) == false) return "This user name has already been taken.";
				break;
			case "captcha":
				if (Config::$security["useCaptcha"] !== true) return "";
				if ($this->captcha == "") return "Please type the captcha text.";
				if (!isset($_SESSION["captcha"]) || $this->captcha != $_SESSION["captcha"]) return "You did not type the captcha text correctly.";
				break;
		};

		return "";
	}

	public function ValidateModel() {
		if (Config::$security["emailIsUsername"] === true)
			$this->userName = $this->email;
		return "";
	}
}

?>
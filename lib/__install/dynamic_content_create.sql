/*
SQLyog Community v8.82 
MySQL - 5.5.9 : Database - mvcsitetemplate
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `dynamic_content` */

DROP TABLE IF EXISTS `dynamic_content`;

CREATE TABLE `dynamic_content` (
  `dynamic_content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('PAGE','BLOG','MENU','WIDGET','IMAGE') NOT NULL DEFAULT 'PAGE',
  `title` varchar(50) NOT NULL,
  `status` enum('DRAFT','PRIVATE','PUBLISHED') DEFAULT 'DRAFT',
  `create_by_user_id` int(11) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `modify_by_user_id` int(11) unsigned NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dynamic_content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `dynamic_content_data` */

DROP TABLE IF EXISTS `dynamic_content_data`;

CREATE TABLE `dynamic_content_data` (
  `dynamic_content_id` int(11) unsigned NOT NULL,
  `seo_title` varchar(50) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `teaser` text,
  `body` text,
  `image` int(11) unsigned DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `sidebar` varchar(255) DEFAULT NULL,
  `layout` varchar(50) NOT NULL,
  KEY `FK_dynamic_content_data` (`dynamic_content_id`),
  CONSTRAINT `FK_dynamic_content_data` FOREIGN KEY (`dynamic_content_id`) REFERENCES `dynamic_content` (`dynamic_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `dynamic_content_routes` */

DROP TABLE IF EXISTS `dynamic_content_routes`;

CREATE TABLE `dynamic_content_routes` (
  `dynamic_content_id` int(11) unsigned NOT NULL,
  `route` varchar(255) NOT NULL,
  `is_primary` bit(1) NOT NULL,
  UNIQUE KEY `dynamic_content_route` (`route`),
  KEY `FK_dynamic_content_routes` (`dynamic_content_id`),
  CONSTRAINT `FK_dynamic_content_routes` FOREIGN KEY (`dynamic_content_id`) REFERENCES `dynamic_content` (`dynamic_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

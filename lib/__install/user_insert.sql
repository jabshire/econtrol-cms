INSERT INTO UserGroup(UserGroupId, GroupName)
               VALUES(1, "Guest"),
                     (2, "Member"),
                     (99, "Admin"),
                     (999, "Super");

INSERT INTO User(UserGroupId, UserName, Password, Email, FirstName, LastName, IsVerified, VerificationCode, CreateDate)
          VALUES(99, 'jabshire@resolutecreative.com', '55855b07ab485fc5ff75b53c90487121', 'jabshire@resolutecreative.com', 'Jacob', 'Abshire', 1, 'none', now());
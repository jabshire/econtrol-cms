/*
SQLyog Community v8.82 
MySQL - 5.5.9 : Database - mvcsitetemplate
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `User` */

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `UserId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserGroupId` int(11) unsigned DEFAULT '2',
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `IsVerified` tinyint(1) NOT NULL DEFAULT '0',
  `VerificationCode` varchar(255) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `LastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `User.UserName` (`UserName`),
  KEY `FK_User` (`UserGroupId`),
  KEY `User.Email` (`Email`),
  KEY `User.UserNamePassword` (`UserName`,`Password`),
  CONSTRAINT `FK_User` FOREIGN KEY (`UserGroupId`) REFERENCES `UserGroup` (`UserGroupId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `UserGroup` */

DROP TABLE IF EXISTS `UserGroup`;

CREATE TABLE `UserGroup` (
  `UserGroupId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(50) NOT NULL,
  PRIMARY KEY (`UserGroupId`),
  UNIQUE KEY `UserGroup.GroupName` (`GroupName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

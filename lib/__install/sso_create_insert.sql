/*
SQLyog Community v8.82 
MySQL - 5.5.9 : Database - mvcsitetemplate
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sso_endpoint` */

DROP TABLE IF EXISTS `sso_endpoint`;

CREATE TABLE `sso_endpoint` (
  `sso_provider_id` varchar(50) NOT NULL,
  `authorization` varchar(255) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  PRIMARY KEY (`sso_provider_id`),
  CONSTRAINT `FK_sso_endpoint` FOREIGN KEY (`sso_provider_id`) REFERENCES `sso_provider` (`sso_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sso_endpoint` */

insert  into `sso_endpoint`(`sso_provider_id`,`authorization`,`scope`,`token`,`identity`) values ('aol','','contact/email','','https://www.aol.com'),('facebook','https://graph.facebook.com/oauth/authorize','email','https://graph.facebook.com/oauth/access_token','https://graph.facebook.com/me'),('google','https://accounts.google.com/o/oauth2/auth','https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile','https://accounts.google.com/o/oauth2/token','https://www.googleapis.com/oauth2/v2/userinfo'),('wordpress','','contact/email','','http://YOURBLOG.wordpress.com'),('yahoo','','contact/email;namePerson;namePerson/friendly','','https://me.yahoo.com');

/*Table structure for table `sso_identity` */

DROP TABLE IF EXISTS `sso_identity`;

CREATE TABLE `sso_identity` (
  `sso_provider_id` varchar(50) NOT NULL,
  `identity_id` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`sso_provider_id`,`identity_id`),
  UNIQUE KEY `sso_identity.identity_id-user_id` (`identity_id`,`user_id`),
  UNIQUE KEY `sso_identity.identity_id` (`identity_id`),
  KEY `sso_identity.user_id` (`user_id`),
  CONSTRAINT `FK_sso_identity` FOREIGN KEY (`sso_provider_id`) REFERENCES `sso_provider` (`sso_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sso_identity` */

/*Table structure for table `sso_provider` */

DROP TABLE IF EXISTS `sso_provider`;

CREATE TABLE `sso_provider` (
  `sso_provider_id` varchar(50) NOT NULL,
  `sso_type` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT '99',
  `enabled` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`sso_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sso_provider` */

insert  into `sso_provider`(`sso_provider_id`,`sso_type`,`description`,`priority`,`enabled`) values ('aol','openid','AOL',9,0),('blogger','openid','Blogger',5,0),('facebook','oauth2','Facebook',2,1),('flickr','openid','Flickr',6,0),('google','oauth2','Google',1,1),('myopenid','openid','myOpenID',8,0),('myspace','openid','MySpace',10,0),('twitter','oauth','Twitter',3,1),('wordpress','openid','Wordpress',7,0),('yahoo','openid','Yahoo',4,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

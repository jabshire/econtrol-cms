<?php

//***************************************************************************************************************
// defaults

MvcRouter::AddReRoute("*/", "*/index/"); // index is default action for all controllers
MvcRouter::AddReRoute("", (Config::$site["homePage"])? Config::$site["homePage"]:"home/index/" ); // set default home page

//***************************************************************************************************************
// security

MvcRouter::AddReRoute("member/login/*", "security/*");
MvcRouter::AddReRoute("member/changepassword/*", "security/*");
MvcRouter::AddReRoute("member/forgotpassword/*", "security/*");
MvcRouter::AddReRoute("member/resetpassword/*", "security/*");

//***************************************************************************************************************
// admin

MvcRouter::AddReRoute("admin/page/add/","admin/addpage/");
MvcRouter::AddReRoute("admin/page/edit/*","admin/editpage/");
MvcRouter::AddReRoute("admin/page/delete/*","admin/deletepage/");

MvcRouter::AddReRoute("admin/menu/add/","admin/addmenu/");
MvcRouter::AddReRoute("admin/menu/edit/*","admin/editmenu/");
MvcRouter::AddReRoute("admin/menu/delete/*","admin/deletemenu/");

MvcRouter::AddReRoute("admin/widget/add/","admin/add-widget/");
MvcRouter::AddReRoute("admin/widget/edit/*","admin/edit-widget/");
MvcRouter::AddReRoute("admin/widget/delete/*","admin/delete-widget/");

?>
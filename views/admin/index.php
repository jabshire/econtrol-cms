<?php
$__pageTitle = "Home";
ob_start();
?>

<div class="content">
	<h1><?php echo Config::$site["name"]; ?> Admin</h1>

	<ul>
		<li><?php echo MvcHtml::ActionLink("Users", "admin", "users"); ?></li>
		<li><?php echo MvcHtml::ActionLink("Pages", "admin", "pages"); ?></li>
		<li><?php echo MvcHtml::ActionLink("Site Home", "",""); ?></li>
		<li><?php echo MvcHtml::ActionLink("Logout", "Member","Logout"); ?></li>
	</ul>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
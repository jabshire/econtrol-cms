<?php
$__pageTitle = "Edit Widget";
ob_start();
?>

<h1>Edit Widget <small><?php echo $__model->title; ?></small></h1>

<hr>

<section class="messages">
	<?php if ($__viewContext->modelState->IsValid() == false) { ?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true, "<b>Error!</b> Something is wrong in the form."); ?>
		</div>
	<?php } ?>

	<?php if (isset($__viewContext->viewData["updated"])) { ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<b>Sucess!</b> The widget has been updated.
		</div>
	<?php } ?>
</section>

<section class="row">
	
	<!--main-->
	<div class="span12">
		
		<?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true); ?>
		
		<!--form-->
		<form id="editForm" action="<?php echo MvcRouter::GetActionUrl('admin', 'widget', 'edit'); ?>" method="post" class="row layout-3">
		
			<?php echo MvcHtml::RenderPartialView($__viewContext, 'inc/widget.form', $__model); ?>
			
			<!--post-actions-->
			<section class="span12 page-info">
				<div class="well well-small">
					<div class="row-fluid">
						<div class="span4">
							<b>Created By:</b><br>
							<?php echo $__model->createByUserName; ?><br>
							<?php echo $__model->createDate; ?>
						</div>
						<div class="span4">
							<b>Modified By:</b><br>
							<?php echo $__model->modifyByUserName; ?><br>
							<?php echo $__model->modifyDate; ?>
						</div>
						<div class="span4">
							<b>Published On:</b><br>
							<?php echo $__model->publishDate; ?>
						</div>
					</div>
				</div>
			</section>
			
			<!--post-actions-->
			<section class="span12 page-save">
				<div class="well well-small">
					<button type="submit" class="btn btn-primary" onclick="document.getElementById('editForm').submit(); return false;">Save</button>
					<button type="submit" class="btn" onclick="window.location='<?php echo MvcRouter::GetActionUrl('admin', 'widgets'); ?>'; return false;">Cancel</button>
					<button type="submit" class="btn btn-danger" onclick="Delete(); return false;">Delete</button>
				</div>
			</section>
			
		</form>
		<? include_once('inc/imageModal.php') ?>
	
	</div>
	
</section>

<script type="text/javascript">
function Delete(title, id) {
	if (confirm("Are you sure you want to delete '<?php echo $__model->title; ?>'?"))
		window.location = "<?php echo MvcRouter::GetActionUrl('admin', 'widget/delete/' . $__model->id); ?>";
}
</script>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
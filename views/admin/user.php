<?php
$__pageTitle = "Edit User";
ob_start();
?>

<h1>Edit User <small><?php echo $__model->email; ?></small></h1>

<hr>

<section class="row">
	
	<!--main-->
	<div class="span12">
		
		<!--form-->
		<form action="<?php echo MvcRouter::GetActionUrl("admin","user"); ?>" method="post" class="row layout-3" id="editForm">
		
			<!--hidden-->
			<?php echo MvcHtml::HiddenFor($__model, 'userId'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'userName'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'oldEmail'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'email'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'firstName'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'lastName'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'isVerified'); ?>
			<?php echo MvcHtml::HiddenFor($__model, 'userGroupId'); ?>
			<!--/hidden-->
			
			<!--post-content-->
			<section class="span8 user-content">
				<div class="well well-small">
					<fieldset>
						<?php echo MvcHtml::TextBoxFor($__model, 'email', array("class"=>'input-block-level', 'placeholder'=>'Email')); ?>
					</fieldset>
					<fieldset>
						<?php echo MvcHtml::TextBoxFor($__model, 'firstName', array("class"=>'input-block-level', 'placeholder'=>'First Name')); ?>
					</fieldset>
					<fieldset>
						<?php echo MvcHtml::TextBoxFor($__model, 'lastName', array("class"=>'input-block-level', 'placeholder'=>'First Name')); ?>
					</fieldset>
				</div>
				<div class="well well-small">
					<fieldset>
						<p><i>Leave blank to not set.</i></p>
						<input type="text" id="password" name="password" value="" class="input-block-level" placeholder="Reset Password">
					</fieldset>
				</div>
			</section>
			
			<!--post-info-->
			<section class="span4 post-info">
				<div class="well well-small">
					
					<fieldset style="margin-bottom:10px;">
						<div class="btn-group" id="selectUserGroup">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"><span class="name"><?php
								foreach(Config::$security["userGroups"] as $groupName => $groupId){
									if ($groupId == $__model->userGroupId) echo ucfirst($groupName);
								}
								?></span> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php foreach(Config::$security["userGroups"] as $name=>$id) { ?>
								<?php if ($id > $__userContext->userGroupId) continue; ?>
								<li class="<?php echo ($id == $__model->userGroupId ? 'active':'' ) ?>"><a href="<?php echo $id ?>" title=""><?php echo ucfirst($name) ?></a></li>
								<?php } ?>
							</ul>
						</div>
						<span class="help-inline">User Group</span>
					</fieldset>
					
					<fieldset>
						<div class="btn-group" id="selectIsVerified">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="0"><span class="name"><?php echo ($__model->isVerified == 1)? 'Yes' : 'No'; ?></span> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li class="<?php if ($__model->isVerified == 1){ echo 'active'; } ?>"><a href="1">Yes</a></li>
								<li class="<?php if ($__model->isVerified == 0){ echo 'active'; } ?>"><a href="0">No</a></li>
							</ul>
						</div>
						<span class="help-inline">Is user verified?</span>
					</fieldset>
					
				</div>
			</section>
			<script type="text/javascript">
			$(function(){
				// USERGROUP SELECT
				$('#selectUserGroup ul.dropdown-menu li a').click(function(){
					var userGroupId = $(this).attr('href');
					var userGroupName = $(this).html();
					$('#selectUserGroup a.dropdown-toggle .name').text(userGroupName);
					$('#userGroupId').val(userGroupId);
					$('#selectUserGroup').removeClass('open');
					$('#selectUserGroup ul.dropdown-menu li').removeClass('active');
					$(this).parent().addClass('active');
					return false;
				});
				// VERFIFIED SELECT
				$('#selectIsVerified ul.dropdown-menu li a').click(function(){
					var isVerifiedId = $(this).attr('href');
					var isVerifiedLabel = $(this).html();
					$('#selectIsVerified a.dropdown-toggle .name').text(isVerifiedLabel);
					$('#isVerified').val(isVerifiedId);
					$('#selectIsVerified').removeClass('open');
					$('#selectIsVerified ul.dropdown-menu li').removeClass('active');
					$(this).parent().addClass('active');
					return false;
				});
			});
			</script>
			
			<?php if (count($__model->ssoIdentities) > 0) {?>
			<!--post-sso-->
			<section class="span4 post-sso">
				<div class="well well-small">
					
					<?php foreach($__model->ssoIdentities as $identity) { ?>
					<fieldset>
						<?php $imgRoot = Config::$themes["default"]["root"] . "img/sso/alt/"; ?>
						<img src="<?php echo $imgRoot . $identity->providerId ?>.gif" class="pull-right img-rounded">
						<p>
							<b><?php echo $identity->providerName ?></b><br>
							<?php echo $identity->identityId ?><br>
							<?php if ($identity->userName != $identity->email){ echo $identity->userName . '<br>'; } ?>
							<?php if ($identity->email){ echo $identity->email . '<br>'; } ?>
						</p>
					</fieldset>
					<?php } ?>
					
				</div>
			</section>
			<?php } ?>
			
			<!--post-actions-->
			<section class="span12 user-save">
				<div class="well well-small">
					<?php if ($__userContext->userId != $__model->userId) { ?><button type="submit" class="btn btn-primary">Save</button><?php } ?>
					<button type="submit" class="btn">Cancel</button>
					<?php if ($__userContext->userId != $__model->userId) { ?><button type="submit" class="btn btn-danger">Delete</button><?php } ?>
				</div>
			</section>
		
		</form>
		
	</div>
	
</section>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
<?php
$appRoot = MvcRouter::$appRoot . "lib/fileUploader/";
$imageVersions = $__viewContext->viewData["imageVersions"];
?>

<link rel="stylesheet" href="<?php echo $appRoot; ?>css/fileuploader.css"><!-- Bootstrap Image Gallery styles -->
<link rel="stylesheet" href="<?php echo $appRoot; ?>css/jquery.fileupload-ui.css"><!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<noscript><link rel="stylesheet" href="<?php echo $appRoot; ?>css/jquery.fileupload-ui-noscript.css"></noscript><!-- CSS adjustments for browsers with JavaScript disabled -->

<div>
    <form id="fileupload" action="<?php echo MvcRouter::GetActionUrl("admin", "file-uploader-handler"); ?>" method="POST" enctype="multipart/form-data">
        
        <noscript><input type="hidden" name="redirect" value="http://blueimp.github.com/jQuery-File-Upload/"></noscript><!-- Redirect browsers with JavaScript disabled to the origin page -->
        
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="fileupload-buttonbar">
            <div>
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancel upload</span>
                </button>
            </div>
        </div>
        
        <br/>
        
        <!-- ============== TABLE OF MEDIA -->
        <table role="presentation" class="table table-striped bootstrap-wysihtml5-insert-image-selector">
        	<tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
	        	
        	</tbody>
        </table>
        <!-- ============== /TABLE OF MEDIA -->
        
    </form>
</div>

<!-- ========================================= -->
<!-- TEMPLATE > FILES AVAILABLE FOR UPLOAD -->
<!-- go to: /lib/fileUploader/js/jquery.fileupload-ui.js -->
<!-- ========================================= -->

<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td>
			<h4>{%= file.name %}</h4>
			<br/>
			{% if (file.error) { %}
				<div class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</div>
			{% } else { %}
				<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
			{% } %}
		</td>
		<td>
			{% if (!file.error && o.files.valid && !i) { %}
				<span class="start">
					{% if (!o.options.autoUpload) { %}
						<button class="btn btn-primary" title="start upload">
							<span class="icon-upload icon-white"></span>
						</button>
					{% } %}
				</span>
				<span class="cancel">
					{% if (!i) { %}
						<button class="btn btn-warning" title="cancel upload">
							<span class="icon-ban-circle icon-white"></span>
						</button>
					{% } %}
				</span>
			{% } %}
		</td>
    </tr>
{% } %}
</script>

<!-- ========================================= -->
<!-- TEMPLATE > LIST OF MEDIA IN GALLERY -->
<!-- go to: /lib/fileUploader/js/jquery.fileupload-ui.js -->
<!-- ========================================= -->

<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
        	<td><button class="btn btn-danger"><span class="icon-remove icon-white"></span></button></td>
            <td class="preview"><span class="fade"></span></td>
            <td colspan="2">
				<h4>{%= file.name %}</h4>
				<div class="error"><span class="label label-important">Error</span> {%=file.error%}</div>
			</td>
        {% } else { %}
            <td valign="middle"><a onclick="deleteImage({%=file.dynamic_content_id%});return false;" class="btn btn-danger deleteImageButton" id="{%=file.dynamic_content_id%}"><span class="icon-remove icon-white"></span></a></td>
            <td class="preview"><img src="{%=file.small_url%}"></td>
            <td colspan="2">
				<h4>{%= file.name %}</h4>
				<a id="{%=file.dynamic_content_id%}" href="{%=file.small_url%}" title="{%=file.name%}" class="btn btn-choose btn-choose-feature">Choose Image</a>
				<a id="{%=file.dynamic_content_id%}" href="{%=file.url%}" title="{%=file.name%}" class="btn btn-small btn-choose">Actual</a>
				<a id="{%=file.dynamic_content_id%}" href="{%=file.large_url%}" title="{%=file.name%}" class="btn btn-small btn-choose"><?php echo $imageVersions["large"]["max_width"] . " x " . $imageVersions["large"]["max_height"]; ?></a>
				<a id="{%=file.dynamic_content_id%}" href="{%=file.medium_url%}" title="{%=file.name%}" class="btn btn-small btn-choose"><?php echo $imageVersions["medium"]["max_width"] . " x " . $imageVersions["medium"]["max_height"]; ?></a>
				<a id="{%=file.dynamic_content_id%}" href="{%=file.small_url%}" title="{%=file.name%}" class="btn btn-small btn-choose"><?php echo $imageVersions["small"]["max_width"] . " x " . $imageVersions["small"]["max_height"]; ?></a>
            </td>
        {% } %}
    </tr>
{% } %}
</script>

<!-- ========================================= -->
<!-- DELETE IMAGE BUTTON -->
<!-- ========================================= -->

<script type="text/javascript">
function deleteImage(imageId){
	
	if (imageId){
		$.ajax({
			url: '/admin/deleteImage/'+imageId+'/'
		}).done(function(data) {
			$('#'+imageId).parent().parent().fadeOut();
		});
	}
}
</script>

<script src="<?php echo $appRoot; ?>js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo $appRoot; ?>js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo $appRoot; ?>js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo $appRoot; ?>js/canvas-to-blob.min.js"></script>
<script src="<?php echo $appRoot; ?>js/bootstrap-image-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo $appRoot; ?>js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo $appRoot; ?>js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo $appRoot; ?>js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo $appRoot; ?>js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!--<script src="<?php echo $appRoot; ?>js/main.js"></script>-->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->
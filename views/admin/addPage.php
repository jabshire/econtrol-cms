<?php
$__pageTitle = "Add Page";
ob_start();
?>
<h1>New Page</h1>

<hr>

<section class="messages">
	<?php if ($__viewContext->modelState->IsValid() == false) { ?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true, "<b>Error!</b> Something is wrong in the form."); ?>
		</div>
	<?php } ?>
</section>

<section class="row">
	
	<!--main-->
	<div class="span12">
		
		<form id="editForm" action="<?php echo MvcRouter::GetActionUrl("admin","page","add"); ?>" method="post" class="row layout-3">
			<?php 
			$__model->type = "PAGE"; 
			$__model->publishDate = date('Y-m-d H:i:s'); 
			$__model->status = 'draft'; 
			$__model->layout = '2' 
			?>
			<?php echo MvcHtml::RenderPartialView($__viewContext, "inc/page.form", $__model); ?>
		</form>
		<? include_once('inc/imageModal.php') ?>
		
		<!--post-actions-->
		<section class=" page-save">
			<div class="well well-small">
				<button type="submit" class="btn btn-primary" onclick="document.getElementById('editForm').submit(); return false;">Save</button>
				<button type="submit" class="btn" onclick="window.location='<?php echo MvcRouter::GetActionUrl('admin', 'pages'); ?>'; return false;">Cancel</button>
			</div>
		</section>
			
	</div>
	
</section>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
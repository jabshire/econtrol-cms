<?php
$__pageTitle = "Users";
ob_start();

$orderBy = ($__model->criteria->orderBy ? $__model->criteria->orderBy : '');
$orderByDirection = ($__model->criteria->orderByDirection ? $__model->criteria->orderByDirection : 'DESC');
$orderByDirectionOpposite = ($orderByDirection == "DESC" ? "ASC" : "DESC");
?>

<h1>Listing Users <small>Showing <?php echo count($__model->results) . " of " . $__model->totalResults . " results."; ?></small></h1>

<hr>

<section class="row">

    <!--sidebar-->
    <section class="span3">
        <div class="well well-small">
            <?php include_once('inc/users.filter.php') ?>
        </div>
    </section>

    <!--main-->
    <section class="span9">
        <div class="well well-small">
            <?php include_once('inc/users.paging.php') ?>
        </div>
        <!--list-->
        <?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true, "The following error(s) occurred:"); ?>
        <table class="table table-striped table-hover table-shadow">
            <thead>
                <tr>
                    <th></th>
                    <th class="hidden-phone">
                        <a href="#" orderBy="UserId" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by ID DESC">
                            ID 
                        </a>                                            
                    </th>
                    <th>
                        <a href="#" orderBy="FirstName" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Name DESC">
                            Name
                        </a>
                    </th>
                    <th>                                            
                        <a href="#" orderBy="Email" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Email DESC">
                            Email 
                        </a>
                    </th>
                    <th class="hidden-phone">                                            
                        <a href="#" orderBy="UserGroupId" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Group DESC">
                            Group 
                        </a>
                    </th>
                    <th class="hidden-phone hidden-tablet">                                            
                        <a href="#" orderBy="CreateDate" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Created DESC">
                            Created
                        </a>
                    </th>	
                    <th class="hidden-phone hidden-tablet">                                            
                        <a href="#" orderBy="sso_provider_id" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by SSO DESC">
                            SSO
                        </a>
                    </th>	
                </tr>
            </thead>
            <tbody>
                <?php foreach ($__model->results as $user) { ?>
                    <tr>
                        <td>
                            <div class="btn-group">
                                <?php
                                echo MvcHtml::ActionLink('<span class="icon-edit"></span>', 'admin', 'user', $user->userId, array('class' => 'btn btn-small'));
                                if ($__userContext->userId != $user->userId)
                                    echo MvcHtml::ActionLink('<span class="icon-remove"></span>', 'admin', 'delete-user', $user->userId, array('class' => 'btn btn-small', 'onclick' => "return confirm('Are you sure you want to delete " . $user->email . "?');"));
                                ?>
                            </div>
                        </td>
                        <td class="hidden-phone"><?php echo $user->userId ?></td>
                        <td class="hidden-phone"><?php echo $user->firstName . ' ' . $user->lastName ?></td>
                        <td><?php echo $user->email ?></td>
                        <td class="hidden-phone"><?php echo ucfirst(array_search($user->userGroupId, Config::$security["userGroups"])) ?></td>
                        <td class="hidden-phone hidden-tablet"><?php
                            $dt = explode(" ", $user->createDate);
                            echo $dt[0];
                            ?></td>
                        <td class="hidden-phone hidden-tablet"><?php
                            //if (count($user->ssoIdentities) > 0) {
                                $imgRoot = Config::$themes["default"]["root"] . "img/sso/icon/";
                                //foreach ($user->ssoIdentities as $identity)
                                    if($user->ssoProviderId != "")
                                        echo "<img src='" . $imgRoot . $user->ssoProviderId . "_32.png' style='width:1.45em;' title='" . $user->ssoEmail . "'/>";
                                    else
                                        echo "-";
                                //}
                            ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </section>

</section>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
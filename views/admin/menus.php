<?php
$__pageTitle = "Menus";
ob_start();

$orderBy = ($__model->criteria->orderBy ? $__model->criteria->orderBy : '');
$orderByDirection = ($__model->criteria->orderByDirection ? $__model->criteria->orderByDirection : 'DESC');
$orderByDirectionOpposite = ($orderByDirection == "DESC" ? "ASC" : "DESC");
?>

<h1>Listing Menus <small>Showing <?php echo count($__model->results) . " of " . $__model->totalResults . " results."; ?></small></h1>

<hr>

<section class="row">

    <!--sidebar-->
    <section class="span3">
        <div class="well well-small">
            <?php include_once('inc/menus.filter.php') ?>
        </div>
    </section>

    <!--main-->
    <section class="span9">
        <div class="well well-small">
            <?php include_once('inc/menus.paging.php') ?>
        </div>
        <!--list-->
        <table class="table table-striped table-hover table-shadow">
            <thead>
                <tr>
                    <th></th>
                    <th class="hidden-phone">
                        <a href="#" orderBy="c.dynamic_content_id" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by ID DESC">
                            ID 
                        </a>                                            
                    </th>
                    <th>
                        <a href="#" orderBy="c.title" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Title DESC">
                            Title
                        </a>
                    </th>
                    <th class="hidden-phone">                                            
                        <a href="#" orderBy="d.layout" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Layout DESC">
                            Layout 
                        </a>
                    </th>
                    <th class="hidden-phone hidden-tablet">                                            
                        <a href="#" orderBy="c.status" orderByDirection="<?php echo $orderByDirection; ?>" class="header-table" title="Sort by Status DESC">
                            Status
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($__model->results as $content) { ?>
                    <tr>
                        <td>
                            <div class="btn-group">
                                <?php
                                echo MvcHtml::ActionLink('<span class="icon-edit"></span>', 'admin', 'menu/edit', $content->id, array('class' => 'btn btn-small'));
                                ?>
                            </div>
                        </td>
                        <td class="hidden-phone"><?php echo $content->id ?></td>
                        <td><b><?php echo $content->title ?></b></td>
                        <td class="hidden-phone"><?php echo $content->layout ?></td>
                        <td class="hidden-phone hidden-tablet"><?php echo ucfirst(strtolower($content->status)) ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </section>

</section>


<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
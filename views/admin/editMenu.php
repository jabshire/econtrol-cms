<?php
$__pageTitle = "Edit Menu";
ob_start();
?>
<h1>Edit Menu</h1>

<hr>

<section class="messages">
	<?php if ($__viewContext->modelState->IsValid() == false) { ?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true, "<b>Error!</b> Something is wrong in the form."); ?>
		</div>
	<?php } ?>
	<?php if (isset($__viewContext->viewData["updated"])) { ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<b>Sucess!</b> The menu has been updated.
		</div>
	<?php } ?>
</section>

<section class="row">
	
	<!--main-->
	<div class="span12">
		
		<form id="editForm" action="<?php echo MvcRouter::GetActionUrl("admin","menu","edit"); ?>" method="post" class="row layout-3">
			<?php $__model->type = "MENU"; ?>
			<?php echo MvcHtml::RenderPartialView($__viewContext, "inc/menu.form", $__model); ?>

			<!--post-actions-->
			<section class="span12 page-save">
				<div class="well well-small">
					<button type="submit" class="btn btn-primary" onclick="document.getElementById('editForm').submit(); return false;">Save</button>
					<button type="submit" class="btn" onclick="window.location='<?php echo MvcRouter::GetActionUrl('admin', 'menus'); ?>'; return false;">Cancel</button>
					<button type="submit" class="btn btn-danger" onclick="Delete(); return false;">Delete</button>
				</div>
			</section>
		</form>
	
	</div>
	
</section>

<script type="text/javascript">
function Delete(title, id) {
	if (confirm("Are you sure you want to delete '<?php echo $__model->title; ?>'?"))
		window.location = "<?php echo MvcRouter::GetActionUrl('admin', 'menu/delete/' . $__model->id); ?>";
}
</script>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["admin"]["root"] . Config::$themes["admin"]["masterPageFile"]);
?>
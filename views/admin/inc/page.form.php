
<?php 

// PARSER RULES

function parserRules($rules){
	$ind = '	';//'&nbsp;&nbsp;&nbsp;&nbsp;';
	$br = "\n";//'<br>';
	$ret = '';
	
	echo $br;
	
	foreach ($rules as $tagKey1 => $tagValue1){
		$ret .= $tagKey1 . ': ';
		
		if (!is_array($tagValue1)){
			$ret .= parserRulesVars($tagValue1);
			$ret .= ','.$br;
			
		} else { // LEVEL 2
			
			$ret .= '{'.$br;
			foreach ($tagValue1 as $tagKey2 => $tagValue2){
				$ret .= $ind . $tagKey2 . ': ';
				
				if (!is_array($tagValue1)){
					$ret .= parserRulesVars($tagValue1);
					
				} else { // LEVEL 3
					$ret .= '{'.$br;
					foreach ($tagValue2 as $tagKey3 => $tagValue3){
						$ret .= $ind . $ind . $tagKey3 . ': ';
						$ret .= parserRulesVars($tagValue3) . ',' . $br;
					}
					$ret .= $ind . '},'.$br;
					
				}
			}
			$ret .= '},'.$br;
		}
	}
	return $ret;
}

// PARSER VARIABLES

function parserRulesVars($var=''){
	if ($var){
		return $var;
	} else {
		return '{}';
	}
}

?>

<?php echo MvcHtml::HiddenFor($__model, 'id'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'type'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createByUserName'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'publishDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, "status"); ?>
<?php echo MvcHtml::HiddenFor($__model, "image"); ?>
<?php echo MvcHtml::HiddenFor($__model, "layout"); ?>

<?php $__userContext = MvcSecurity::GetUserContext(); ?>

<input type="hidden" name="modifyByUserName" value="<?php echo $__userContext->userName; ?>">

<!--layout-selector-->
<div class="span12">
	<? include_once('page.layoutSelector.php') ?>
</div>
<!--/layout-selector-->

<!--post-content-->
<section class="span8 user-content">
	<div class="well well-small">
		
		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, 'title', array("class"=>'input-block-level', 'placeholder'=>'Title')); ?>
		</fieldset>
		<fieldset>
			<textarea name="body" placeholder="Body" class="input-block-level noresize-horizontal wysihtml5" rows="30"><?php echo $__model->body; ?></textarea>
		</fieldset>
		<fieldset>
			<textarea name="teaser" placeholder="Teaser" class="input-block-level noresize-horizontal" rows="10"><?php echo $__model->teaser; ?></textarea>
		</fieldset>
		
		<legend>SEO</legend>
		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, 'seoTitle', array("class"=>'input-block-level', 'placeholder'=>'Title')); ?>
			<textarea name="seoDescription" placeholder="Description" class="input-block-level noresize-horizontal" rows="3"><?php echo $__model->seoDescription; ?></textarea>
		</fieldset>
		
		<legend>Routes</legend>
		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, "primaryRoute", array("class"=>'input-block-level', 'placeholder'=>'Primary Route')); ?>
			<?php if ($__model->primaryRoute) echo '<span class="help-block"><a href="' . $__model->primaryRoute . '?view=1" target="_blank"><small>preview</small></a></span>'; ?>
		</fieldset>

	</div>
</section>

<!--post-info-->
<section class="span4 post-info">
	<div class="well well-small">
		
		<!--publish-date-->
		<fieldset>
			<div class="row-fluid" id="setPublishDateTime">
				<!--datepicker-->
				<div class="input-append datepicker" data-date="12-02-2012" data-date-format="mm-dd-yyyy">
					<input type="text" class="input-medium" id="publish_date" value="<?=date('Y-m-d', strtotime($__model->publishDate))?>">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<!--timepicker-->
				<div class="input-append timepicker">
					<input type="text" class="timepicker input-medium" id="publish_time" value="<?=date('h:i A', strtotime($__model->publishDate))?>">
					<span class="add-on"><i class="icon-time"></i></span>
				</div>
			</div>
		</fieldset>
		
		<!--status-->
		<fieldset>
			<?php $stats = array("DRAFT", "PRIVATE", "PUBLISHED"); ?>
			<div class="btn-group" id="selectStatus">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="0"><span class="name"><?php echo ucfirst(strtolower($__model->status)) ?></span> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<?php foreach($stats as $status) { ?>
					<li class="<?php echo ($status == $__model->status ? 'active':'' ) ?>"><a href="<?php echo $status ?>"><? echo ucfirst(strtolower($status)) ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</fieldset>
	</div>
</section>

<!--post-sidebar-->
<input type="hidden" name="sidebar" id="sidebar" value='<?php echo $__model->sidebar; ?>'/>

<?php if (isset($__viewContext->viewData["widgets"]) && count($__viewContext->viewData["widgets"])) { ?>
<section class="span4 post-sidebar">
	<div class="well well-small widgets">
		<table id="sidebarWidgets" class="table table-striped table-hover table-shadow">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>Id</th>
					<th>Title</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<!--add-widget-->
		<div class="add-widget">
			<div class="btn-group" id="selectWidgets">
				<button class="btn" onclick="return false;">Add Widget</button>
				<button class="btn dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
				<ul class="dropdown-menu">
					<?php foreach($__viewContext->viewData["widgets"] as $widget) { ?>
					<li><a href="" class="widget-add" name="<?php echo $widget->id ?>"><?php echo $widget->title ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<!--/add-widget-->
	</div>
</section>
<?php } ?>

<!--post-image-->
<section class="span4 post-image">
	<div class="well well-small">
		<div id="featuredImagePreview" style="margin-bottom:10px;">
			<?php
			if ($__model->image > 0)
				echo MvcEcontrolHtml::Image($__model->image, "small");
			?>
		</div>
		<a class="btn btn-feature-image-modal" data-toggle="modal" href="#imageModal" ><?php echo ($__model->image > 0) ? "Change" : "Add"; ?> Image</a>
		<button class="btn btn-inverse" type="button" id="removeFeaturedImage"><span class="icon-remove icon-white"></span></button>
	</div>
</section>

<script type="text/javascript">
$(function(){
	// =====================================================================
	// FEATURED IMAGE > REMOVE
	// =====================================================================
	$('#removeFeaturedImage').click(function(){
		$('input[name="image"]').val('0');
		$('#featuredImagePreview').html('');
		return false;
	});
	
	
	// STATUS SELECT
	$('#selectStatus ul.dropdown-menu li a').click(function(){
		var statusId = $(this).attr('href');
		var statusName = $(this).html();
		$('#selectStatus a.dropdown-toggle .name').text(statusName);
		$('#status').val(statusId);
		$('#selectStatus').removeClass('open');
		$('#selectStatus ul.dropdown-menu li').removeClass('active');
		$(this).parent().addClass('active');
		return false;
	});
	// WIDGETS SELECT
	$('#selectWidgets ul.dropdown-menu li a').click(function(e){
		var widgetName = $(this).html();
		var widgets = GetWidgets();
		// dropdown
		$('#selectWidgets a.dropdown-toggle .name').text(widgetName);
		$('#selectWidgets').removeClass('open');
		$('#selectWidgets ul.dropdown-menu li').removeClass('active');
		$(this).parent().addClass('active');
		// table
		widgets[widgets.length] = $(this).attr("name");
		SetWidgets(widgets);
		//e.preventDefault();
		return false;
	});
	RenderWidgets();
	
	// FORM FIELDS
	$('.datepicker').datepicker().on('changeDate', function(){
		setPublishDate();
	});
	//
    $('.timepicker').timepicker({
	    'defaultTime':'value'
    }).on('hide.timepicker', function(e){
	    setPublishDate();
    });
    //
    <?php
	$tags = Config::$dynamicContent['htmlEditor']['tags'];
	$classes= Config::$dynamicContent['htmlEditor']['classes'];
    ?>
	$('.wysihtml5').wysihtml5('deepExtend', {
		html: true,
		parserRules: {
			classes: { <?php echo parserRules($classes); ?> },
			tags: { <?php echo parserRules($tags); ?> }
		}
	});
		// "stylesheets": ["/path/to/editor.css"]
});


function ImageModalCompleted(choice) {
	$(".btn-wysihtml5-image-modal").click(function() {
		$("#imageModal").removeClass("feature-image-modal");
		$("#imageModal .bootstrap-wysihtml5-insert-image-url").val("http://");
	});

	$(".btn-feature-image-modal").click(function() {
		$("#imageModal").addClass("feature-image-modal");
	});

	$("#imageModal .btn-choose-feature").click(function(e) {
		$("#image").val($(this).attr("id"));
		$("#featuredImagePreview").html("<img src='" + $(this).attr("href") + "'>");
		$("#imageModal").modal("hide");
		e.preventDefault();
	});
}

function setPublishDate(){
	var publishDate = $('#publish_date').val();
	var publishTime = $('#publish_time').val();
	if (publishDate.length > 9 && publishTime.length > 7){
		$('#publishDate').val(publishDate + ' ' + publishTime);
	}
}

function GetWidgets() {
	var sidebar = $('#sidebar');
	var widgets = Array();
	if (sidebar.val().length > 0) widgets = JSON.parse(sidebar.val());
	return widgets;
}

function SetWidgets(widgets) {
	$("#sidebar").val(JSON.stringify(widgets));
	RenderWidgets();
}

function MoveWidget(id, direction) {
	var widgets = GetWidgets();
	var i = 0;

	for(i = 0; i < widgets.length; i++) {
		if (widgets[i] == id) break;
	}

	if (direction == -1) {
		var temp = widgets[i];
		widgets[i] = widgets[i-1];
		widgets[i-1] = temp;
	}
	else if (direction == 1) {
		var temp = widgets[i];
		widgets[i] = widgets[i+1];
		widgets[i+1] = temp;
	}

	SetWidgets(widgets);
}

function RenderWidgets() {
	var widgets = GetWidgets();
	var i = 0;

	// dropdown
	$('.add-widget').show();
	$("a.widget-add").show();
	for(i = 0; i < widgets.length; i++) {
		$("a.widget-add[name='" + widgets[i] + "']").hide();
	}
	if ($('.add-widget .dropdown-menu li').size() == widgets.length)
		$('.add-widget').hide();

	// table
	var rows = "";

	for(i = 0; i < widgets.length; i++) {
		rows += "<tr>";
			rows += "<td><div class=\"btn-group\">";
				rows += "<a href='' name='" + widgets[i] + "' class=\"btn btn-small widget-delete\"><span class=\"icon-remove\"></span></a>";
			rows += "</div></td>";
			rows += "<td><div class=\"btn-group\">";
			if (i > 0) 
				rows += "<a href='' onclick=\"MoveWidget('" + widgets[i] + "', -1); return false;\" class=\"btn btn-small\"><span class=\"icon-arrow-up\"></span></a>";
			else
				rows += "<a href='' onclick=\"return false;\" class=\"btn btn-small btn-arrow-up disabled\"><span class=\"icon-arrow-up\"></span></a>";
			if (i < widgets.length-1) 
				rows += "<a href='' onclick=\"MoveWidget('" + widgets[i] + "', 1); return false;\" class=\"btn btn-small\"><span class=\"icon-arrow-down\"></span></a>";
			else
				rows += "<a href='' onclick=\"return false;\" class=\"btn btn-small disabled\" ><span class=\"icon-arrow-down\"></span></a>";
			rows += "</div></td>";
			rows += "<td>" + widgets[i] + "</td>";
			rows += "<td>" + $("a.widget-add[name='" + widgets[i] + "']").html() + "</td>";
		rows += "</tr>";
	}

	if (rows == "")
		rows = "<tr><td colspan='4'><i>No widgets have been added.</i></tr>";

	$("#sidebarWidgets tbody").html(rows);

	$("a.widget-delete").click(function(e) {
		var widgets = GetWidgets();
		var newWidgets = Array();
		var i = 0;

		for(i = 0; i < widgets.length; i++) {
			if (widgets[i] != $(this).attr("name"))
				newWidgets[newWidgets.length] = widgets[i];
		}

		SetWidgets(newWidgets);
		e.preventDefault();
	});
}

</script>

<?php echo MvcHtml::HiddenFor($__model, 'id'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'type'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createByUserName'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'publishDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'layout'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'status'); ?>
<?php
	$__userContext = MvcSecurity::GetUserContext();

	$menuItems = array();
	if ($__model->body)
		$menuItems = json_decode($__model->body);
?>

<input type="hidden" name="modifyByUserName" value="<?php echo $__userContext->userName; ?>">

<!--layout-selector-->
<div class="span12">
	<? include_once('menu.layoutSelector.php') ?>
</div>
<!--/layout-selector-->

<!--post-content-->
<section class="span8 user-content">
	<div class="well well-small">
		<input type="hidden" name="status" value="PUBLISHED"/>

		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, 'title', array("class"=>'input-block-level', 'placeholder'=>'Menu Name')); ?>
			<input type="hidden" name="body" id="body" value='<?php echo stripslashes($__model->body); ?>'/>
		</fieldset>
	</div>
</section>

<section class="span8 user-content">
	<div class="well well-small">
		<fieldset>
			<table id="menuItems" class="table table-striped table-hover table-shadow">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th>Text</th>
						<th>Title</th>
						<th>URL / Page Id</th>
					</tr>
				</thead>
				<tbody>
					<tr><td colspan='5'><i>There are no menu items.</i></tr>
				</tbody>
			</table>
			<?php if ($__model->id) echo MvcHtml::ActionLink("preview", "admin", "preview-menu", $__model->id, array("target"=>"_blank")); ?>
		</fieldset>
	</div>
</section>

<section class="span8 user-content">
	<div class="well well-small">
		<fieldset>
			<input type="hidden" id='__guid'/>
			<input type="text" id='__text' class='input-block-level' placeholder='Menu Item Text'/>
			<input type="text" id='__title' class='input-block-level' placeholder='Menu Item Title'/>
			<input type="text" id='__url' class='input-block-level' placeholder='Menu Item URL or Page Id'/>

			<input type="submit" id="btnAddItem" class="btn" value="Add Menu Item" onclick="AddMenuItem(); return false;"/>
			<input type="submit" id="btnUpdateItem" class="btn" style="display:none;" value="Update Menu Item" onclick="UpdateMenuItem(); return false;"/>
			<input type="submit" id="btnCancelItem" class="btn" style="display:none;" value="Cancel" onclick="ClearMenuItemForm(); return false;"/>
		</fieldset>
	</div>
</section>

<script type="text/javascript">

$(document).ready(function() {
	RenderMenuItems();
});

function ClearMenuItemForm() {
	$("#__guid").val("");
	$("#__text").val("");
	$("#__title").val("");
	$("#__url").val("");

	$("#btnAddItem").show();
	$("#btnUpdateItem").hide();
	$("#btnCancelItem").hide();
}

function GetMenuItems() {
	var body = $("#body");
	var items = Array();
	if (body.val().length > 0) items = JSON.parse(body.val());

	return items;
}

function SetMenuItems(items) {
	$("#body").val(JSON.stringify(items));
	RenderMenuItems();
}

function AddMenuItem() {
	var items = GetMenuItems();
	
	var item = {
		guid : guid(),
		text : $("#__text").val(),
		title : $("#__title").val(),
		url : $("#__url").val()
	}

	items[items.length] = item;

	SetMenuItems(items);
	ClearMenuItemForm();
}

function EditMenuItem(guid) {
	var items = GetMenuItems();
	var i = 0;

	for(i = 0; i < items.length; i++) {
		if (items[i].guid == guid) break;
	}

	$("#__guid").val(items[i].guid);
	$("#__text").val(items[i].text);
	$("#__title").val(items[i].title);
	$("#__url").val(items[i].url);

	$("#btnAddItem").hide();
	$("#btnUpdateItem").show();
	$("#btnCancelItem").show();
}

function UpdateMenuItem() {
	var items = GetMenuItems();
	var guid = $("#__guid").val();
	var i = 0;

	for(i = 0; i < items.length; i++) {
		if (items[i].guid == guid) break;
	}

	items[i].text = $("#__text").val();
	items[i].title = $("#__title").val();
	items[i].url = $("#__url").val();

	SetMenuItems(items);
	ClearMenuItemForm();
}

function DeleteMenuItem(guid) {
	var items = GetMenuItems();
	var newItems = Array();
	var i = 0;

	for(i = 0; i < items.length; i++) {
		if (items[i].guid != guid)
			newItems[newItems.length] = items[i];
	}

	SetMenuItems(newItems);
}

function MoveMenuItem(guid, direction) {
	var items = GetMenuItems();
	var i = 0;

	for(i = 0; i < items.length; i++) {
		if (items[i].guid == guid) break;
	}

	if (direction == -1) {
		var temp = items[i];
		items[i] = items[i-1];
		items[i-1] = temp;
	}
	else if (direction == 1) {
		var temp = items[i];
		items[i] = items[i+1];
		items[i+1] = temp;
	}

	SetMenuItems(items);
}

function RenderMenuItems() {
	var items = GetMenuItems();
	var rows = "";
	var i = 0;

	for(i = 0; i < items.length; i++) {
		rows += "<tr>";
			rows += "<td><div class=\"btn-group\">";
				rows += "<a href='' onclick=\"EditMenuItem('" + items[i].guid + "'); return false;\" class=\"btn btn-small\"><span class=\"icon-edit\"></span></a>";
				rows += "<a href='' onclick=\"DeleteMenuItem('" + items[i].guid + "'); return false;\" class=\"btn btn-small\"><span class=\"icon-remove\"></span></a>";
			rows += "</div></td>";
			rows += "<td><div class=\"btn-group\">";
			if (i > 0) 
				rows += "<a href='' onclick=\"MoveMenuItem('" + items[i].guid + "', -1); return false;\" class=\"btn btn-small\"><span class=\"icon-arrow-up\"></span></a>";
			else
				rows += "<a href='' onclick=\"return false;\" class=\"btn btn-small btn-arrow-up disabled\"><span class=\"icon-arrow-up\"></span></a>";
			if (i < items.length-1) 
				rows += "<a href='' onclick=\"MoveMenuItem('" + items[i].guid + "', 1); return false;\" class=\"btn btn-small\"><span class=\"icon-arrow-down\"></span></a>";
			else
				rows += "<a href='' onclick=\"return false;\" class=\"btn btn-small disabled\" ><span class=\"icon-arrow-down\"></span></a>";
			rows += "</div></td>";
			rows += "<td>" + items[i].text + "</td>";
			rows += "<td>" + items[i].title + "</td>";
			rows += "<td>" + items[i].url + "</td>";
		rows += "</tr>";
	}

	if (rows == "")
		rows = "<tr><td colspan='5'><i>There are no menu items.</i></tr>";

	$("#menuItems tbody").html(rows);
}

function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
function guid() {
   return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

</script>
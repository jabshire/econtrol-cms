
<form action="<?php echo MvcRouter::GetActionUrl('admin', 'pages'); ?>" method="post">

    <?php echo MvcHtml::HiddenFor($__model->criteria, "type"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "status"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "statusText"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "resultsPerPage"); ?>    
    <?php echo MvcHtml::HiddenFor($__model->criteria, "orderBy"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "orderByDirection"); ?>

    <legend>Filter</legend>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'title', array('class' => 'input-block-level', 'placeholder' => 'Title')); ?>
    </fieldset>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'createByUserName', array('class' => 'input-block-level', 'placeholder' => 'Created By')); ?>
    </fieldset>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'modifyByUserName', array('class' => 'input-block-level', 'placeholder' => 'Modified By')); ?>
    </fieldset>

    <fieldset>
        <ul class="unstyled">
            <li class="dropdown" id="filterStatus">
                <a href="#" class="dropdown-toggle btn" data-toggle="dropdown"><span class="name">Status</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="All">All</a></li>
                    <?php
                    $stats = array("Draft", "Private", "Published");
                    foreach ($stats as $status) {
                        echo '<li class="' . ($status == $__model->criteria->status ? 'active' : '') . '"><a href="' . $status . '">' . $status . '</a></li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <button type="submit" class="btn btn-inverse">Filter List</button>
        <button type="clear" class="btn btnClear">Clear List</button>
    </fieldset>


</form>
<script type="text/javascript">
    $(function() {
        
        <?php if($__model->criteria->statusText != ""){ ?>
            var $filterStatus = $("#filterStatus .dropdown-menu a[href*='<?php echo $__model->criteria->statusText; ?>']");
            $('#filterStatus a.dropdown-toggle .name').text($filterStatus.text());
        <?php }?>

        // User Group
        $('#filterStatus ul.dropdown-menu li a').click(function() {
            var text = $(this).text();
            var value = $(this).attr('href');
            
            if(value == "All")
                value = "";
            $('#filterStatus a.dropdown-toggle .name').text(text);
            $('#status').val(value);
            $('#statusText').val(text);
            $('#filterStatus').removeClass('open');
            return false;
        });

        $(".btnClear").click(function() {
            location.reload(true);
            return false;
        });
    });
</script>

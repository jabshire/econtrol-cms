
<form action="<?php echo MvcRouter::GetActionUrl('admin', 'users'); ?>" method="post" class="row-fluid nomargins frm-paging" style="height:auto">
	
	<!--left-->
	<div class="span8">
		<div class="btn-group" id="filterCurrentPage">
		    <?php include_once('general.paging.php') ?>	
                </div>
	</div>
	
	<!--right-->
	<div class="span4">
		<ul class="unstyled nomargin" style="float:right;">
			<li class="dropdown" id="filterResultsPerPage">
				<a href="#" class="dropdown-toggle btn" data-toggle="dropdown">Results/Page <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<?php
					$resultOptions = array(5, 10,20,50,100);
					foreach($resultOptions as $x) {
						echo '<li class="' . ($x == $__model->criteria->resultsPerPage ? ' active' : '') . '"><a href="' . $x . '">' . $x . '</a></li>';
					}
					?>
				</ul>
			</li>
		</ul>
	</div>

	<?php
            echo MvcHtml::HiddenFor($__model->criteria, "firstName");
            echo MvcHtml::HiddenFor($__model->criteria, "lastName");
            echo MvcHtml::HiddenFor($__model->criteria, "userName");
            echo MvcHtml::HiddenFor($__model->criteria, "email");
            echo MvcHtml::HiddenFor($__model->criteria, "userGroupId");
            echo MvcHtml::HiddenFor($__model->criteria, "userGroupText");
            echo MvcHtml::HiddenFor($__model->criteria, "isVerified");
            echo MvcHtml::HiddenFor($__model->criteria, "isVerifiedText");
            echo MvcHtml::HiddenFor($__model->criteria, "currentPage");
            echo MvcHtml::HiddenFor($__model->criteria, "resultsPerPage");                
            echo MvcHtml::HiddenFor($__model->criteria, "orderBy");
            echo MvcHtml::HiddenFor($__model->criteria, "orderByDirection");
	?>
	
</form>
<?php include_once('tables.sorting.php') ?>
<script type="text/javascript">
$(function(){
	// CURRENT PAGE
	$('#filterCurrentPage a').click(function(){
		var currentPage = $(this).attr('href');
		$('#filterCurrentPage a').removeClass('active');
		$(this).addClass('active');
		$('#currentPage')
			// first set the new page value
			.val(currentPage)
			// then submit the paging form
			.closest('form').submit();
		return false;
	});
	// RESULTS PER PAGE
	$('#filterResultsPerPage ul.dropdown-menu a').click(function(){
		var results = $(this).attr('href');
		$('#filterResultsPerPage a.dropdown-toggle .name').text(results);
		$('#resultsPerPage').val(results);
		$('#filterResultsPerPage').removeClass('open');
		$(this).parent().parent().children('li').removeClass('active');
		$(this).parent().addClass('active');
		$('#resultsPerPage').closest('form').submit();
		return false;
	});
});
</script>


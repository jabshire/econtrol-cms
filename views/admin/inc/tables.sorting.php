<script type="text/javascript">
    $(function() {
        <?php if ($orderBy != "" && $orderByDirection != "") { ?>
            var $currentSortElement = $("a.header-table[orderBy*='<?php echo $orderBy; ?>']");
            if ($currentSortElement.length > 0)
            {
                var sortIcon = "<i class='<?php echo ($__model->criteria->orderByDirection == "ASC" ? "icon-chevron-up" : "icon-chevron-down")?> icon-white'></i>";
                var elHtml = $currentSortElement.html().trim();
                $currentSortElement.html(elHtml + " " + sortIcon);
                var attrTitleText = "Sort by " + elHtml + " <?php echo $orderByDirectionOpposite; ?>";
                $currentSortElement.attr("title",attrTitleText); 
                $currentSortElement.addClass("active");
            }
        <?php } ?>

        // SORTING FUNCTIONALITY
        $('a.header-table').click(function() {
            var orderBy = $(this).attr('orderBy');
            var orderByDirection = $(this).attr('orderByDirection');
            $('.frm-paging #orderBy').val(orderBy);

            if (orderByDirection == "ASC") {
                orderByDirection = "DESC";
                //$(this).find("i").attr("class","icon-chevron-up icon-white");
            }
            else if (orderByDirection = "DESC") {
                orderByDirection = "ASC";
                //$(this).find("i").attr("class","icon-chevron-down icon-white");
            }

            $('.frm-paging #orderByDirection').val(orderByDirection);
            $(".frm-paging").submit();
            return false;
        });
    });
</script>

<form id="frm-pages-paging" action="<?php echo MvcRouter::GetActionUrl('admin', 'pages'); ?>" method="post" class="row-fluid nomargins frm-paging" style="height:auto">

    <!--left-->
    <div class="span8">
        <div class="btn-group" id="filterCurrentPage">
            <?php include_once('general.paging.php') ?>
        </div>
    </div>

    <!--right-->
    <div class="span4">
        <ul class="unstyled nomargin" style="float:right;">
            <li class="dropdown" id="filterResultsPerPage">
                <a href="#" class="dropdown-toggle btn" data-toggle="dropdown">Results/Page <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <?php
                    $resultOptions = array(5, 10, 20, 50, 100);
                    foreach ($resultOptions as $x) {
                        echo '<li class="' . ($x == $__model->criteria->resultsPerPage ? ' active' : '') . '"><a href="' . $x . '">' . $x . '</a></li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
    </div>

    <?php
    echo MvcHtml::HiddenFor($__model->criteria, "title");
    echo MvcHtml::HiddenFor($__model->criteria, "type");
    echo MvcHtml::HiddenFor($__model->criteria, "createByUserName");
    echo MvcHtml::HiddenFor($__model->criteria, "modifyByUserName");
    echo MvcHtml::HiddenFor($__model->criteria, "status");
    echo MvcHtml::HiddenFor($__model->criteria, "statusText");
    echo MvcHtml::HiddenFor($__model->criteria, "currentPage");
    echo MvcHtml::HiddenFor($__model->criteria, "resultsPerPage");
    echo MvcHtml::HiddenFor($__model->criteria, "orderBy");
    echo MvcHtml::HiddenFor($__model->criteria, "orderByDirection");
    ?>

</form>
<?php include_once('tables.sorting.php') ?>
<script type="text/javascript">
    $(function() {
        // RESULTS PER PAGE
        $('#filterCurrentPage a, .btnPagination').click(function() {
            var currentPage = $(this).attr('href');
            $('#filterCurrentPage a').removeClass('active');
            $(this).addClass('active');
            $('#frm-pages-paging #currentPage').val(currentPage);
            $("#frm-pages-paging").submit();
            return false;
        });
        // RESULTS PER PAGE
        $('#filterResultsPerPage ul.dropdown-menu a').click(function() {
            var results = $(this).attr('href');
            $('#filterResultsPerPage a.dropdown-toggle .name').text(results);
            $('#frm-pages-paging #resultsPerPage').val(results);
            $('#frm-pages-paging #currentPage').val(1);
            $('#filterResultsPerPage').removeClass('open');
            $(this).parent().parent().children('li').removeClass('active');
            $(this).parent().addClass('active');
            $("#frm-pages-paging").submit();
            return false;
        });                
    });
</script>



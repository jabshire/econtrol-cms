
<?php echo MvcHtml::HiddenFor($__model, 'id'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'type'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createByUserName'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'createDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'publishDate'); ?>
<?php echo MvcHtml::HiddenFor($__model, 'layout'); ?>
<?php echo MvcHtml::HiddenFor($__model, "image"); ?>
<?php echo MvcHtml::HiddenFor($__model, 'status'); ?>
<?php $__userContext = MvcSecurity::GetUserContext(); ?>

<input type="hidden" name="modifyByUserName" value="<?php echo $__userContext->userName; ?>">

<!--layout-selector-->
<div class="span12">
	<? include_once('widget.layoutSelector.php') ?>
</div>
<!--/layout-selector-->

<!--post-content-->
<section class="span8 user-content">
	<div class="well well-small">
		<input type="hidden" name="status" value="PUBLISHED"/>

		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, 'title', array("class"=>'input-block-level', 'placeholder'=>'Title')); ?>
		</fieldset>
		<fieldset>
			<textarea name="body" placeholder="Body" class="input-block-level noresize-horizontal wysihtml5" rows="15"><?php echo stripslashes($__model->body); ?></textarea>
		</fieldset>

	</div>
</section>

<!--post-image-->
<section class="span4 post-image">
	<div class="well well-small">
		<div id="featuredImagePreview">
			<?php
			if ($__model->image > 0)
				echo MvcEcontrolHtml::Image($__model->image, "small");
			?>
		</div>
		<br/>
		<a class="btn btn-feature-image-modal" data-toggle="modal" href="#imageModal" ><?php echo ($__model->image > 0) ? "Change" : "Add"; ?> Image</a>
	</div>
</section>

<section class="span4 user-content">
	<div class="well well-small">
		<fieldset>
			<?php echo MvcHtml::TextBoxFor($__model, 'class', array("class"=>'input-block-level', 'placeholder'=>'Class')); ?>
		</fieldset>
	</div>
</section>

<script type="text/javascript">
$(function(){
	$('.wysihtml5').wysihtml5({
		'html':true,
		'color':true
		// "stylesheets": ["/path/to/editor.css"]
	});
});

function ImageModalCompleted(choice) {
	$(".btn-wysihtml5-image-modal").click(function() {
		$("#imageModal").removeClass("feature-image-modal");
		$("#imageModal .bootstrap-wysihtml5-insert-image-url").val("http://");
	});

	$(".btn-feature-image-modal").click(function() {
		$("#imageModal").addClass("feature-image-modal");
	});

	$("#imageModal .btn-choose-feature").click(function(e) {
		$("#image").val($(this).attr("id"));
		$("#featuredImagePreview").html("<img src='" + $(this).attr("href") + "'>");
		$("#imageModal").modal("hide");
		e.preventDefault();
	});
}
</script>

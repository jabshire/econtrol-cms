
<form action="<?php echo MvcRouter::GetActionUrl('admin', 'menus'); ?>" method="post">
	
	<?php echo MvcHtml::HiddenFor($__model->criteria, "type"); ?>
	<?php echo MvcHtml::HiddenFor($__model->criteria, "status"); ?>
        <?php echo MvcHtml::HiddenFor($__model->criteria, "orderBy"); ?>
        <?php echo MvcHtml::HiddenFor($__model->criteria, "orderByDirection"); ?>
	
	<legend>Filter</legend>
	
	<fieldset>
		<?php echo MvcHtml::TextBoxFor($__model->criteria, 'title', array('class'=>'input-block-level', 'placeholder'=>'Title')); ?>
	</fieldset>
	
	<fieldset>
		<?php echo MvcHtml::TextBoxFor($__model->criteria, 'createByUserName', array('class'=>'input-block-level', 'placeholder'=>'Created By')); ?>
	</fieldset>
	
	<fieldset>
		<?php echo MvcHtml::TextBoxFor($__model->criteria, 'modifyByUserName', array('class'=>'input-block-level', 'placeholder'=>'Modified By')); ?>
	</fieldset>
	
	<fieldset>
		<button type="submit" class="btn btn-inverse">Filter List</button>
		<button type="clear" class="btn btnClear">Clear List</button>
	</fieldset>
	
	<!--filter-hidden-inputs-->
	<input type="hidden" name="userGroupId" id="userGroupId" value="-1">
	<input type="hidden" name="isVerified" id="isVerified" value="-1">
	
</form>
<script type="text/javascript">
    $(function() {
        $(".btnClear").click(function() {
            location.reload(true);
            return false;
        });                
    });
</script>



<form action="<?php echo MvcRouter::GetActionUrl('admin', 'users'); ?>" method="post" class="frm-filters">

    <?php echo MvcHtml::HiddenFor($__model->criteria, "resultsPerPage"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "orderBy"); ?>
    <?php echo MvcHtml::HiddenFor($__model->criteria, "orderByDirection"); ?>

    <legend>Filter</legend>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'firstName', array('class' => 'input-block-level', 'placeholder' => 'First Name')); ?>
    </fieldset>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'lastName', array('class' => 'input-block-level', 'placeholder' => 'Last Name')); ?>
    </fieldset>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'userName', array('class' => 'input-block-level', 'placeholder' => 'Username')); ?>
    </fieldset>

    <fieldset>
        <?php echo MvcHtml::TextBoxFor($__model->criteria, 'email', array('class' => 'input-block-level', 'placeholder' => 'Email')); ?>
    </fieldset>

    <fieldset>
        <ul class="unstyled">
            <li class="dropdown" id="filterUserGroup">
                <a href="#" class="dropdown-toggle btn" data-toggle="dropdown"><span class="name">User Group</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="-1">All</a></li>
                    <?php
                    foreach (Config::$security["userGroups"] as $name => $id) {
                        if ($id > $__userContext->userGroupId)
                            continue;
                        echo '<li><a href="' . $id . '" text="' . ucfirst($name) . '">' . ucfirst($name) . '</a></li>';
                    }
                    ?>
                </ul>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <ul class="unstyled">
            <li class="dropdown" id="filterVerified">
                <a href="#" class="dropdown-toggle btn" data-toggle="dropdown"><span class="name">Verified</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="Both">Both</a></li>
                    <li><a href="1">Yes</a></li>
                    <li><a href="0">No</a></li>
                </ul>
            </li>
        </ul>
    </fieldset>

    <fieldset>
        <button type="submit" class="btn btn-inverse">Filter List</button>
        <span class="btn btnClear">Clear List</span>
    </fieldset>

    <!--filter-hidden-inputs-->
    <input type="hidden" name="userGroupId" id="userGroupId" value="-1">
    <input type="hidden" name="userGroupText" id="userGroupText" value="">
    <input type="hidden" name="isVerified" id="isVerified" value="">
    <input type="hidden" name="isVerifiedText" id="isVerifiedText" value="">

</form>
<script type="text/javascript">
    $(function() {
        <?php if($__model->criteria->userGroupText != ""){ ?>
                var $filterUserGroup = $("#filterUserGroup .dropdown-menu a:contains('<?php echo $__model->criteria->userGroupText; ?>')");
                $("#filterUserGroup a.dropdown-toggle .name").text($filterUserGroup.text());
                $filterUserGroup.addClass("active");
        <?php } ?>
            
        <?php if($__model->criteria->isVerifiedText != ""){ ?>
                var $filterVerified = $("#filterVerified .dropdown-menu a:contains('<?php echo $__model->criteria->isVerifiedText; ?>')");
                $('#filterVerified a.dropdown-toggle .name').text($filterVerified.text());
                $filterVerified.addClass("active");
        <?php } ?>
        
        // User Group
        $('#filterUserGroup ul.dropdown-menu li a').click(function() {
            var userGroupName = $(this).text();
            var userGroupId = $(this).attr('href');
            $('#filterUserGroup a.dropdown-toggle .name').text(userGroupName);
            $('#userGroupId').val(userGroupId);
            $('#userGroupText').val(userGroupName);
            $('#filterUserGroup').removeClass('open');
            return false;
        });
        // User Group
        $('#filterVerified ul.dropdown-menu li a').click(function() {
            var text = $(this).text();
            var value = $(this).attr('href');
            if(value == "Both")
                value = "";
            $('#filterVerified a.dropdown-toggle .name').text(text);
            $('#isVerified').val(value);
            $('#isVerifiedText').val(text);
            $('#filterVerified').removeClass('open');
            return false;
        });

        $(".btnClear").click(function() {
            location.reload(true);
            return false;
        });        
        
        
    });
</script>

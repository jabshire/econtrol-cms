
<div class="bootstrap-wysihtml5-insert-image-modal modal hide fade" id="imageModal">
	<!--modal-header-->
	<div class="modal-header">
		<a class="close" data-dismiss="modal">&times;</a>
		<h3>Insert image</h3>
	</div>
	<!--modal-body-->
	<div class="modal-body">
		<?php echo MvcHtml::RenderAction("admin", "file-uploader"); ?>
	</div>
	<!--modal-footer-->
	<div class='modal-footer'>
		<div><input type='text' value='http://' class='bootstrap-wysihtml5-insert-image-url input-block-level'></div>
		<a href='#' class='btn' data-dismiss='modal'>Cancel</a>
		<a href='#' class='btn btn-primary' data-dismiss='modal'>Insert</a>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#fileupload').fileupload({
			url: "<?php echo MvcRouter::GetActionUrl("admin", "file-uploader-handler"); ?>",
			prependFiles: true,
			previewMaxWidth: 120,
			previewMaxHeight: 90,
			completed: function (e, data) {
				$("#imageModal .btn-choose").click(function(e) {
					$("#imageModal .bootstrap-wysihtml5-insert-image-url").val($(this).attr("href"));
					e.preventDefault();
				});

				if(typeof ImageModalCompleted=='function')
					ImageModalCompleted();
			}
		});

		// Load existing files:
		$.ajax({
			url: $('#fileupload').fileupload('option', 'url'),
			dataType: 'json',
			context: $('#fileupload')[0]
		}).done(function (result) {
			if (result && result.length) {
				$(this).fileupload('option', 'done')
					.call(this, null, {result: result});

				$("#imageModal .btn-choose").click(function(e) {
					$("#imageModal .bootstrap-wysihtml5-insert-image-url").val($(this).attr("href"));
					e.preventDefault();
				});

				if(typeof ImageModalCompleted=='function')
					ImageModalCompleted();
			}
		});
	});
</script>
<?php
//
?>
<?php $imgRoot = Config::$themes["admin"]["root"] ?>
<section class="well well-small">
	<ul class="layout-selection">
		<? foreach (Config::$dynamicContent['layouts']['widget'] as $layout => $layoutfile){ ?>
		<li class="layout"><button class="btn btn-large <?php echo ($__model->layout == $layout)? 'active':'' ?>" type="button" title="<?=$layout?>"><img src="<?=$imgRoot?>img/icons/layout-widget-<?=$layout?>.svg"></button></li>
		<? } ?>
	</ul>
</section>
<script type="text/javascript">
$(function(){
	// LAYOUT SELECTION
	$('.layout-selection li button').click(function(){
		var layout = $(this).attr('title');
		changeLayoutInput(layout);
		changeLayoutButton(layout);
		changeLayoutForm(layout);
		return false;
	});
});

// CHANGE LAYOUT INPUT
function changeLayoutInput(layout){
	$('#layout').val(layout);
}

// CHANGE LAYOUT BUTTON
function changeLayoutButton(layout){
	$('.layout-selection li button').removeClass('active');
	$('.layout-selection li button[title="' + layout + '"]').addClass('active');
}

// CHANGE LAYOUT FORM
function changeLayoutForm(layout){
	$('#editForm').removeClass();
	$('#editForm').addClass('row ' + layout);
}
</script>

<?php
$__pageTitle = "Edit SSO";
$imgRoot = Config::$themes["default"]["root"] . "img/sso/";

ob_start();
?>

<div class="content">
	<h1>Edit My Single Sign-On Identities</h1>

	<?php echo MvcHtml::ValidationSummary($__viewContext,false,""); ?>

	<div id="assoc">
		<h2>Associated Identities</h2>
		<?php 
			if (count($__model) > 0) {
				echo "The following Single Sign-On (SSO) identities are associated with your account:";
				echo "<ol>";
				foreach($__model as $identity) {
					echo "<li>";
						echo "<div><img src='" . $imgRoot . "icon/" . $identity->providerId . "_32.png'/></div>";
						echo "<div>";
							echo "<table>";
								echo "<tr><td><b>Provider:</b></td><td>$identity->providerName</td></tr>";
								echo "<tr><td><b>Identifier:</b></td><td>$identity->identityId</td></tr>";
								if ($identity->userName != $identity->email) echo "<tr><td><b>Username:</b></td><td>$identity->userName</td></tr>";
								if ($identity->email) echo "<tr><td><b>Email:</b></td><td>$identity->email</td></tr>";
								echo "<tr><td><b>First Name:</b></td><td>$identity->firstName</td></tr>";
								echo "<tr><td><b>Last Name:</b></td><td>$identity->lastName</td></tr>";
							echo "</table>";
							echo MvcHtml::ActionLink("Delete", "security", "sso-delete", $identity, array("onclick"=>"return confirm('Are you sure you want to delete this identity?');"));
						echo "</div>";
					echo "</li>";
				}
				echo "</ol>";							
			}
			else {
				echo "<i>You do not have any Single Sing-On (SSO) identities associated with your account.</i>";
			}
		?>
	</div>

	<div id="addSso">
		<h2>Link a New Identity</h2>
		<?php
			foreach($__viewContext->viewData["ssoProviders"] as $provider) {
				echo MvcHtml::ActionLink("<img src='" . $imgRoot . "/" . $provider->id . ".png' title='" . $provider->description . "'/>", "security", "sso-add", $provider->id);
			}
		?>
	</div>

	<input type="button" value="Back to My Account" onclick="window.location='<?php echo MvcRouter::GetActionUrl("Member", "Account"); ?>';"/>
</div>

<style type="text/css">
	#assoc ol {
		list-style-type: none;
	}

	#assoc li {
		clear:left;
		padding: 1.0em 0;
	}

	#assoc li img,
	#assoc li div {
		float:left;
	}

	#addSso {
		clear:left;
		width:50%;
		padding:25px 0;
	}

	#addSso img {
		padding:5px 10px;
		border:0;
		width:100px;
	}
</style>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
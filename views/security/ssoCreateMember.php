<?php
$__pageTitle = "Create Member Account";
$imgRoot = Config::$themes["default"]["root"] . "img/sso/";

ob_start();
?>

<div class="content">
	<h1>Are You a New User?</h2>
	<?php //var_dump($__model); ?>

	<div>
		Welcome <img src='<?php echo $imgRoot . "icon/" . $__model->providerId . "_32.png"; ?>' align='center' width='24px;'/> <strong><?php echo $__model->userName; ?></strong>!
		We haven't seen this <strong><?php echo ucfirst($__model->providerId); ?></strong> identity before, so please choose from the following options:<br/>
		<br/>
		<?php echo MvcHtml::ValidationSummary($__viewContext, true, ""); ?>
		<ul class="choose">
			<?php if (!$__viewContext->modelState->GetError("email") && !$__viewContext->modelState->GetError("identityId")) { ?>
				<li>
					<input type="radio" name="radChoose" id='radContinue' onclick="ShowContinue();"/>
					<span class='choose-option'>Continue</span>, and a <u>new</u> <strong><?php echo Config::$site["name"]; ?></strong> account will be instantly created for you.
					<form id="frmContinue" method="post" action="security/sso-create-member">
						<?php echo MvcHtml::HiddenFor($__model, "providerId"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "identityId"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "userName"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "email"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "firstName"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "lastName"); ?>
						<?php echo MvcHtml::HiddenFor($__model, "reply"); ?>
						<input type="hidden" name="userGroupId" id="userGroupId" value="2"/>
						<table>
							<tr>
								<td><b>User Name:&nbsp;</b></td>
								<td><?php echo MvcHtml::TextBoxFor($__model, "newUserName") . "<br/>"; ?></td>
							</tr>
							<?php if ($__model->email) { ?>
								<tr>
									<td><b>Email:</b></td>
									<td><?php echo $__model->email; ?><?php echo MvcHtml::HiddenFor($__model, "newEmail"); ?></td>
								</tr>
							<?php } else { ?>
								<tr>
									<td><b>Email:</b></td>
									<td><?php echo MvcHtml::TextBoxFor($__model, "newEmail") . "<br/>"; ?></td>
								</tr>
							<?php } ?>
							<tr>
								<td></td>
								<td><input type="submit" value="Continue"/></td>
							</tr>
						</table>
					</form>
					<br/><br/>
				</li>
			<?php } ?>
			<li>
				<input type="radio" name="radChoose" id="radCancel" onclick="ShowCancel();"/>
				<span class='choose-option'>Cancel</span>, and link this identity with an existing <strong><?php echo Config::$site["name"]; ?></strong> account:
				<ol style='padding-top:0.25em;' id="cancelInstructions">
					<li><?php echo MvcHtml::ActionLink("Click here to link your <strong>" . ucfirst($__model->providerId) . "</strong> identity", "security","sso-edit"); ?>.</li>
					<li>Sign in using your existing account or previously used identity.</li>
					<li>Follow the on-screen instructions.</li>
				</ol>
			</li>
		<ul>
	</div>

	<style type="text/css">
		.validationError ul { padding-left:0; list-style-type:none; }

		.choose { list-style-type:none; padding-left:1em; }
		.choose-option { color:blue; font-size:1.25em; font-weight:bold; }

		#frmContinue { padding: 1em 0em 0em 3em; }
		#cancelInstructions { padding-left:5em; margin-top:1em; line-height:1.75em; }
	</style>

	<script type='text/javascript'>
		<?php if (!$__viewContext->modelState->GetError("email") && !$__viewContext->modelState->GetError("identityId")) { ?>
			ShowContinue();
		<?php } else { ?>
			ShowCancel();
		<?php } ?>
		function ShowContinue() {
			document.getElementById('radContinue').checked = true;
			document.getElementById('frmContinue').style.display='';
			document.getElementById('cancelInstructions').style.display='none';
		}

		function ShowCancel() {
			document.getElementById('radCancel').checked = true;
			document.getElementById('frmContinue').style.display='none';
			document.getElementById('cancelInstructions').style.display='';
		}
	</script>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
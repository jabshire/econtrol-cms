<?php
$__pageTitle = "Change Password";

ob_start();
?>

<div class="content">
	<h1>Change Your Password</h1>

	<form method="post" style="margin-left:2.0em;">
		<?php echo MvcHtml::HiddenFor($__model, "userId"); ?>
		<table>
			<tr><td colspan="2"><?php echo MvcHtml::ValidationSummary($__viewContext,false,""); ?></td></tr>
			<tr>
				<td><b>Current Password:</b></td>
				<td>
					<?php 
						echo MvcHtml::PasswordFor($__model, "currentPassword");
						echo MvcHtml::ValidationMessageFor($__viewContext, "currentPassword");
					?>
				</td>
			</tr>
			<tr>
				<td><b>New Password:</b></td>
				<td>
					<?php 
						echo MvcHtml::PasswordFor($__model, "newPassword");
						echo MvcHtml::ValidationMessageFor($__viewContext, "newPassword");
					?>
				</td>
			</tr>
			<tr>
				<td><b>Confirm New Password:</b></td>
				<td>
					<?php 
						echo MvcHtml::PasswordFor($__model, "confirmNewPassword");
						echo MvcHtml::ValidationMessageFor($__viewContext, "confirmNewPassword");
					?>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" value="Save"/>
					<input type="button" value="Cancel" onclick="window.location='<?php echo MvcRouter::GetActionUrl($__viewContext->viewData["__OriginatingControllerType"], "Account"); ?>';"/>
				</td>
			</tr>
		</table>
	</form>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
<?php
$__pageTitle = "Login";

ob_start();
?>

<div class="content">
	
	<?php echo MvcHtml::ValidationSummary($__viewContext,false,""); ?>

	<div class="login">
		<?php if (Config::$security["enableSso"] === true) {?>
			<h2>Option 1:</h2>
			<h3>Login using <?php echo AorAn() . " " . Config::$site["name"]; ?> account</h3>
		<?php } else { ?>
			<h1>Login</h1>
		<?php } ?>

		<form action="<?php echo MvcRouter::GetActionUrl($__viewContext->viewData["__OriginatingControllerType"], "Login"); ?>" method="post">
			<?php echo MvcHtml::HiddenFor($__model, "reply"); ?>
			<table>
				<tr>
					<td><b>Email:</b></td>
					<td>
						<?php 
							echo MvcHtml::TextBoxFor($__model, "email");
							echo MvcHtml::ValidationMessageFor($__viewContext, "email");
						?>
					</td>
				</tr>
				<tr>
					<td><b>Password:</b></td>
					<td>
						<?php 
							echo MvcHtml::PasswordFor($__model, "password");
							echo MvcHtml::ValidationMessageFor($__viewContext, "password");
						?>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;"><?php echo MvcHtml::CheckBoxFor($__model, "remember");?></td>
					<td>Remember Me</td>
				</tr>
				<tr><td></td><td><input type="submit" value="Login"/></td></tr>
				<tr><td colspan="2"><br/></td></tr>
				<tr><td></td><td><?php echo MvcHtml::ActionLink("Create a New Account", "Member", "Create"); ?></td></tr>
				<tr><td></td><td><?php echo MvcHtml::ActionLink("Forgot Password?", "Member", "ForgotPassword"); ?></td></tr>
			</table>
		</form>
	</div>

	<?php if (Config::$security["enableSso"] === true) {?>
		<div class="openid">
			<h2>Option 2:</h2>
			<h3>Sign in using your account with</h3>
			<table>
				<?php
					$ssoImgRoot = Config::$themes["default"]["root"] . "img/sso/";
					$count = 0;
					echo "<tr>";
					foreach($__viewContext->viewData["ssoProviders"] as $provider) {
						$data = $provider->id . ($__model->reply ? "/?reply=" . $__model->reply : "");

						//if (!$provider["endpoint"]) continue;
						echo "<td>" . str_replace("//","/", MvcHtml::ActionLink("<img src='" . $ssoImgRoot . "/" . $provider->id . ".png' title='" . $provider->description . "'/>", "security", "sso", $data)) . "</td>";
						if (++$count % 2 == 0)
							echo "</tr><tr>";
					}
				?>
			</table>
		</div>
	<?php } ?>
	<div style="clear:both;"></div>
</div>

<style>
	<?php if (Config::$security["enableSso"] === true) {?>
		.login, .openid {
			float:left;
			margin-top:10px;
		}

		.openid {
			border-left:2px #cccccc solid;
			margin-left:50px;
			padding-left:50px;
			/*display:none;*/
		}

		.openid img {
			padding:5px 10px;
			border:0;
			height:100%;
		}
	<?php } ?>

	.login table, .openid table { 
		margin-left:2.0em;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("#email").focus();
	});

</script>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);

function AorAn() {
	$vowels = array("a"=>"a","e"=>"e","i"=>"i","o"=>"o","u"=>"u");
	return isset($vowels[strtolower(Config::$site["name"][0])]) ? "an" : "a";
}
?>
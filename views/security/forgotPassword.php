<?php
$__pageTitle = "Forgot Password";

ob_start();
?>

<div class="content">
	<h1>I Forgot My Password</h1>

	<?php if (!$__model || !$__viewContext->modelState->IsValid()) { ?>
		<form method="post" style="margin-left:2.0em;">
			<?php echo MvcHtml::ValidationSummary($__viewContext,false,""); ?>
			<table>
				<tr>
					<td><b>Enter Your E-mail Address:</b></td>
					<td><input type="text" name="email" value="<?php echo $__model; ?>"/></td>
					<td><input type="submit"/></td>
				</tr>
			</table>
		</form>
	<?php } else { ?>
		An email has been sent to <b><?php echo $__model; ?></b>. Please follow the instructions in that email to reset your password.
	<?php } ?>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
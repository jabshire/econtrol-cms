<?php
$__pageTitle = "Reset Password";

ob_start();
?>

<div class="content">
	<h1>Reset Your Password</h1>

	<?php if (!isset($__viewContext->viewData["passwordReset"])) { ?>
		<form method="post" style="margin-left:2.0em;">
			<?php echo MvcHtml::HiddenFor($__model, "userId"); ?>
			<table>
				<tr><td colspan="2"><?php echo MvcHtml::ValidationSummary($__viewContext,false,""); ?></td></tr>
				<tr>
					<td><b>New Password:</b></td>
					<td>
						<?php 
							echo MvcHtml::PasswordFor($__model, "password");
							echo MvcHtml::ValidationMessageFor($__viewContext, "password");
						?>
					</td>
				</tr>
				<tr>
					<td><b>Confirm New Password:</b></td>
					<td>
						<?php 
							echo MvcHtml::PasswordFor($__model, "confirmPassword");
							echo MvcHtml::ValidationMessageFor($__viewContext, "confirmPassword");
						?>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="Save"/>
					</td>
				</tr>
			</table>
		</form>
	<?php } else { ?>
		Your password has been reset. <?php echo MvcHtml::ActionLink("Click here",$__viewContext->viewData["__OriginatingControllerType"],"Login");?> to log in.
	<?php } ?>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
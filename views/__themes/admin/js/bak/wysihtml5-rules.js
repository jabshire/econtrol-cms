/**
 * Full HTML5 compatibility rule set
 * These rules define which tags and css classes are supported and which tags should be specially treated.
 *
 * Examples based on this rule set:
 *
 *    <a href="http://foobar.com">foo</a>
 *    ... becomes ...
 *    <a href="http://foobar.com" target="_blank" rel="nofollow">foo</a>
 *
 *    <img align="left" src="http://foobar.com/image.png">
 *    ... becomes ...
 *    <img class="wysiwyg-float-left" src="http://foobar.com/image.png" alt="">
 *
 *    <div>foo<script>alert(document.cookie)</script></div>
 *    ... becomes ...
 *    <div>foo</div>
 *
 *    <marquee>foo</marquee>
 *    ... becomes ...
 *    <span>foo</marquee>
 *
 *    foo <br clear="both"> bar
 *    ... becomes ...
 *    foo <br class="wysiwyg-clear-both"> bar
 *
 *    <div>hello <iframe src="http://google.com"></iframe></div>
 *    ... becomes ...
 *    <div>hello </div>
 *
 *    <center>hello</center>
 *    ... becomes ...
 *    <div class="wysiwyg-text-align-center">hello</div>
 */
var wysihtml5ParserRules = {
    /**
     * CSS Class white-list
     * Following css classes won't be removed when parsed by the wysihtml5 html parser
     */
    "classes": {
        "alignleft": 1,
        "alignright": 1,
        "floatleft": 1,
        "floatright": 1,
        "clear": 1,
        "left": 1,
        "right": 1,
        "polaroid": 1
    },
    /**
     * Tag list
     *
     * Following options are available:
     *
     *    - add_class:        converts and deletes the given HTML4 attribute (align, clear, ...) via the given method to a css class
     *                        The following methods are implemented in wysihtml5.dom.parse:
     *                          - align_text:  converts align attribute values (right/left/center/justify) to their corresponding css class "wysiwyg-text-align-*")
                                  <p align="center">foo</p> ... becomes ... <p> class="wysiwyg-text-align-center">foo</p>
     *                          - clear_br:    converts clear attribute values left/right/all/both to their corresponding css class "wysiwyg-clear-*"
     *                            <br clear="all"> ... becomes ... <br class="wysiwyg-clear-both">
     *                          - align_img:    converts align attribute values (right/left) on <img> to their corresponding css class "wysiwyg-float-*"
     *
     *    - remove:             removes the element and it's content
     *
     *    - rename_tag:         renames the element to the given tag
     *
     *    - set_class:          adds the given class to the element (note: make sure that the class is in the "classes" white list above)
     *
     *    - set_attributes:     sets/overrides the given attributes
     *
     *    - check_attributes:   checks the given HTML attribute via the given method
     *                            - url:      checks whether the given string is an url, deletes the attribute if not
     *                            - alt:      strips unwanted characters. if the attribute is not set, then it gets set (to ensure valid and compatible HTML)
     *                            - numbers:  ensures that the attribute only contains numeric characters
     */
    "tags": {
        "p": {},
        "h1": {},
        "h2": {},
        "h3": {},
        "h4": {},
        "h5": {},
        "b": {},
        "i": {},
        "em": {},
        "u": {},
        "span": {},
        "br": {},
        "code": {},
        "abbr": {},
        "sup": {},
        "sub": {},
        "address": {},
        "div": {},
        "tbody": {},
        "pre": {},
        
        "ul": {},
        "ol": {},
        "li": {},
        
        "table": {},
        "tr": {},
        "td": {},
        
        "a": {
            "check_attributes": {
                "href": "url"
            },
            "set_attributes": {
                "rel": "follow",
                "target": "_self",
                "title": "foo"
            }
        },
        
        "img": {
            "check_attributes": {
                "alt": "alt",
                "src": "url"
            }
        },
        /* EXAMPLES BELOW 
        
        "strike": {
            "remove": 1
        },
        "form": {
            "rename_tag": "div"
        },
        "small": {
            "rename_tag": "span",
            "set_class": "wysiwyg-font-size-smaller"
        }
        
        */
    }
};
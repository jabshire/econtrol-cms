
<header class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<nav class="container">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="/admin/">eControl</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li class="<?php echo ($__currentAction=='users')? 'active':'' ?>"><?php echo MvcHtml::ActionLink('Users', 'admin', 'users'); ?></li>
					<li class="dropdown <?php echo ($__currentAction=='menus')? 'active':'' ?>">
						<?php echo MvcHtml::ActionLink('Menus <span class="caret"></span>', 'admin', 'menus', '', array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown')); ?>
						<ul class="dropdown-menu">
							<li><?php echo MvcHtml::ActionLink('Create Menu', 'admin', 'menu', 'add'); ?></li>
							<li><?php echo MvcHtml::ActionLink('List Menu', 'admin', 'menus'); ?></li>
						</ul>
					</li>
					<li class="dropdown <?php echo ($__currentAction=='pages')? 'active':'' ?>">
						<?php echo MvcHtml::ActionLink('Pages <span class="caret"></span>', 'admin', 'pages', '', array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown')); ?>
						<ul class="dropdown-menu">
							<li><?php echo MvcHtml::ActionLink('Create Page', 'admin', 'page', 'add'); ?></li>
							<li><?php echo MvcHtml::ActionLink('List Pages', 'admin', 'pages'); ?></li>
						</ul>
					</li>
					<li class="dropdown <?php echo ($__currentAction=='widgets')? 'active':'' ?>">
						<?php echo MvcHtml::ActionLink('Widgets <span class="caret"></span>', 'admin', 'widgets', '', array('class'=>'dropdown-toggle', 'data-toggle'=>'dropdown')); ?>
						<ul class="dropdown-menu">
							<li><?php echo MvcHtml::ActionLink('Create Widget', 'admin', 'widget', 'add'); ?></li>
							<li><?php echo MvcHtml::ActionLink('List Widget', 'admin', 'widgets'); ?></li>
						</ul>
					</li>
					<li><?php echo MvcHtml::ActionLink('Site', '', ''); ?></li>
					<li><?php echo MvcHtml::ActionLink('Logout', 'Member', 'Logout'); ?></li>
				</ul>
			</div>
		</nav>
	</div>
</header>

<?php require_once(Config::$themes["admin"]["root"] . "themeVariables.php"); ?>
<!DOCTYPE html>
<html class="<?php echo isset($__cssHtml) ? $__cssHtml : ""; ?>" lang="<?php echo @Config::$site["lang"]; ?>">
<head>
	<base href="<?php echo Config::$site["http"] . "://" . Config::$site["domain"] . Config::$site["root"]; ?>" />

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="shortcut icon" href="<?php echo $__imgRoot; ?>favicon.ico" />
	<link rel="icon" type="image/png" href="<?php echo $__imgRoot; ?>favicon.png" />
	
	
	
	<?php if (isset($__meta) && is_array($__meta)) { foreach($__meta as $key=>$val) {
	?><meta name="<?php echo $key; ?>" content="<?php echo $val; ?>">
	<?php }}?>

	<title><?php echo (@$__pageTitle) ? $__pageTitle . " | " . Config::$site["name"] : Config::$site["name"]; ?></title>

	<link rel="stylesheet" href="<?php echo $__cssRoot; ?>econtrol.css">
	<link rel="stylesheet" href="<?php echo $__cssRoot; ?>bootstrap.css">
	<link rel="stylesheet" href="<?php echo $__cssRoot; ?>bootstrap-wysihtml5.css">
	<link rel="stylesheet" href="<?php echo $__cssRoot; ?>bootstrap-datepicker.css">
	<link rel="stylesheet" href="<?php echo $__cssRoot; ?>bootstrap-timepicker.css">
	<?php if (isset($__css) && is_array($__css)) { foreach($__css as $val) {
			if (substr($val, 0, 1)=='/' || substr($val, 0, 1)=='.' || substr($val, 0, 4)=='http') {
	?><link rel="stylesheet" href="<?php echo $val; ?>">
	<?php	} else {
	?><link rel="stylesheet" href="<?php echo $__cssRoot . $val; ?>.css">
	<?php	}}}?>

	<script src="<?php echo $__jsRoot . "jquery.js" ?>"></script>
	<script src="<?php echo $__jsRoot . "wysihtml5.js" ?>"></script>
	<script src="<?php echo $__jsRoot . "bootstrap.js" ?>"></script>
	<script src="<?php echo $__jsRoot . "bootstrap-wysihtml5.js" ?>"></script>
	<script src="<?php echo $__jsRoot . "bootstrap-datepicker.js" ?>"></script>
	<script src="<?php echo $__jsRoot . "bootstrap-timepicker.js" ?>"></script>
	<?php if (isset($__js) && is_array($__js)) { foreach($__js as $val) {
			if (substr($val, 0, 1)=='/' || substr($val, 0, 4)=='http'){ 
	?><script src="<?php echo $val; ?>"></script>
	<?php	} else { 
	?><script src="<?php echo $__jsRoot . $val; ?>.js"></script>
	<?php	}}}?>
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body class="<?php echo isset($__cssBody) ? $__cssBody : ""; ?>">

<?php include_once('inc/header.php') ?>

<div class="container">

<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- end header -->


<?php echo $__pageCenter; ?>


<!-- begin footer -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->

</div><!--/container-->

</body>
</html>

/* =======================================================
/* eCONTROL
/* ======================================================= */
/* =============================== eControl Tabs */

$(function(){
	setCurrent(0, 0);
	$('.ec-tabsNav ul li a').click(function(){
		var tabNavIndex = getIndex( $(this).parent().parent().parent().parent() );
		var tabIndex = getIndex( $(this).parent() );
		setCurrent(tabNavIndex, tabIndex);
	});
});

function setCurrent(tabNavIndex, tabIndex){
	if (!tabNavIndex){
		// by load
		$('.ec-tabs .ec-tabsBody .ec-tabsBodyPage:first-child')
			.addClass('current')
			.siblings().removeClass('current');
		$('.ec-tabs .ec-tabsNav ul li:first-child')
			.addClass('current')
			.siblings().removeClass('current');
	} else {
		// by click
		$('.ec-tabs:nth-child('+tabNavIndex+') .ec-tabsBody .ec-tabsBodyPage:nth-child('+tabIndex+')')
			.addClass('current')
			.siblings().removeClass('current');
		$('.ec-tabs:nth-child('+tabNavIndex+') .ec-tabsNav ul li:nth-child('+tabIndex+')')
			.addClass('current')
			.siblings().removeClass('current');
	}
}

$(function(){
	resizeTabs();
});

$(window).resize(function() {
	resizeTabs();
}).resize();

function resizeTabs(){
	$('.ec-tabs').each(function(){
		var eWidth = $(this).width()-10;
		var eTabsWidth = $(this).children('.ec-tabsNav').outerWidth(true)+10;
		if (eWidth < eTabsWidth){
			$(this).addClass('collapse');
		}
		if (eWidth >= eTabsWidth){
			$(this).removeClass('collapse');
		}
	});
}

/* =============================== public: GET SIBLING INDEX */

function getIndex(e){
	return $(e).index()+1;
}

/* =============================== public: GET SIBLING INDEX */

function getElementWidth(e){
	return e.width();
}



<?php
require_once(Config::$themes["default"]["root"] . "themeVariables.php");
?>

<!DOCTYPE html>
<html class="<?php echo isset($__cssHtml) ? $__cssHtml : ""; ?>" lang="<?php echo @Config::$site["lang"]; ?>">
	<head>
		<base href="<?php echo Config::$site["http"] . "://" . Config::$site["domain"] . Config::$site["root"]; ?>" />

		<meta charset="utf-8" />
		<link rel="shortcut icon" href="<?php echo $__imgRoot; ?>favicon.ico" />
		<link rel="icon" type="image/png" href="<?php echo $__imgRoot; ?>favicon.png" />
		
		<?php if (isset($__meta) && is_array($__meta)) { foreach($__meta as $key=>$val) {
		?><meta name="<?php echo $key; ?>" content="<?php echo $val; ?>">
		<?php }}?>

		<title><?php echo (@$__pageTitle) ? $__pageTitle . " | " . Config::$site["name"] : Config::$site["name"]; ?></title>

		<link rel="stylesheet" href="<?php echo $__cssRoot; ?>econtrol.css"/>
		<link rel="stylesheet" href="<?php echo $__cssRoot; ?>template.css"/>
		<?php if (isset($__css) && is_array($__css)) { foreach($__css as $val) {
				if (substr($val, 0, 1)=='/' || substr($val, 0, 1)=='.' || substr($val, 0, 4)=='http') {
		?><link rel="stylesheet" href="<?php echo $val; ?>">
		<?php	} else {
		?><link rel="stylesheet" href="<?php echo $__cssRoot . $val; ?>.css">
		<?php	}}}?>

		<script src="<?php echo $__jsRoot . "jquery-1.5.2.min.js" ?>"></script>
		<script src="<?php echo $__jsRoot . "econtrol.js" ?>"></script>
		<?php if (isset($__js) && is_array($__js)) { foreach($__js as $val) {
				if (substr($val, 0, 1)=='/' || substr($val, 0, 4)=='http'){ 
		?><script src="<?php echo $val; ?>"></script>
		<?php	} else { 
		?><script src="<?php echo $__jsRoot . $val; ?>.js"></script>
		<?php	}}}?>

	</head>

	<body class="<?php echo isset($__cssBody) ? $__cssBody : ""; ?>">
		<header>
			<div style="float:left;"><?php echo MvcHtml::ActionLink("Home", "",""); ?></div>
			<div style="float:right;">
				<?php
					if ($__userContext) {
						echo "Welcome ";
						echo $__userContext->userName . "!&nbsp;";
						if ($__userContext->ssoProviderId) {
							echo "(<img src='" . $__imgRoot . "sso/icon/" . $__userContext->ssoProviderId . "_32.png' width='18px' align='center'/>)&nbsp;&nbsp;&nbsp;";
						}
						if ($__userContext->userGroupId >= Config::$security["userGroups"]["admin"])
							echo MvcHtml::ActionLink("Admin", 'admin','', '', array('class'=>'active')) . "&nbsp;";

						echo MvcHtml::ActionLink("My Account", 'Member','Account') . "&nbsp;";
						echo MvcHtml::ActionLink("Logout", "Member","Logout");
					}
					else {
							echo MvcHtml::ActionLink("Login", 'Member','Login') . "&nbsp;";
							echo MvcHtml::ActionLink("Create An Account", 'Member','Create');
					}
				?>
			</div>
			<div style="clear:left;"></div>
		</header>
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- end header -->










		<div class="content">
			<div style="width:980px; margin: 0 auto;">
				<i>Layout 1: single column, no sidebar</i>
				<div><?php echo $__body; ?></div>
			</div>
		</div>











<!-- begin footer -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
<!-- *********************************************************************************************** -->
		<footer>
			<i>footer</i>
		</footer>

		<?php 
			if (is_array(Config::$site["analyticsId"]) && count(Config::$site["analyticsId"]) > 0) { 
				foreach(Config::$site["analyticsId"] as $analyticsId) {
		?>

		<script type="text/javascript">
				// GOOGLE ANALYTICS
				var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
				document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
				try {
					var pageTracker = _gat._getTracker("<?php echo $analyticsId; ?>");
					pageTracker._trackPageview();
				} catch(err) {}
		</script>

		<?php 
				}
			}
		?>

	</body>
</html>
<div class="ec-widget <?php echo $__class; ?>">
	<div class="ec-widget-title"><?php echo $__title; ?></div>
	<?php if ($__featuredImage) echo "<div>" . MvcEcontrolHtml::Image($__featuredImage, "medium") . "</div>"; ?>
	<?php echo $__body; ?>
</div>
<?php
require_once(Config::$themes["default"]["root"] . "themeVariables.php");

$__pageTitle = "Home";
ob_start();
?>

<div class="content">
	<h1><?php echo Config::$site["name"]; ?></h1>

	<ul>
	<?php 
		if ($__userContext) {
			if ($__userContext->userGroupId >= Config::$security["userGroups"]["admin"])
				echo "<li>" . MvcHtml::ActionLink("Admin", 'admin','') . "</li>";

			echo "<li>" . MvcHtml::ActionLink("My Account", 'Member','Account') . "</li>";
			echo "<li>" . MvcEcontrolHtml::SlugActionLink("My Info (slug example)", $__userContext->userName, 'Member','Info') . "</li>";
			echo "<li>" . MvcHtml::ActionLink("Logout", "Member","Logout") . "</li>";
		}
		else {
			echo "<li>" . MvcHtml::ActionLink("Login", 'Member','Login') . "</li>";
			echo "<li>" . MvcHtml::ActionLink("Create An Account", 'Member','Create') . "</li>";
		} 
	?>
	</ul>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
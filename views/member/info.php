<?php
$__pageTitle = "Member Info";
//$__model->ssoIdentities = array();
ob_start();
?>

<div class="content">
	<h1>Member Info</h1>

	<table>
		<tr>
			<td><b>User Name:</b></td>
			<td><?php echo $__model->userName; ?></td>
		</tr>
		<tr>
			<td><b>First Name:</b></td>
			<td><?php echo $__model->firstName; ?></td>
		<tr>
			<td><b>Last Name:</b></td>
			<td><?php echo $__model->lastName; ?></td>
		</tr>
		<tr>
			<td><b>E-mail:</b></td>
			<td><?php echo $__model->email; ?></td>
		</tr>
	</table>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
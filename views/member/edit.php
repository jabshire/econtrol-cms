<?php
$__pageTitle = "Edit Account";

ob_start();
?>

<div class="content">
	<h2>Your Account</h2>

	<?php
		if ($__userContext->ssoProviderId) {
			$siteName = Config::$site["name"];
			$ssoProviderName = "Single Sign-On";

			echo "<div class='notice'>";
			echo "Please be aware that your <strong>$siteName</strong> account information is stored separately from your <strong>$ssoProviderName</strong> identity information. Any changes you make to your <strong>$siteName</strong> account will not affect your <strong>$ssoProviderName</strong> identity.";
			echo "</div><br/>";
		}
	?>

	<form method="post" style="margin-left:2.0em;">
		<?php echo MvcHtml::HiddenFor($__model, "userId"); ?>
		<?php echo MvcHtml::HiddenFor($__model, "userGroupId"); ?>
		<?php echo MvcHtml::ValidationSummary($__viewContext); ?>
		<table>
			<tr>
				<td><b>User Name:</b></td>
				<td>
					<?php echo MvcHtml::HiddenFor($__model, "userName"); ?>
					<?php echo $__model->userName; ?>
				</td>
			</tr>
			<tr>
				<td><b>E-mail:</b></td>
				<td>
					<?php echo MvcHtml::HiddenFor($__model, "oldEmail"); ?>
					<?php echo MvcHtml::TextBoxFor($__model, "email"); ?>
					<?php echo MvcHtml::ValidationMessageFor($__viewContext, "email"); ?>
				</td>
			</tr>
			<tr>
				<td><b>First Name:</b></td>
				<td>
					<?php 
					echo MvcHtml::TextBoxFor($__model, "firstName");
					echo MvcHtml::ValidationMessageFor($__viewContext, "firstName");
					?>
				</td>
			</tr>
			<tr>
				<td><b>Last Name:</b></td>
				<td>
					<?php 
					echo MvcHtml::TextBoxFor($__model, "lastName");
					echo MvcHtml::ValidationMessageFor($__viewContext, "lastName");
					?>
				</td>
			</tr>
			<tr><td colspan='2'><br/></td></tr>
			<tr>
				<td></td>
				<td>
					<input type='submit' value='Update'/>
					<input type="button" value="Cancel" onclick="window.location='<?php echo MvcRouter::GetActionUrl("Member", "Account"); ?>';"/>
				</td>
			</tr>
		</table>
	</form>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
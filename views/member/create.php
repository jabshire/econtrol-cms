<?php
$__pageTitle = "Create Account";

ob_start();
$captchaPath = MvcRouter::$viewsRoot . "security/coolCaptcha0.3/captcha.php";
?>

<style type="text/css">
	.note { font-size: 0.75em; font-style:italic; }
</style>

<div class="content">
	<h1>Create Account</h1>

	<?php echo MvcEcontrolHtml::ValidationSummary($__viewContext, true); ?>
	<form action="<?php echo MvcRouter::GetActionUrl($__viewContext->controllerType,$__viewContext->action); ?>" method="post" style="margin-left:2.0em;">
		<input type="hidden" name="userGroupId" value="2"/>
		<table>
			<tr><td colspan="2" class="note">Your email address will be your login id.</td></tr>
			<tr>
				<td><b>Email:</b></td>
				<td>
					<?php echo MvcHtml::TextBoxFor($__model, "email", array("size"=>40)); ?>
					<?php echo MvcEcontrolHtml::ValidationMessageFor($__viewContext, "email"); ?>
				</td>
			</tr>
			<tr>
				<td><b>Password:</b></td>
				<td>
					<?php echo MvcHtml::PasswordFor($__model, "password"); ?>
					<?php echo MvcEcontrolHtml::ValidationMessageFor($__viewContext, "password", "*"); ?>
				</td>
			</tr>
			<tr>
				<td><b>Confirm Password:</b></td>
				<td>
					<?php echo MvcHtml::PasswordFor($__model, "confirmPassword"); ?>
					<?php echo MvcEcontrolHtml::ValidationMessageFor($__viewContext, "confirmPassword"); ?>
				</td>
			</tr>
			<tr><td colspan='2'><br/></td></tr>
			<tr><td colspan="2" class="note">Your user name is your screen name. This is not your login id.</td></tr>
			<tr>
				<td><b>User Name:</b></td>
				<td>
					<?php echo MvcHtml::TextBoxFor($__model, "userName", array("size"=>15)); ?>
					<?php echo MvcEcontrolHtml::ValidationMessageFor($__viewContext, "userName"); ?>

				</td>
			</tr>
			<tr>
				<td><b>First Name:</b></td>
				<td><?php echo MvcHtml::TextBoxFor($__model, "firstName"); ?></td>
			</tr>
			<tr>
				<td><b>Last Name:</b></td>
				<td><?php echo MvcHtml::TextBoxFor($__model, "lastName", array("size"=>30)); ?></td>
			</tr>

		<?php if (Config::$security["useCaptcha"] === true) { ?>
			<tr><td colspan='2'><br/></td></tr>
			<tr>
				<td><b>Captcha:</b></td>
				<td><img src="<?php echo $captchaPath; ?>" id="captchaImg" align="middle"/><br/>
					<a href="#" onclick="$('#captchaImg').attr('src','<?php echo $captchaPath; ?>?'+Math.random()); $('#captcha').focus(); return false;" style="font-size:0.8em;">Not readable? Change text.</a></td>
			</tr>
			<tr>
				<td><b>Type Captcha:</b></td>
				<td>
					<input type='text' id='captcha' name='captcha'/>
					<?php echo MvcEcontrolHtml::ValidationMessageFor($__viewContext, "captcha"); ?>
				</td>
			</tr>
		<?php } ?>

			<tr><td colspan='2'><br/></td></tr>
			<tr><td></td><td><input type="submit" value="Create Account"/></td></tr>
		</table>
	</form>
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
<?php
$__pageTitle = "Account Verified";

ob_start();
?>

<div class="content">
	<h1>Account Verified</h1>

	Thank you! Your account has been verified. <?php echo MvcHtml::ActionLink("Click here", $__viewContext->viewData["__OriginatingControllerType"],"Login");?> to log in.
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
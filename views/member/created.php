<?php
$__pageTitle = "Account Created";

ob_start();
?>

<div class="content">
	<h1>Account Created</h1>

	Your account has been created, but it must be verified before you can log in. An email has been sent to <b><?php echo $__model; ?></b>. Please follow the instructions in the email to verify your account.
</div>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
<?php
$__pageTitle = "My Account";
//$__model->ssoIdentities = array();
ob_start();
?>

<div class="content">
	<h1>My Account</h1>

	<h2><?php echo Config::$site["name"]; ?> Account Information</h2>

	<?php
		if ($__userContext->ssoProviderId) {
			$siteName = Config::$site["name"];
			$ssoProviderName = "";
			
			foreach($__model->ssoIdentities as $identity) {
				if ($identity->providerId == $__userContext->ssoProviderId) {
					$ssoProviderName = $identity->providerName;
					break;
				}
			}
			echo "<div class='notice'>";
			echo "Please be aware that your <strong>$siteName</strong> account information is stored separately from your <strong>$ssoProviderName</strong> identity information. Any changes you make to your <strong>$siteName</strong> account will not affect your <strong>$ssoProviderName</strong> identity. Therefore, the information below may be different from that of your <strong>$ssoProviderName</strong> identity.";
			echo "</div><br/>";
		}
	?>

	<div class="edit">
		<?php echo MvcHtml::ActionLink("Edit", "Member","Edit"); ?>
	</div>

	<table>
		<tr>
			<td><b>User Name:</b></td>
			<td><?php echo $__model->userName; ?></td>
		</tr>
		<tr>
			<td><b>First Name:</b></td>
			<td><?php echo $__model->firstName; ?></td>
		<tr>
			<td><b>Last Name:</b></td>
			<td><?php echo $__model->lastName; ?></td>
		</tr>
		<tr>
			<td><b>E-mail:</b></td>
			<td><?php echo $__model->email; ?></td>
		</tr>
	</table>

	<br/>
	<h2>Security</h2>
	<div class="edit">
		<?php 
			echo MvcHtml::ActionLink("Change Password","Member","ChangePassword");
			if (Config::$security["enableSso"] === true) {
				echo "&nbsp;|&nbsp;";
				echo MvcHtml::ActionLink("Edit Single Sign-On", "security", "sso-edit");
			}
		?>
	</div>

	<table>
		<?php if (isset($__viewContext->viewData["HasPasswordChanged"]) && $__viewContext->viewData["HasPasswordChanged"]) { ?>
			<tr>
				<td><b>Password:</b></td>
				<td><div class="updateStatus">Your password has been changed!</div></td>
			</tr>
			<tr><td colspan="2"><br/></td></tr>
		<?php } ?>
		<?php if (Config::$security["enableSso"] === true) { ?>
			<tr id="sso">
				<td><b>Single Sign-On:</b></td>
				<td>
					<?php
						if (count($__model->ssoIdentities) > 0) {
							echo "The following Single Sign-On (SSO) identities are associated with your account:";						
						}
						else {
							echo "<i>You do not have any Single Sing-On (SSO) identities associated with your account.</i>";
						}
					?>
				</td>
			</tr>
			<?php
				if (count($__model->ssoIdentities) > 0) {
					$imgRoot = Config::$themes["default"]["root"] . "img/sso/icon/";

					foreach($__model->ssoIdentities as $identity) {
						echo "<tr id='sso'>";
							echo "<td align='right'>";
								echo "<img src='" . $imgRoot . $identity->providerId . "_32.png'/>";
							echo "</td>";
							echo "<td><nobr/>";
								echo "<table>";
									echo "<tr><td><b>Provider:</b></td><td>$identity->providerName</td></tr>";
									echo "<tr><td><b>Identifier:</b></td><td>$identity->identityId</td></tr>";
									if ($identity->userName != $identity->email) echo "<tr><td><b>Username:</b></td><td>$identity->userName</td></tr>";
									if ($identity->email) echo "<tr><td><b>Email:</b></td><td>$identity->email</td></tr>";
									echo "<tr><td><b>First Name:</b></td><td>$identity->firstName</td></tr>";
									echo "<tr><td><b>Last Name:</b></td><td>$identity->lastName</td></tr>";
									//echo "<tr><td><b>Signed In?:</b></td><td>" . ($identity->providerId == $__userContext->ssoProviderId ? "Yes" : "No") . "</td></tr>";
								echo "</table>";
							echo "</td>";
						echo "</tr>";
					}				
				}
			?>
		<?php } // end: if (Config::$security["enableSso"] === true) ?>
	</table>
</div>

<style type="text/css">
	.edit { padding-bottom:10px; }
	#sso td { vertical-align:top; }
	.updateStatus { color: #007700; }
	.notice { color:#bb5500; }
</style>

<?php
$__pageCenter = ob_get_contents();
ob_end_clean();
include_once(Config::$themes["default"]["root"] . Config::$themes["default"]["masterPageFile"]);
?>
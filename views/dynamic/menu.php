<?php
// *******************************************************
// $__title
$__title = $__model->title;
?>

<?php
// *******************************************************
// $__menuItems
ob_start();
if (count($__model->body)) {

	$__menuItemsArray = array();
	$i = 0;
	
	foreach($__model->body as $menuItem){
		
		$menuIsActive = ($menuItem->url == $__viewContext->viewData["originalRoute"] || $menuItem->url == $__viewContext->viewData["reRoute"] || $menuItem->url == $__viewContext->viewData["dynamicContentPrimaryRoute"])? true : false;
		
		$__menuItem = array(
			'title' => $menuItem->title,
			'text' => $menuItem->text,
			'url' => $menuItem->url,
			'is_active' => $menuIsActive
		);
		
		$__menuItemsArray[$i] = $__menuItem;
		
		$i++;
		
	}

}
$__menuItems = ob_get_contents();
ob_end_clean();
// *******************************************************
?>

<?php
	$dynamicTheme = Config::$dynamicContent["theme"];
	$dynamicLayout = Config::$dynamicContent["layouts"]["menu"][$__model->layout];
	include(Config::$themes[$dynamicTheme]["root"] . $dynamicLayout);
?>
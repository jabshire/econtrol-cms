<?php
$__pageTitle = $__model->seoTitle;
$__meta = array("description"=>$__model->seoDescription);

// MAKE PAGE PRIVATE ENABLED

if ($__model->status == 'PRIVATE'){
	if (MvcSecurity::GetUserContext() == null) {
		$routeRequest = MvcRouter::FindRouteRequest();
		$replyUrl = MvcRouter::GetActionUrl($routeRequest->controllerType, $routeRequest->action, $routeRequest->data);
		MvcController::Redirect("member", "login", array("reply"=>$replyUrl));
	}
}
?>


<?php
	$__featuredImage = $__model->image;
?>


<?php
// *******************************************************
// $__body
ob_start();
?>
	<h1><?php echo $__model->title; ?></h1>
	<h2><?php echo $__model->layout; ?></h2>
	<?php echo MvcEcontrolHtml::ReplaceShortCodes($__model->body); ?>

<?php
$__body = ob_get_contents();
ob_end_clean();
// *******************************************************
?>




<?php
// *******************************************************
// $__sidebar

$__sidebar = $__model->sidebar;

// *******************************************************
?>




<?php
// *******************************************************
// layout

$dynamicTheme = Config::$dynamicContent["theme"];
$dynamicLayout = Config::$dynamicContent["layouts"]["page"][$__model->layout];
include_once(Config::$themes[$dynamicTheme]["root"] . $dynamicLayout);
?>
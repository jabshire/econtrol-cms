<?php
// *******************************************************
// $__title
$__class = $__model->class;
// *******************************************************

// *******************************************************
// $__title
$__title = $__model->title;
// *******************************************************
?>


<?php
// *******************************************************
$__featuredImage = $__model->image;
// *******************************************************
?>

<?php
// *******************************************************
// $__body
$__body = MvcEcontrolHtml::ReplaceShortCodes($__model->body);
// *******************************************************
?>



<?php
	$dynamicTheme = Config::$dynamicContent["theme"];
	$dynamicLayout = Config::$dynamicContent["layouts"]["widget"][$__model->layout]; // TODO: make layout dynamic
	include(Config::$themes[$dynamicTheme]["root"] . $dynamicLayout);
?>
-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 13, 2013 at 09:44 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ec_econtrolcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_content`
--

CREATE TABLE `dynamic_content` (
  `dynamic_content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('PAGE','BLOG','MENU','WIDGET','IMAGE') NOT NULL DEFAULT 'PAGE',
  `title` varchar(50) NOT NULL,
  `status` enum('DRAFT','PRIVATE','PUBLISHED') DEFAULT 'DRAFT',
  `create_by_user_id` int(11) unsigned NOT NULL,
  `create_date` datetime NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `modify_by_user_id` int(11) unsigned NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dynamic_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `dynamic_content`
--

INSERT INTO `dynamic_content` (`dynamic_content_id`, `type`, `title`, `status`, `create_by_user_id`, `create_date`, `publish_date`, `modify_by_user_id`, `modify_date`) VALUES
(15, 'PAGE', 'Jacob is Testing', 'DRAFT', 3, '2012-11-01 19:22:52', '2012-11-01 19:22:52', 3, '2012-11-03 17:52:08'),
(16, 'PAGE', 'Phasellus Molestie Magna Non', 'PUBLISHED', 3, '2012-11-02 13:39:39', '2012-11-02 13:39:39', 3, '2012-11-05 13:46:58'),
(19, 'PAGE', 'About NoteToShelf', 'PUBLISHED', 3, '2012-11-06 16:28:54', '2012-11-06 16:28:54', 1, '2013-03-07 16:55:08'),
(20, 'MENU', 'Main Menu', 'PUBLISHED', 3, '2012-11-06 16:32:03', '2012-11-06 16:32:03', 3, '2012-11-07 00:32:03'),
(21, 'WIDGET', 'First Widget', 'PUBLISHED', 2, '2012-11-06 19:24:40', '2012-11-06 19:40:15', 2, '2012-11-14 04:30:56'),
(24, 'WIDGET', 'Dynamic Widget', 'PUBLISHED', 2, '2012-11-06 19:44:41', '2012-11-06 19:44:41', 2, '2012-11-14 04:30:50'),
(25, 'PAGE', 'hello world', 'PUBLISHED', 2, '2012-11-07 20:22:58', '2012-11-07 20:22:58', 2, '2012-12-04 03:05:00'),
(29, 'PAGE', 'test1', 'DRAFT', 2, '2012-11-08 16:38:53', NULL, 2, '2012-11-09 00:38:53'),
(30, 'PAGE', 'test2', 'PUBLISHED', 2, '2012-11-08 16:39:06', '2012-02-12 18:00:00', 1, '2013-03-06 22:35:09'),
(31, 'PAGE', 'Tab Testing', 'PUBLISHED', 2, '2012-11-08 16:39:20', '2012-11-12 05:16:43', 3, '2013-02-21 21:19:14'),
(32, 'WIDGET', 'This is People Servics', 'PUBLISHED', 3, '2012-11-08 19:10:23', '2012-11-08 19:10:23', 2, '2012-11-14 04:31:04'),
(33, 'WIDGET', 'David Menu Widget', 'PUBLISHED', 2, '2012-11-13 20:21:48', '2012-11-13 20:21:48', 3, '2013-02-09 18:58:23'),
(34, 'WIDGET', 'Static Widget', 'PUBLISHED', 2, '2012-11-13 20:22:07', '2012-11-13 20:22:07', 10, '2012-12-10 22:51:07'),
(39, 'MENU', 'Utility Menu', 'PUBLISHED', 3, '2012-12-29 18:22:45', '2012-12-29 18:22:45', 1, '2013-03-07 16:18:23'),
(40, 'WIDGET', 'Tab One', 'PUBLISHED', 3, '2013-02-09 14:38:33', '2013-02-09 14:38:33', 3, '2013-02-09 20:38:33'),
(41, 'WIDGET', 'Tab Two', 'PUBLISHED', 3, '2013-02-09 14:38:53', '2013-02-09 14:38:53', 3, '2013-02-09 20:38:53'),
(42, 'WIDGET', 'Tab Three', 'PUBLISHED', 3, '2013-02-09 14:39:11', '2013-02-09 14:39:11', 3, '2013-02-09 20:39:11'),
(43, 'MENU', 'Footer Menu', 'PUBLISHED', 1, '2013-03-07 10:40:55', '2013-03-07 10:40:55', 1, '2013-03-07 16:41:01');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_content_data`
--

CREATE TABLE `dynamic_content_data` (
  `dynamic_content_id` int(11) unsigned NOT NULL,
  `seo_title` varchar(50) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `teaser` text,
  `body` text,
  `image` int(11) unsigned DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `sidebar` varchar(255) DEFAULT NULL,
  `layout` varchar(50) NOT NULL,
  KEY `FK_dynamic_content_data` (`dynamic_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dynamic_content_data`
--

INSERT INTO `dynamic_content_data` (`dynamic_content_id`, `seo_title`, `seo_description`, `teaser`, `body`, `image`, `class`, `sidebar`, `layout`) VALUES
(15, 'Jacob is Testing', '', 'Jacob is Testing', 'Jacob is <b>Testingsss</b>', 0, '', '', '3'),
(16, 'Etiam At Risus Et Justo ', '', 'Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor.', 'Suspendisse dictum feugiat nisl ut dapibus. Mauris iaculis porttitor posuere. Praesent id metus massa, ut blandit odio. Proin quis tortor orci. Etiam at risus et justo dignissim congue. Donec congue lacinia dui, a porttitor lectus condimentum laoreet. Nunc eu ullamcorper orci. <b>Quisque eget odio</b> ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum. Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante.<br>', 0, '', '', '5'),
(19, 'About eControl', '', '', '<p>In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin sapien justo in libero. Vestibulum mollis mauris enim. Morbi euismod magna ac lorem rutrum elementum. Donec viverra auctor lobortis. Pellentesque eu est a nulla placerat dignissim. Morbi a enim in magna semper bibendum.\r\n\r\nEtiam scelerisque, nunc ac egestas consequat, odio nibh euismod nulla, eget auctor orci nibh vel nisi.</p><p>Aliquam erat volutpat. Mauris vel neque sit amet nunc gravida congue sed sit amet purus. Quisque lacus quam, egestas ac tincidunt a, lacinia vel velit. Aenean facilisis nulla vitae urna tincidunt congue sed ut dui. Morbi malesuada nulla nec purus convallis consequat. Vivamus id mollis quam. Morbi ac commodo nulla. In condimentum orci id nisl volutpat bibendum.&nbsp;</p><p></p><ul><li>Quisque commodo hendrerit lorem quis egestas.</li><li>Maecenas quis tortor arcu.</li><li>Vivamus rutrum nunc non neque consectetur quis placerat neque lobortis.</li><li>Nam vestibulum, arcu sodales feugiat consectetur, nisl orci bibendum elit, eu euismod magna sapien ut nibh.</li><li>Donec semper quam scelerisque tortor dictum gravida.</li><li>In hac habitasse.<br></li></ul><p></p>', 0, '', '["24"]', '2'),
(20, '', '', '', '[{\\"guid\\":\\"d914908c-49e7-b273-f37d-4d497fe3b0d0\\",\\"text\\":\\"Home\\",\\"title\\":\\"Home\\",\\"url\\":\\"/\\"},{\\"guid\\":\\"46cc34ea-3046-465c-0c2a-2001ffb5f472\\",\\"text\\":\\"About\\",\\"title\\":\\"About eControl\\",\\"url\\":\\"19\\"}]', 0, '', '', '0'),
(21, '', '', '', 'This is a <b><u>widget</u></b>.', 0, '', '', '1'),
(24, '', '', '', 'Hello World!', 0, '', '', '1'),
(25, 'hello', '', '', '<br><h2>Here are my images :)</h2><div><br></div><div><img alt="" src="http://econtrolcms.com/uploads/2012/11/medium/DSC04991.JPG" title="Image: http://econtrolcms.com/uploads/2012/11/medium/DSC04991.JPG"><img alt="" src="http://econtrolcms.com/uploads/2012/11/small/DSC04991.JPG"><img alt="" src="http://econtrolcms.com/uploads/2012/11/small/images.jpeg"></div>', 37, '', '["34"]', '2'),
(29, 'test1', '', '', 'test1', 0, '', '', '2'),
(30, 'test2', '', '', 'asdfasdf', 0, '', '["42"]', '1'),
(31, 'test3', '', '', '<div>[{tabs id=''40,41,42''}]</div>', 0, '', '["33"]', '3'),
(32, '', '', '', '<p>Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', 0, '', '', '1'),
(33, '', '', '', '<div>\r\n<div>Spectators</div>\r\n<div>\r\n[{menu id=''39'' layout=''1''}]\r\n</div>\r\n</div>', 0, '', '', '1'),
(34, '', '', '', '[{static action=''hello-world''}]', 38, '', '', '1'),
(39, '', '', '', '[{"guid":"4b9fd815-5598-399e-9b0c-4a677de91d5e","text":"Follow","title":"Follow","url":"http://twitter.com/thepreacherslib"},{"guid":"5be50b1c-0163-fe9c-fd80-85bc3db278ac","text":"Donate","title":"Donate","url":"/donate/"},{"guid":"70458c09-220b-f686-94e6-dc5ee949eb92","text":"Contact","title":"Contact","url":"/contact/"}]', 0, '', '', '0'),
(40, '', '', '', '<p>Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna.</p>', 0, '', '', '4'),
(41, '', '', '', '<p>&nbsp;Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna.&nbsp;Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>', 0, '', '', '4'),
(42, '', '', '', '<p>Vivamus luctus urna.&nbsp;&nbsp;Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna.</p>', 0, '', '', '4'),
(43, '', '', '', '[{"guid":"e77da707-fb33-68ad-2db0-66beefd4fc4e","text":"Home","title":"Home","url":"/"},{"guid":"535d5beb-90eb-a402-a8b6-14b3b20bc18f","text":"About","title":"About NoteToShelf","url":"19"},{"guid":"2a75c579-bf8a-84b1-f5f4-1ec372b56053","text":"Follow","title":"Follow","url":"http://twitter.com/thepreacherslib"},{"guid":"7c74e76e-5fdb-15b1-a581-97dcd1728e69","text":"Donate","title":"Donate","url":"/donate/"},{"guid":"d55e46aa-b88b-e9af-e72a-4127813ed4b1","text":"Contact","title":"Contact","url":"/contact/"}]', 0, '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_content_routes`
--

CREATE TABLE `dynamic_content_routes` (
  `dynamic_content_id` int(11) unsigned NOT NULL,
  `route` varchar(255) NOT NULL,
  `is_primary` bit(1) NOT NULL,
  UNIQUE KEY `dynamic_content_route` (`route`),
  KEY `FK_dynamic_content_routes` (`dynamic_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dynamic_content_routes`
--

INSERT INTO `dynamic_content_routes` (`dynamic_content_id`, `route`, `is_primary`) VALUES
(19, 'about/', ''),
(25, 'hello/world/', ''),
(15, 'jacob/is/testing/', ''),
(16, 'jacob/route/', '\0'),
(16, 'jacob/routes/', ''),
(29, 'test/1/', ''),
(30, 'test/2/', ''),
(31, 'test/3/', '');

-- --------------------------------------------------------

--
-- Table structure for table `sso_endpoint`
--

CREATE TABLE `sso_endpoint` (
  `sso_provider_id` varchar(50) NOT NULL,
  `authorization` varchar(255) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `identity` varchar(255) NOT NULL,
  PRIMARY KEY (`sso_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sso_endpoint`
--

INSERT INTO `sso_endpoint` (`sso_provider_id`, `authorization`, `scope`, `token`, `identity`) VALUES
('aol', '', 'contact/email', '', 'https://www.aol.com'),
('facebook', 'https://graph.facebook.com/oauth/authorize', 'email', 'https://graph.facebook.com/oauth/access_token', 'https://graph.facebook.com/me'),
('google', 'https://accounts.google.com/o/oauth2/auth', 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile', 'https://accounts.google.com/o/oauth2/token', 'https://www.googleapis.com/oauth2/v2/userinfo'),
('wordpress', '', 'contact/email', '', 'http://YOURBLOG.wordpress.com'),
('yahoo', '', 'contact/email;namePerson;namePerson/friendly', '', 'https://me.yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `sso_identity`
--

CREATE TABLE `sso_identity` (
  `sso_provider_id` varchar(50) NOT NULL,
  `identity_id` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`sso_provider_id`,`identity_id`),
  UNIQUE KEY `sso_identity.identity_id-user_id` (`identity_id`,`user_id`),
  UNIQUE KEY `sso_identity.identity_id` (`identity_id`),
  KEY `sso_identity.user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sso_identity`
--

INSERT INTO `sso_identity` (`sso_provider_id`, `identity_id`, `user_name`, `email`, `first_name`, `last_name`, `user_id`) VALUES
('google', '108864257053036847854', 'drogers415@gmail.com', 'drogers415@gmail.com', 'Dennis', 'Rogers', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sso_provider`
--

CREATE TABLE `sso_provider` (
  `sso_provider_id` varchar(50) NOT NULL,
  `sso_type` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT '99',
  `enabled` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`sso_provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sso_provider`
--

INSERT INTO `sso_provider` (`sso_provider_id`, `sso_type`, `description`, `priority`, `enabled`) VALUES
('aol', 'openid', 'AOL', 9, 0),
('blogger', 'openid', 'Blogger', 5, 0),
('facebook', 'oauth2', 'Facebook', 2, 1),
('flickr', 'openid', 'Flickr', 6, 0),
('google', 'oauth2', 'Google', 1, 0),
('myopenid', 'openid', 'myOpenID', 8, 0),
('myspace', 'openid', 'MySpace', 10, 0),
('twitter', 'oauth', 'Twitter', 3, 1),
('wordpress', 'openid', 'Wordpress', 7, 0),
('yahoo', 'openid', 'Yahoo', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `UserId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `UserGroupId` int(11) unsigned DEFAULT '2',
  `UserName` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `FirstName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `LastName` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `IsVerified` tinyint(1) NOT NULL DEFAULT '0',
  `VerificationCode` varchar(255) CHARACTER SET latin1 NOT NULL,
  `CreateDate` datetime NOT NULL,
  `LastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `User.UserName` (`UserName`),
  KEY `FK_user` (`UserGroupId`),
  KEY `User.Email` (`Email`),
  KEY `User.UserNamePassword` (`UserName`,`Password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserId`, `UserGroupId`, `UserName`, `Password`, `Email`, `FirstName`, `LastName`, `IsVerified`, `VerificationCode`, `CreateDate`, `LastUpdated`) VALUES
(1, 2, 'drogers415', '202cb962ac59075b964b07152d234b70', 'drogers415@yahoo.com', 'd', 'r', 1, '4fdf336a935c4', '2012-06-18 06:55:54', '2012-06-18 13:56:16'),
(2, 99, 'drogers415@gmail.com', '202cb962ac59075b964b07152d234b70', 'drogers415@gmail.com', 'Dennis', 'Quade', 1, '108864257053036847854', '2012-06-30 13:04:25', '2012-11-07 19:08:56'),
(3, 999, 'jabshire', '5fcfd41e547a12215b173ff47fdd3739', 'jabshire@t411.com', 'Jacob', 'Abshire', 1, '4fef5cbe73cf0', '2012-06-30 13:08:30', '2012-10-20 15:29:00'),
(5, 99, 'david.littrell@insperity.com', '31b907e7d5775a5e54ab40bf1872e2e5', 'david.littrell@insperity.com', 'David', 'Littrell', 1, '50a41b2d43016', '2012-11-14 14:29:01', '2012-11-14 22:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `UserGroup`
--

CREATE TABLE `UserGroup` (
  `UserGroupId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`UserGroupId`),
  UNIQUE KEY `UserGroup.GroupName` (`GroupName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 ;

--
-- Dumping data for table `UserGroup`
--

INSERT INTO `UserGroup` (`UserGroupId`, `GroupName`) VALUES
(99, 'Admin'),
(1, 'Guest'),
(2, 'Member'),
(999, 'Super');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dynamic_content_data`
--
ALTER TABLE `dynamic_content_data`
  ADD CONSTRAINT `FK_dynamic_content_data` FOREIGN KEY (`dynamic_content_id`) REFERENCES `dynamic_content` (`dynamic_content_id`);

--
-- Constraints for table `dynamic_content_routes`
--
ALTER TABLE `dynamic_content_routes`
  ADD CONSTRAINT `FK_dynamic_content_routes` FOREIGN KEY (`dynamic_content_id`) REFERENCES `dynamic_content` (`dynamic_content_id`);

--
-- Constraints for table `sso_endpoint`
--
ALTER TABLE `sso_endpoint`
  ADD CONSTRAINT `FK_sso_endpoint` FOREIGN KEY (`sso_provider_id`) REFERENCES `sso_provider` (`sso_provider_id`);

--
-- Constraints for table `sso_identity`
--
ALTER TABLE `sso_identity`
  ADD CONSTRAINT `FK_sso_identity` FOREIGN KEY (`sso_provider_id`) REFERENCES `sso_provider` (`sso_provider_id`);

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `FK_user` FOREIGN KEY (`UserGroupId`) REFERENCES `UserGroup` (`UserGroupId`);
